
#ifndef LAUNCHER_UPDATE_H__
#define LAUNCHER_UPDATE_H__

#include <memory>
#include <string>

class WebDownloader;
class IniFileMan;

class LauncherUpdate
{
private:
    std::unique_ptr<WebDownloader> m_download;
    std::string m_currentVersion;
    std::string m_appName;
    std::string m_prevAppName;
    std::string m_state;

public:
    LauncherUpdate();
    ~LauncherUpdate();

private:
    std::unique_ptr<IniFileMan> fetchUpdateInfo(const std::string &fn);
    bool needUpdate(IniFileMan &ini, const std::string &cver);
    bool removePreviousFile(const std::string &fn);
    bool downloadLauncher(IniFileMan &ini);
    bool eraseBeforeDownload(const std::string &fn);
    void downloadRest(IniFileMan &ini);
    void runApp(const std::string &param);

public:
    bool ReadParams(const std::string &paramFilename);

private:
    bool putStateMessage(const std::string &message, bool ret);
    bool updateImpl();

public:
    void DoUpdate();
    const char *GetState() const;
};

#endif

