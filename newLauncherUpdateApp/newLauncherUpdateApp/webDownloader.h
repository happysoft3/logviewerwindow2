
#ifndef WEB_DOWNLOADER_H__
#define WEB_DOWNLOADER_H__

#include <string>
#include <memory>

class FileOutput;
class PathManager;

class WebDownloader
{
private:
    std::string m_repo;
    std::string m_user;
    std::string m_path;
    std::string m_lastErrorMsg;
    std::unique_ptr<FileOutput> m_file;

public:
    WebDownloader();
    ~WebDownloader();

public:
    void SetRepository(const std::string &repoName)
    {
        m_repo = repoName;
    }
    void SetUserName(const std::string &userName)
    {
        m_user = userName;
    }
    void SetPath(const std::string &resourcePath)
    {
        m_path = resourcePath;
    }
    const char *const LastError() const
    {
        return m_lastErrorMsg.c_str();
    }

private:
    std::unique_ptr<PathManager> makePath(const std::string &src);
    std::unique_ptr<PathManager> correctFileName(const std::string &src);
    static size_t writeData(void *ptr, size_t size, size_t nmemb, void *stream);
    std::string reconstructionUrl() const;
    bool downloadImpl();
    std::unique_ptr<PathManager> downloadCommon(const std::string &fileName, bool overwrite = false);

public:
    bool Download(const std::string &fileName, std::string &altUrl);
    bool DownloadOverwrite(const std::string &fileName);
};

#endif

