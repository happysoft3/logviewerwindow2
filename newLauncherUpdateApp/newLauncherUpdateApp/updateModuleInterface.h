
#ifndef UPDATE_MODULE_INTERFACE_H__
#define UPDATE_MODULE_INTERFACE_H__

#include "expose.h"

class EXPORT_OBJECT UpdateModuleInterface
{
public:
    UpdateModuleInterface();
    virtual ~UpdateModuleInterface();

    virtual const char *const DoUpdate() = 0;
};

#endif

