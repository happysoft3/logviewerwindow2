
#include "webDownloader.h"
#include "common/utils/pathManager.h"
#include "common/include/incFilesystem.h"
#include "common/utils/stringhelper.h"
#   include <iostream>
#include "curl-8.2.1/include/curl/curl.h"

/* For older cURL versions you will also need
#include <curl/types.h>
#include <curl/easy.h>
*/
#include <string>

#ifdef _DEBUG
#pragma comment(lib, "libcurld.lib")
#else
#pragma comment(lib, "libcurl.lib")
#endif
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "Wldap32")
#pragma comment (lib, "crypt32")
#pragma comment(lib, "libcrypto")
#pragma comment(lib, "libssl")

using namespace _StringHelper;

class FileOutput
{
private:
    FILE *m_file;

public:
    FileOutput()
    {
        m_file = nullptr;
    }
    ~FileOutput()
    {
        if (m_file)
            fclose(m_file);
    }

    bool Create(const PathManager &pathMan)
    {
        FILE *fp = nullptr;

        fopen_s(&fp, toArray( pathMan.FullPath() ), "wb");
        //m_file = std::ofstream(pathMan.FullPath(), std::ios::binary);
        //m_file << std::noskipws;
        //return m_file ? true : false;
        if (fp)
        {
            m_file = fp;
            return true;
        }
        return false;
    }

    size_t Write(const char *stream, size_t size, size_t nmemb)
    {
        return fwrite(stream, size, nmemb, m_file);
    }
};

WebDownloader::WebDownloader()
{ }

WebDownloader::~WebDownloader()
{ }

std::unique_ptr<PathManager> WebDownloader::makePath(const std::string &src)
{
    auto pathMan = std::make_unique<PathManager>();

    pathMan->SetUrl(src);
    return pathMan;
}

std::unique_ptr<PathManager> WebDownloader::correctFileName(const std::string &src)
{
    int dupCounter = 0;
    auto pathMan = std::make_unique<PathManager>();
    std::string ext, sub, _d;

    pathMan->SetUrl(src);
    pathMan->GetSubNameAndExt(sub, ext);
    while (NAMESPACE_FILESYSTEM::exists(pathMan->FullPath()))
    {
        pathMan->GetSubNameAndExt(_d, ext);
        pathMan->SetUrl(stringFormat("%s(%d)%s", sub, ++dupCounter, ext));
    }
    return pathMan;
}

size_t WebDownloader::writeData(void *ptr, size_t size, size_t nmemb, void *stream)
{
    const char *data = reinterpret_cast<const char *>(ptr);
    FileOutput *file = reinterpret_cast<FileOutput *>(stream);

    return file->Write(data, size, nmemb);
}

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
    size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
    return written;
}

std::string WebDownloader::reconstructionUrl() const
{
    return stringFormat(
        "https://gitlab.com/%s/%s/-/raw/%s",
        m_user, m_repo, m_path);
}

bool WebDownloader::downloadImpl()
{
    CURL *curl_handle;
    bool result = false;
    std::string url = reconstructionUrl();

    curl_global_init(CURL_GLOBAL_ALL);

    /* init the curl session */
    curl_handle = curl_easy_init();

    /* set URL to get here */
    curl_easy_setopt(curl_handle, CURLOPT_URL, toArray(url));

    /* Switch on full protocol/debug output while testing */
    curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);
    
    curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, 0L);

    //curl_easy_setopt(curl_handle, CURLOPT_NOSIGNAL, 1);
    //curl_easy_setopt(curl_handle, CURLOPT_TIMEOUT, 10);

    /* disable progress meter, set to 0L to enable and disable debug output */
    curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);

    /* send all data to this function  */
//#define _MY_TEST
#ifndef _MY_TEST
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WebDownloader::writeData);

    /* open the file */
    //if (m_file)
    //pagefile = fopen(pagefilename, "wb");

    std::unique_ptr<FileOutput> file = std::move(m_file);
    if (file) {

        /* write the page body to this file handle */
        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, file.get());

        /* get it! */
        CURLcode res = curl_easy_perform(curl_handle);

        m_lastErrorMsg = curl_easy_strerror(res);
        long respCode = 0L;
        curl_easy_getinfo(curl_handle, CURLINFO_RESPONSE_CODE, &respCode);
        result = respCode == 200;

        /* close the header file */
        //fclose(pagefile);
    }
#else
    m_file.reset();
    FILE *fp;

    fopen_s(&fp, "output.bin", "wb");
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, fp);
    CURLcode res = curl_easy_perform(curl_handle);
    m_lastErrorMsg = curl_easy_strerror(res);
    fclose(fp);
#endif

    /* cleanup curl stuff */
    curl_easy_cleanup(curl_handle);

    curl_global_cleanup();
    return result;
}

std::unique_ptr<PathManager> WebDownloader::downloadCommon(const std::string &fileName, bool overwrite)
{
    if (fileName.empty())
    {
        m_lastErrorMsg = "input url is empty";
        return{ };
    }

    auto pathMan = overwrite ? makePath(fileName) : correctFileName(fileName);
    auto file = std::make_unique<FileOutput>();

    if (file->Create(*pathMan))
    {
        m_file = std::move(file);
        if (!downloadImpl())
            return{ };
        return pathMan;
    }
    return{ };
}

bool WebDownloader::Download(const std::string &fileName, std::string &altUrl)
{
    auto pathman = downloadCommon(fileName);

    if (pathman)
    {
        altUrl = pathman->FullPath();
        return true;
    }
    return false;
}

bool WebDownloader::DownloadOverwrite(const std::string &fileName)
{
    return downloadCommon(fileName, true) ? true : false;
}
