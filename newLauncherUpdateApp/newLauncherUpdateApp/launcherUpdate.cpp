
#include "launcherUpdate.h"
#include "webDownloader.h"
#include "systemFileParser\iniFileMan.h"
#include "common/utils/pathManager.h"
#include "common/include/incFilesystem.h"
#include "common/utils/stringhelper.h"

#include <windows.h>

using namespace _StringHelper;

static constexpr auto s_updateKey = "UPDATE";
static constexpr auto s_paramKey = "PARAM";

LauncherUpdate::LauncherUpdate()
{
    m_download = std::make_unique<WebDownloader>();
    m_download->SetRepository("logviewerwindow2");
    m_download->SetUserName("happysoft3");
    m_state = "ok";
}

LauncherUpdate::~LauncherUpdate()
{ }

std::unique_ptr<IniFileMan> LauncherUpdate::fetchUpdateInfo(const std::string &fn)
{
    m_download->SetPath("master/Release/update_data.txt");
    std::string upfile;

    if (!m_download->Download(fn, upfile))
        return{ };

    auto ini = std::make_unique<IniFileMan>();

    if (!ini->ReadIni(upfile))
        return{ };

    if (NAMESPACE_FILESYSTEM::exists(upfile))
        NAMESPACE_FILESYSTEM::remove(upfile);

    return ini;
}

bool LauncherUpdate::needUpdate(IniFileMan &ini, const std::string &cver)
{
    std::string wver;

    if (!ini.GetItemValue(s_updateKey, "VERSION", wver))
        return false; //Todo. 오류로 간주해야됨

    return wver != cver;
}

bool LauncherUpdate::removePreviousFile(const std::string &fn)
{
    std::string cn = m_prevAppName;
    std::string nn = fn;

    std::transform(cn.cbegin(), cn.cend(), cn.begin(), [](const char &c) { return tolower(c); });
    std::transform(nn.cbegin(), nn.cend(), nn.begin(), [](const char &c) { return tolower(c); });
    if (cn != nn)
    {
        if (NAMESPACE_FILESYSTEM::exists(cn))
        {
            NAMESPACE_FILESYSTEM::remove(cn);
            NAMESPACE_FILESYSTEM::rename(nn, m_appName);
            return true;
        }
        //m_appName = nn;
    }
    return false;
}

bool LauncherUpdate::downloadLauncher(IniFileMan &ini)
{
    static constexpr auto launcherKey = "LAUNCHER";
    std::string launcherPath;
    PathManager pathMan;

    if (ini.GetItemValue(s_updateKey, launcherKey, launcherPath))
    {
        pathMan.SetUrl(launcherPath);
        m_download->SetPath(launcherPath);

        if (!m_download->Download(pathMan.SubName(), launcherPath))
            return false;
        
        removePreviousFile(launcherPath);
        return true;
    }
    return false;
}

bool LauncherUpdate::eraseBeforeDownload(const std::string &fn)
{
    if (NAMESPACE_FILESYSTEM::exists(fn))
        return NAMESPACE_FILESYSTEM::remove(fn);
    return false;
}

void LauncherUpdate::downloadRest(IniFileMan &ini)
{
    static constexpr auto fileKey = "FILE";
    int count = 0;
    std::string item;
    PathManager pathMan;

    while (ini.GetItemValue(s_updateKey, stringFormat("%s%d", fileKey, ++count), item))
    {
        if (item.empty())
            return;

        pathMan.SetUrl(item);
        m_download->SetPath(item);
        m_download->DownloadOverwrite(pathMan.SubName());
    }
}

void LauncherUpdate::runApp(const std::string &param)
{
    SHELLEXECUTEINFOA execInfo = { };

    execInfo.cbSize = sizeof(SHELLEXECUTEINFOA);
    execInfo.lpFile = m_appName.c_str();
    execInfo.lpParameters = param.c_str();
    execInfo.nShow = SW_SHOW;
    execInfo.lpVerb = "open";
    ::ShellExecuteExA(&execInfo);
}

bool LauncherUpdate::ReadParams(const std::string &paramFilename)
{
    IniFileMan ini;

    if (!ini.ReadIni(paramFilename))
        return false;

    std::string ver;

    if (!ini.GetItemValue(s_paramKey, "VERSION", ver))
        return false;

    std::string app;
    if (!ini.GetItemValue(s_paramKey, "APPNAME", app))
        return false;

    std::string prevApp;
    if (!ini.GetItemValue(s_paramKey, "CAPP", prevApp))
        return false;

    m_currentVersion = ver;
    m_appName = app;
    m_prevAppName = stringFormat("%s.exe", prevApp);
    return true;
}

bool LauncherUpdate::putStateMessage(const std::string &message, bool ret)
{
    m_state = message;
    return ret;
}

bool LauncherUpdate::updateImpl()
{
    auto ini = fetchUpdateInfo("update_data.txt");

    if (!ini)
        return putStateMessage("fail got update txt",false);

    if (needUpdate(*ini, m_currentVersion))
    {
        if (!downloadLauncher(*ini))
            return putStateMessage("launcher down fail", false);

        downloadRest(*ini);
        m_download.reset();
    }
    return true;
}

void LauncherUpdate::DoUpdate()
{
    if (!updateImpl())
    {
        m_appName = m_prevAppName;
        runApp(" -invalid");
        return;
    }
    runApp(" -pass");
}

const char *LauncherUpdate::GetState() const
{
    if (m_state.empty())
        return "(empty)";

    return m_state.c_str();
}
