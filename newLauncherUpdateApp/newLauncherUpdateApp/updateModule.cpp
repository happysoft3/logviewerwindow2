
#include "updateModule.h"
#include "launcherUpdate.h"
#include "common/utils/pathManager.h"
#include "common/include/incFilesystem.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

class UpdateModule::Impl
{
private:
    std::unique_ptr<LauncherUpdate> m_update;

public:
    Impl()
    {
        m_update = std::make_unique<LauncherUpdate>();
    }
    ~Impl()
    { }

public:
    LauncherUpdate *operator->() const
    {
        return m_update.get();
    }
};

UpdateModule::UpdateModule()
{
    m_impl = std::make_unique<Impl>();
}

UpdateModule::~UpdateModule()
{ }

void UpdateModule::recycleParamFileName(const std::string &prefix, std::function<void(const std::string &)>&&f)
{
    int dupCounter = 0;
    auto pathMan = std::make_unique<PathManager>();
    std::string ext, sub, _d;

    pathMan->SetUrl(prefix);
    pathMan->GetSubNameAndExt(sub, ext);
    while (NAMESPACE_FILESYSTEM::exists(pathMan->FullPath()))
    {
        f(pathMan->FullPath());
        pathMan->GetSubNameAndExt(_d, ext);
        pathMan->SetUrl(stringFormat("%s(%d)%s", sub, ++dupCounter, ext));
    }
}

std::string UpdateModule::loadParamFileName(const std::string &prefix)
{
    std::string ret;

    recycleParamFileName(prefix, [&ret](const std::string &s) { ret = s; });
    return ret;
}

void UpdateModule::cleanupParams(const std::string &prefix)
{
    recycleParamFileName(prefix, [](const std::string &s) { NAMESPACE_FILESYSTEM::remove(s); });
}

const char *const UpdateModule::DoUpdate()
{
    static constexpr auto param_prefix = "param.txt";
    if ((*m_impl)->ReadParams(loadParamFileName( param_prefix)) )
        (*m_impl)->DoUpdate();
    cleanupParams(param_prefix);
    return (*m_impl)->GetState();
}

UpdateModuleInterface *UpdateModule::MakeInstance()
{
    return new UpdateModule;
}

