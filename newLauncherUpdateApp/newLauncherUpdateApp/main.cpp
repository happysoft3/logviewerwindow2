
//#include "webDownloader.h"
#include "launcherUpdate.h"
#include <iostream>

#pragma comment(lib, "common")
#pragma comment(lib, "systemfileparser")

//main argc 에서 버전을 받는다
//gitlab 에서 최신버전을 확인한다
//버전을 비교하고, 아니면은 파일을 내려받는다
//완료 후, 런처를 켜준다
int main()
{
    /*WebDownloader down;

    down.SetPath("master/eud_project/backupMaps/deadland.map");
    down.SetRepository("eud-maps-project");
    down.SetUserName("happysoft3");
    down.Download("mymap.map");
    std::cout << std::endl << down.LastError() << std::endl;*/
    LauncherUpdate update;

    if (update.ReadParams("param.txt"))
        update.DoUpdate();
    
    return 0;
}
