
#ifndef UPDATE_MODULE_H__
#define UPDATE_MODULE_H__

#include "updateModuleInterface.h"
#include <memory>
#include <functional>

class UpdateModule : public UpdateModuleInterface
{
    class Impl;
private:
    std::unique_ptr<Impl> m_impl;

public:
    UpdateModule();
    ~UpdateModule() override;

private:
    void recycleParamFileName(const std::string &prefix, std::function<void(const std::string &)>&&f);
    std::string loadParamFileName(const std::string &prefix);
    void cleanupParams(const std::string &prefix);
    const char *const DoUpdate() override;

public:
    EXPORT_OBJECT static UpdateModuleInterface *MakeInstance();
};

#endif

