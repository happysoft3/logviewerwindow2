
#ifndef LOG_NOTIFIER_H__
#define LOG_NOTIFIER_H__

#include "common/ccobject/ccobject.h"

class ConsoleLogItem;

class LogNotifier : public CCObject
{
private:
    std::list<std::weak_ptr<ConsoleLogItem>> m_logQueue;

public:
    LogNotifier(CCObject *parent = nullptr);
    ~LogNotifier() override;

    void Append(std::weak_ptr<ConsoleLogItem> log);
    void SlotNotifyLog(bool forced);

public: DECLARE_SIGNAL(OnReleaseLogList, std::list<std::weak_ptr<ConsoleLogItem>>)
public: DECLARE_SIGNAL(RequestForceWriteLog)
};

#endif

