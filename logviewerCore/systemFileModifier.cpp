
#include "systemFileModifier.h"
#include "systemfileparser/iniFileMan.h"
#include "common/include/incFilesystem.h"

#include <sstream>
#include <fstream>

static constexpr char *s_sysMainKey = "systemKey";
static constexpr char *s_sysSubKeyMaxLog = "maxLogCount";
static constexpr char *s_sysSubKeyAutologPath = "autologPath";

static constexpr char *s_launcherKey = "launcher";
static constexpr char *s_launcherSubkeyGamePath = "gamePath";
static constexpr char *s_launcherSubkeyRunParams = "runParams";
static constexpr char *s_launcherSubkeyDownloadMap = "downloadMap";

#pragma comment(lib, "systemfileparser.lib")

SystemFileModifier::SystemFileModifier(const std::string &sysFile)
{
    m_sysFile = sysFile;
}

SystemFileModifier::~SystemFileModifier()
{ }

bool SystemFileModifier::MakeFile()
{
    std::ofstream file(m_sysFile);

    if (!file.is_open())
        return false;

    file.close();

    std::unique_ptr<IniFileMan> ini(new IniFileMan);

    ini->SetItemValue(s_sysMainKey, s_sysSubKeyMaxLog, 0);
    ini->SetItemValue(s_sysMainKey, s_sysSubKeyAutologPath, "autolog");
    ini->SetItemValue(s_launcherKey, s_launcherSubkeyGamePath, "");
    return ini->WriteIni(m_sysFile);
}

bool SystemFileModifier::OpenImpl()
{
    std::unique_ptr<IniFileMan> ini(new IniFileMan);

    if (!ini->ReadIni(m_sysFile))
        return false;

    m_iniFile = std::move(ini);
    return true;
}

bool SystemFileModifier::Open(bool tryAgain)
{
    if (m_sysFile.empty())
        return false;

    switch (tryAgain)
    {
    case false:
        return OpenImpl();

    case true:
        if (!OpenImpl())
            return MakeFile() ? OpenImpl() : false;
    default: //warning. some control flowing doesn't return a value.
        return true;
    }
}

bool SystemFileModifier::GetMaxAutoLog(size_t &maxLogCount)
{
    if (!m_iniFile)
        return false;

    return m_iniFile->GetItemValue(s_sysMainKey, s_sysSubKeyMaxLog, maxLogCount);
}

bool SystemFileModifier::SetMaxAutoLog(const size_t &maxLogCount)
{
    if (!m_iniFile)
        return false;

    return m_iniFile->SetItemValue(s_sysMainKey, s_sysSubKeyMaxLog, maxLogCount);
}

bool SystemFileModifier::GetAutologPath(std::string &path)
{
    return m_iniFile ? m_iniFile->GetItemValue(s_sysMainKey, s_sysSubKeyAutologPath, path) : false;
}

bool SystemFileModifier::SetAutologPath(const std::string &path)
{
    return m_iniFile ? m_iniFile->SetItemValue(s_sysMainKey, s_sysSubKeyAutologPath, path) : false;
}

bool SystemFileModifier::GetGamePath(std::string &path)
{
    std::string app;

    if (!m_iniFile)
        return false;
    if (!m_iniFile->GetItemValue(s_launcherKey, s_launcherSubkeyGamePath, app))
        return false;

    if (!NAMESPACE_FILESYSTEM::exists(app))
        return false;
    path = app;
    return true;
}

bool SystemFileModifier::SetGamePath(const std::string &path)
{
    if (!m_iniFile)
        return false;
    if (!NAMESPACE_FILESYSTEM::exists(path))
        return false;

    return m_iniFile->SetItemValue(s_launcherKey, s_launcherSubkeyGamePath, path);
}

bool SystemFileModifier::GetRunParams(std::string &params)
{
    if (!m_iniFile)
        return false;

    return m_iniFile->GetItemValue(s_launcherKey, s_launcherSubkeyRunParams, params);
}

bool SystemFileModifier::SetRunParams(const std::string &params)
{
    if (!m_iniFile)
        return false;
    return m_iniFile->SetItemValue(s_launcherKey, s_launcherSubkeyRunParams, params);
}

bool SystemFileModifier::GetMapdownloadSpeed(int &value)
{
    if (!m_iniFile)
        return false;

    std::string svar;
    if (! m_iniFile->GetItemValue(s_launcherKey, s_launcherSubkeyDownloadMap, svar))
        return false;

    if (svar.length() > 4)
        svar.resize(4);

    for (const auto &c : svar)
    {
        switch (c)
        {
        case '0':case '1':case '2':case '3':case '4':
        case '5':case '6':case '7':case '8':case '9':
            break;

        default:
            return false;
        }
    }

    std::stringstream ss(svar);

    ss >> value;
    return true;
}

void SystemFileModifier::Save()
{
    if (m_iniFile)
        m_iniFile->WriteIni(m_sysFile);
}