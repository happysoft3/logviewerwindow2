
#ifndef STRING_ENCORD_UTIL_H__
#define STRING_ENCORD_UTIL_H__

#include <string>

class StringEncordUtil
{
public:
    static bool AnsiToUnicode(const std::string &src, std::wstring &dest);
    static bool UnicodeToAnsi(const std::wstring &src, std::string &dest);
    static bool UnicodeToUtf8(const std::wstring &src, std::string &dest);
    static bool Utf8ToUnicode(const std::string &src, std::wstring &dest);
};

#endif


