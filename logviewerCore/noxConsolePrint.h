
#ifndef NOX_CONSOLE_PRINT_H__
#define NOX_CONSOLE_PRINT_H__

#include "processCodePage.h"
#include <array>

class NoxConsolePrint : public ProcessCodePage
{
public:
    enum class PrintColorId
    {
        color_invalid,
        color_black,
        color_grey,
        color_white,
        color_light_white,
        color_dark_red,
        color_bright_red,
        color_pink,
        color_green,
        color_second_black,
        color_lime,
        color_dark_blue,
        color_blue,
        color_sky_blue,
        color_orange,
        color_yellow,
        color_pale_yellow,
        color_second_grey
    };
private:
    std::array<uint16_t, 256> m_printMsg;
    int m_printColorId;

public:
    NoxConsolePrint();
    ~NoxConsolePrint() override;

private:
    static void PrintConsoleImpl(const uint32_t *args);
    static void PrintConsoleSub();
    
    uint32_t ParamLength() const override
    {
        return m_printMsg.max_size() + sizeof(m_printColorId);
    }

    uint32_t FieldLength() const override;
    uint32_t *ExecutionBase() const override;

    bool BindParams() override;
    NoxConsolePrint *Clone() override;

public:
    void SetPrintMessage(const std::string &printMsg);
    void SetPrintColor(PrintColorId colorId);
};

#endif

