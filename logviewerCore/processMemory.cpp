
#include "processMemory.h"
#include "win/wndHandle.h"
#include "processCodePage.h"
#include <windows.h>

ProcessMemory::ProcessMemory()
    : BinaryStream(16384)
{ }

ProcessMemory::~ProcessMemory()
{
    doClose();
}

bool ProcessMemory::checkValid()
{
    int check = 0;

    if (!GetMemory(validationCheck::PTR, check))
        return false;

    return check == validationCheck::VALUE;
}

void ProcessMemory::doClose()
{
    if (m_handle)
        m_handle.reset();
    if (m_executor)
        m_executor.reset();
}

bool ProcessMemory::IsOpened()
{
    return checkValid();
}

void ProcessMemory::Bind(std::unique_ptr<WndHandle> handle)
{
    if (m_handle)
        return;

    m_handle = std::move(handle);
    if (!checkValid())
    {
        m_handle.reset();
        return;
    }
}

bool ProcessMemory::ReadMemoryImpl(const uint32_t &targetAddress, const size_t &size)
{
    if (!m_handle)
        return false;

    uint8_t *stream = nullptr;
    size_t length = 0;

    BufferResize(size);
    GetStreamInfo(stream, length);
    return ReadProcessMemory(*m_handle, reinterpret_cast<LPCVOID>(targetAddress), stream, length, nullptr) & true;
}

bool ProcessMemory::WriteMemoryImpl(const uint32_t &targetAddress)
{
    if (!m_handle)
        return false;

    uint8_t *stream = nullptr;
    size_t length = 0;

    GetStreamInfo(stream, length);
    return WriteProcessMemory(*m_handle, reinterpret_cast<LPVOID>(targetAddress), stream, length, nullptr) & true;
}

bool ProcessMemory::externExecuteImpl(std::shared_ptr<ProcessCodePage> codepage)
{
    uint32_t pageSize = codepage->FieldLength() + codepage->ParamLength();

    if (!pageSize)
        return false;

    uint32_t *vStream = static_cast<uint32_t *>(::VirtualAllocEx(*m_handle, nullptr, pageSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE));
    uint32_t *vParam = reinterpret_cast<uint32_t *>(reinterpret_cast<uint8_t *>(vStream) + codepage->FieldLength());

    WriteProcessMemory(*m_handle, vStream, codepage->ExecutionBase(), codepage->FieldLength(), nullptr);
    if (codepage->ParamLength())
    {
        uint8_t *params = nullptr;
        size_t paramlength = 0;

        codepage->GetStreamInfo(params, paramlength);

        ::WriteProcessMemory(*m_handle, vParam, params, paramlength, nullptr);
    }
    m_executor = std::make_unique<WndHandle>(::CreateRemoteThread(*m_handle, nullptr, 0, reinterpret_cast<LPTHREAD_START_ROUTINE>(vStream), vParam, CREATE_SUSPENDED, nullptr));
    ::ResumeThread(*m_executor);
    ::WaitForSingleObject(*m_executor, 3000);
    ::VirtualFreeEx(*m_handle, vStream, 0, MEM_RELEASE);
    m_executor.reset();
    return true;
}

bool ProcessMemory::ExternExecute(std::shared_ptr<ProcessCodePage> codepage)
{
    if (!m_handle)
        return false;
    if (!codepage)
        return false;

    if (m_executor)
    {
        codepage->DetermineReuable(true);
        return false;
    }
    return codepage->ExecutionBase() ? externExecuteImpl(codepage) : false;
}

uint32_t ProcessMemory::AllocMemory(uint32_t size)
{
    if (!m_handle)
        return 0;

    return reinterpret_cast<uint32_t>(::VirtualAllocEx(*m_handle, nullptr, size, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE));
}
