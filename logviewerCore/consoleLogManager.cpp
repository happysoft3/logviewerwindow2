
#include "consoleLogManager.h"
#include "autoConsoleLog.h"
#include "logNotifier.h"
#include "consoleLogItemList.h"
#include "consoleLogItem.h"

ConsoleLogManager::ConsoleLogManager()
    : CCObject()
{
    m_autoLog = std::make_unique<AutoConsoleLog>(this);
    m_addedCount = 0;

    m_OnEndedAddItem.Connection(&AutoConsoleLog::SlotQueueAddedCompleted, m_autoLog.get());
}

ConsoleLogManager::~ConsoleLogManager()
{ }

void ConsoleLogManager::SetItemListPtr(std::weak_ptr<ConsoleLogItemList> itemListPtr)
{
    if (itemListPtr.expired())
        return;

    m_itemListPtr = itemListPtr;
}

void ConsoleLogManager::SetLogNotifier(std::weak_ptr<LogNotifier> notifier)
{
    auto logNotifier = notifier.lock();

    if (!logNotifier)
        return;

    m_OnEndedAddItem.Connection(&LogNotifier::SlotNotifyLog, logNotifier.get());
    logNotifier->RequestForceWriteLog().Connection(&ConsoleLogManager::SlotAutoLogForceWrite, this);
    m_logNotifier = notifier;
}

bool ConsoleLogManager::FetchLogItem()
{
    auto itemListPtr = m_itemListPtr.expired() ? nullptr : m_itemListPtr.lock();

    if (!itemListPtr)
        return false;

    do
    {
        std::unique_ptr<ConsoleLogItem> logItem;

        if (!itemListPtr->PopItem(logItem))
            break;

        m_logList.push_back(std::move(logItem));
        m_autoLog->Append(m_logList.back());
        {
            auto logNotifier = m_logNotifier.lock();

            if (logNotifier)
                logNotifier->Append(m_logList.back());
        }
        ++m_addedCount;
    }
    while (true);
    return true;
}

void ConsoleLogManager::SlotListContained(const size_t count)
{
    if (FetchLogItem())
    {
        if (!m_addedCount)
            return;

        m_OnEndedAddItem.QueueEmit( false);
        m_addedCount = 0;
    }
}

void ConsoleLogManager::SlotAutoLogForceWrite()
{
    m_OnEndedAddItem.Emit(true);
}

std::string ConsoleLogManager::ParameterModifier(const std::string &paramType, const std::string &setValue)
{
    if (paramType == "path")
        return m_autoLog->ModifierAutoLogPath(setValue);
    else if (paramType == "limit")
        return m_autoLog->ModifierAutoLogLimit(setValue);

    return{ };
}
