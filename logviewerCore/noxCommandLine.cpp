
#include "noxCommandLine.h"
#include "stringEncordUtil.h"
#include <iostream>

NoxCommandLine::NoxCommandLine()
    : ProcessCodePage()
{
    m_cmdMsg.fill(0);
    m_cmdMode = 0;
}

NoxCommandLine::~NoxCommandLine()
{ }

void NoxCommandLine::ExecuteCommandImpl(const uint32_t *args)
{
    using vfunction_proto_type = int (_cdecl *)(const uint16_t *, int);
    const int mode = static_cast<int>(args[0]);
    const uint16_t *cmd = reinterpret_cast<const uint16_t *>(&args[1]);
    vfunction_proto_type vinvokable = reinterpret_cast<vfunction_proto_type>(0x443C80);

    vinvokable(cmd, mode);
}

void NoxCommandLine::ExecuteCommandSub()
{
    std::cout << "NoxCommandLine::ExecuteCommandSub()" << std::endl;
}

uint32_t NoxCommandLine::FieldLength() const
{
    return reinterpret_cast<uint32_t>(&NoxCommandLine::ExecuteCommandSub) - reinterpret_cast<uint32_t>(&NoxCommandLine::ExecuteCommandImpl);
}

uint32_t *NoxCommandLine::ExecutionBase() const
{
    return reinterpret_cast<uint32_t *>(&NoxCommandLine::ExecuteCommandImpl);
}

bool NoxCommandLine::BindParams()
{
    BufferResize(ParamLength());

    try
    {
        WriteCtx(m_cmdMode);
        for (const auto &us : m_cmdMsg)
            WriteCtx(us);
    }
    catch (const bool &fail)
    {
        return fail;
    }
    return true;
}

NoxCommandLine *NoxCommandLine::Clone()
{
    return new NoxCommandLine(*this);
}

bool NoxCommandLine::SetCommandMessage(const std::string &cmdMsg, int mode)
{
    if (cmdMsg.length() >= m_cmdMsg.max_size())
        return false;

    m_cmdMode = mode;

    std::wstring dest;

    StringEncordUtil::AnsiToUnicode(cmdMsg, dest);
    std::copy(dest.cbegin(), dest.cend(), m_cmdMsg.begin());
    return true;
}
