
#ifndef CONSOLE_LOG_MANAGER_H__
#define CONSOLE_LOG_MANAGER_H__

#include "common/ccobject/ccobject.h"

class ConsoleLogItem;
class ConsoleLogItemList;
class AutoConsoleLog;
class LogNotifier;

class ConsoleLogManager : public CCObject
{
private:
    std::list<std::shared_ptr<ConsoleLogItem>> m_logList;
    std::weak_ptr<ConsoleLogItemList> m_itemListPtr;
    std::unique_ptr<AutoConsoleLog> m_autoLog;
    std::weak_ptr<LogNotifier> m_logNotifier;
    size_t m_addedCount;

public:
    ConsoleLogManager();
    ~ConsoleLogManager() override;

public:
    void SetItemListPtr(std::weak_ptr<ConsoleLogItemList> itemListPtr);
    void SetLogNotifier(std::weak_ptr<LogNotifier> notifier);

private:
    bool FetchLogItem();

public:
    void SlotListContained(const size_t count);
    void SlotAutoLogForceWrite();

    std::string ParameterModifier(const std::string &paramType, const std::string &setValue);

private: DECLARE_SIGNAL(OnEndedAddItem, bool)   //forced mode when true
};

#endif

