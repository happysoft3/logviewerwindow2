
#ifndef CONSOLE_LOG_READER_H__
#define CONSOLE_LOG_READER_H__

#include "common/ccobject/ccobject.h"
#include <vector>

/**
 *\ base= 0069fe4c
 *\ info =  *((*0069fe4c)+32)
 *\ logptr_base = *(info+24)   //single size= 524
 *\ line_count = *((char *)info+44)
*/

class ConsoleLogItemList;
class ProcessMemory;
class ConsoleLogManager;
class LogNotifier;

class ConsoleLogReader : public CCObject
{
    static constexpr uint32_t console_base_offset = 0x69fe4c;

public:
    enum class LogTaskResult
    {
        DONE,
        DISCONNECT,
        LOGPTR_NULL
    };

private:
    std::shared_ptr<ProcessMemory> m_processHandler;
    std::vector<uint8_t> m_rawLogStorage;
    std::shared_ptr<ConsoleLogItemList> m_itemList;
    std::shared_ptr<ConsoleLogManager> m_logManager;
    std::shared_ptr<LogNotifier> m_logNotifier;

public:
    explicit ConsoleLogReader();
    ~ConsoleLogReader() override;

private:
    bool GetConsoleLogInfo(uint32_t &consoleDetail);
    bool GetConsoleLogLineCount(int &lineCount);
    bool GetConsoleLogAddress(uint32_t &consolePtr);
    uint32_t GetConsoleLogAddressWithLine(const int &lineCount);
    bool IsNewLog(const uint32_t &logptr, uint16_t &lowbit);
    void MarkNewLog(const uint32_t &logptr, const uint16_t &lowbit);
    bool GetConsoleLogRawStream(const uint32_t &logptr);
    bool GetConsoleLogPredict();

    void AppendLogItem();

public:
    void Initialize(std::shared_ptr<ConsoleLogManager> logManager);
    LogTaskResult LogReaderTask(std::shared_ptr<ProcessMemory> proc);
    LogNotifier *GetNotifier();
};

#endif

