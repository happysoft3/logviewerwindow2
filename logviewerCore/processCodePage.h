
#ifndef PROCESS_CODE_PAGE_H__
#define PROCESS_CODE_PAGE_H__

#include "binaryStream.h"

class ProcessCodePage : public BinaryStream
{
private:
    bool m_canReuse;

public:
    ProcessCodePage();
    ~ProcessCodePage() override;

    virtual uint32_t *ExecutionBase() const
    {
        return 0;
    }

    virtual uint32_t FieldLength() const
    {
        return 0;
    }

    virtual uint32_t ParamLength() const
    {
        return 0;
    }

    virtual bool BindParams()
    {
        return false;
    }

    virtual ProcessCodePage *Clone();

    bool CanReuse() const
    {
        return m_canReuse;
    }
    void DetermineReuable(bool state);
};

#endif

