
#include "processCodePage.h"

ProcessCodePage::ProcessCodePage()
    : BinaryStream(1024)
{
    m_canReuse = false;
}

ProcessCodePage::~ProcessCodePage()
{ }

ProcessCodePage *ProcessCodePage::Clone()
{
    return new ProcessCodePage(*this);
}

void ProcessCodePage::DetermineReuable(bool state)
{
    if (state != m_canReuse)
        m_canReuse = state;
}
