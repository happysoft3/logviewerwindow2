
#ifndef CONSOLE_LOG_ITEM_LIST_H__
#define CONSOLE_LOG_ITEM_LIST_H__

#include "common/ccobject/ccobject.h"
#include <future>
#include <mutex>

class ConsoleLogItem;

class ConsoleLogItemList : public CCObject
{
private:
    std::list<std::unique_ptr<ConsoleLogItem>> m_logItemList;
    std::vector<std::future<bool>> m_notifiers;
    size_t m_notifierOrder;

public:
    ConsoleLogItemList();
    ~ConsoleLogItemList() override;

public:
    void Append(std::unique_ptr<ConsoleLogItem> &&item);
    bool IsContained() const;

private:
    bool NotifierImpl(std::weak_ptr<CCObject::Core> alive);
    void JoinNotifier(size_t &currentOrder);

public:
    void Notify();
    bool PopItem(std::unique_ptr<ConsoleLogItem> &dest);

private:
    DECLARE_SIGNAL(OnContainedNotify, size_t)

private:
    std::mutex m_lock;
};

#endif

