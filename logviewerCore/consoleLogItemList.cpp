
#include "consoleLogItemList.h"
#include "consoleLogItem.h"

ConsoleLogItemList::ConsoleLogItemList()
    : CCObject()
{
    m_notifiers.resize(32);
    m_notifierOrder = 0;
}

ConsoleLogItemList::~ConsoleLogItemList()
{ }

void ConsoleLogItemList::Append(std::unique_ptr<ConsoleLogItem> &&item)
{
    std::unique_lock<std::mutex> locker(m_lock);
    m_logItemList.push_front(std::move(item));
}

bool ConsoleLogItemList::IsContained() const
{
    return m_logItemList.size() ? true : false;
}

bool ConsoleLogItemList::NotifierImpl(std::weak_ptr<CCObject::Core> alive)
{
    auto aliveChecker = alive.expired() ? nullptr : alive.lock();

    if (!aliveChecker)
        return false;

    m_OnContainedNotify.Emit(m_logItemList.size());
    return true;
}

void ConsoleLogItemList::JoinNotifier(size_t &currentOrder)
{
    m_notifierOrder = (m_notifierOrder >= m_notifiers.size()) ? 0 : m_notifierOrder;

    if (m_notifiers[m_notifierOrder].valid())
        m_notifiers[m_notifierOrder].get();

    currentOrder = m_notifierOrder;
    ++m_notifierOrder;
}

void ConsoleLogItemList::Notify()
{
    size_t currentOrder = 0;

    JoinNotifier(currentOrder);
    m_notifiers[currentOrder] = std::async(std::launch::async, [this](std::weak_ptr<CCObject::Core> alive) { return this->NotifierImpl(alive); }, m_core);
}

bool ConsoleLogItemList::PopItem(std::unique_ptr<ConsoleLogItem> &dest)
{
    std::unique_lock<std::mutex> locker(m_lock);

    if (m_logItemList.empty())
        return false;

    dest = std::move(m_logItemList.front());
    m_logItemList.pop_front();
    return true;
}

