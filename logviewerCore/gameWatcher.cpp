
#include "gameWatcher.h"
#include "consoleLogReader.h"
#include "consoleLogManager.h"
#include "outTaskSend.h"
#include "logNotifier.h"
//#include "patch/patchFastMapDownload.h"
#include "patch/patchMapDownloadFix.h"
#include "patch/dumpLoader.h"
#include "processMemory.h"
#include "execute/noxHandleThread.h"
#include "common/taskthread/taskResultHash.h"
#include "common/taskthread/taskQueue.h"
#include "reg/noxRegistry.h"
#include "pathManagerEx.h"

static constexpr uint32_t s_watcher_frame_counter = 30;

GameWatcher::GameWatcher()
    : CCObject()
{
    m_downloadMap = 0;
}

GameWatcher::~GameWatcher()
{
    m_process.reset();
}

bool GameWatcher::DoLogTask()
{
    auto taskResult = m_logReader->LogReaderTask(m_process);
    
    if (taskResult == ConsoleLogReader::LogTaskResult::DISCONNECT)
        return false;

    return true;
}

bool GameWatcher::watcherWork(std::weak_ptr<int> state)
{
    for (;;)
    {
        {
            auto s = state.lock();

            if (s)
            {
                if (*s == 0)
                    return false;
            }
        }

        if (!m_process)
            return false;

        if (DoLogTask())
            m_sender->DequeueTaskOne(m_process);

        std::this_thread::sleep_for(std::chrono::milliseconds(s_watcher_frame_counter));
    }
    return true;
}

GameWatcher::GameState GameWatcher::doConnect(std::shared_ptr<ProcessMemory> proc)
{
    if (!proc)
        return GameState::Failure;

    if (proc->IsOpened())
    {
        m_process = proc;
        return GameState::Succeed;
    }
    return GameState::Failure;
}

bool GameWatcher::WatcherInit()
{
    m_sender = std::make_unique<OutTaskSend>(this);
    m_logReader = std::make_unique<ConsoleLogReader>();
    m_logManager = std::make_shared<ConsoleLogManager>();
    m_logReader->Initialize(m_logManager);
    return true;
}

LogNotifier *GameWatcher::GetLogNotifier()
{
    return m_logReader->GetNotifier();
}

void GameWatcher::startLogthread()
{
    if (m_logThreadState)
        return;

    m_logThreadState = std::make_shared<int>(1);
    std::packaged_task<bool(std::weak_ptr<int>)> task([this](std::weak_ptr<int> state) { return this->watcherWork(state); });
    m_workResult = task.get_future();
    std::thread logThread(std::move(task), m_logThreadState);

    logThread.detach();
}

void GameWatcher::ForceWriteAutoLog()
{
    LogNotifier *notifier = GetLogNotifier();

    if (notifier == nullptr)
        return;

    notifier->RequestForceWriteLog().Emit();
}

void GameWatcher::CommitOutTask(const std::string &taskName, const std::string &argMessage)
{
    if (taskName == "chat")
        m_sender->MakeTaskChatMessage(argMessage);
    else if (taskName == "comd")
        m_sender->MakeTaskExecCmd(argMessage);
}

std::string GameWatcher::AccessLogParam(const std::string &paramType, const std::string &value)
{
    return m_logManager->ParameterModifier(paramType, value);
}

std::unique_ptr<PathManager> GameWatcher::GetAlternativePath() const
{
    NoxRegistry reg;

    if (reg.LoadPath())
        return reg.GetPath();
    return{ };  //실패 시 오류처리 필요
}

void GameWatcher::slotEndProcess()
{
    if (m_handleThread)
        m_handleThread->GetResult();
    m_process.reset();
    m_OnDisconnected.Emit();
}

void GameWatcher::slotGotProcess(std::shared_ptr<ProcessMemory> proc)
{
    if (m_process)
        return;

    doConnect(proc);
    startLogthread();
    m_OnConnected.Emit();
}

bool GameWatcher::runAppDetail(const PathManagerEx &targPath, std::string &err)
{
    auto queue = std::make_shared<TaskQueue>();
    auto handleThread = std::make_unique<NoxHandleThread>();

    handleThread->OnProcessCreated().Connection(&GameWatcher::slotGotProcess, this);
    handleThread->OnProcessTerminated().Connection(&GameWatcher::slotEndProcess, this);

    if (! handleThread->PutRunPath(targPath))
    {
        err = "path is nil";
        return false;
    }
    m_taskQueue = queue;
    handleThread->Perform(queue);
    m_handleThread = std::move(handleThread);
    return true;
}

bool GameWatcher::RunApp(const PathManagerEx &targPath, std::string &errorMessage)
{
    if (m_taskQueue.expired())
    {
        if (!m_handleThread)
            return runAppDetail(targPath, errorMessage);
    }
    errorMessage = "already running";
    return false;
}

void GameWatcher::DoPatch()
{
    if (!IsRunning())
        return;

    DumpLoader dumper;

    //auto patchMapDownload = std::make_unique<PatchFastMapDownload>();
    auto patchMapDownload = std::make_unique<PatchMapDownloadFix>();

    //patchMapDownload->SetDownloadSpeed(static_cast<uint8_t>(m_downloadMap));
    dumper.Push(std::move(patchMapDownload));
    dumper.Dump(m_process);
}

void GameWatcher::TerminateAppCompletely()
{
    if (m_handleThread)
    {
        m_handleThread.reset();
        if (m_logThreadState)
        {
            m_logThreadState.reset();
            m_workResult.get();
        }
    }
}

bool GameWatcher::IsRunning() const
{
    if (m_handleThread)
        return true;
    if (m_taskQueue.expired())
        return false;
    return true;
}

