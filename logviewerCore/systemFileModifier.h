
#ifndef SYSTEM_FILE_MODIFIER_H__
#define SYSTEM_FILE_MODIFIER_H__

#include <string>
#include <memory>

class IniFileMan;

class SystemFileModifier
{
private:
    std::string m_sysFile;
    std::unique_ptr<IniFileMan> m_iniFile;

public:
    explicit SystemFileModifier(const std::string &sysFile = { });
    ~SystemFileModifier();

private:
    bool MakeFile();
    bool OpenImpl();

public:
    bool Open(bool tryAgain = true);
    bool GetMaxAutoLog(size_t &maxLogCount);
    bool SetMaxAutoLog(const size_t &maxLogCount);
    bool GetAutologPath(std::string &path);
    bool SetAutologPath(const std::string &path);
    bool GetGamePath(std::string &path);
    bool SetGamePath(const std::string &path);
    bool GetRunParams(std::string &params);
    bool SetRunParams(const std::string &params);
    bool GetMapdownloadSpeed(int &value);
    void Save();
};

#endif

