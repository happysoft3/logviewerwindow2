
#ifndef CFG_NODE_H__
#define CFG_NODE_H__

#include <memory>
#include <ostream>

class TokenizerToken;

class CfgNode
{
private:
    std::unique_ptr<TokenizerToken> m_cfgkey;
    std::unique_ptr<TokenizerToken> m_value;

public:
    CfgNode();
    explicit CfgNode(const std::string &key, const std::string &value);
    ~CfgNode();

public:
    const char *KeyName() const;
    void PutKey(std::unique_ptr<TokenizerToken> key, std::unique_ptr<TokenizerToken> value = { });
    void ReplaceValue(std::unique_ptr<TokenizerToken> value);
    void ChangeValueString(const std::string &s);
    void GetKeyValueString(std::string &key, std::string &value) const;
    void WriteOnBuffer(std::ostream &os);
};

#endif

