
#ifndef DEFAULT_CFG_COLLECTOR_H__
#define DEFAULT_CFG_COLLECTOR_H__

#include "cfgCollector.h"

class DefaultCfgCollector : public CfgCollector
{
public:
    DefaultCfgCollector();
    ~DefaultCfgCollector() override;

private:
    void writeDefaultSettings(const std::string &url);
    bool ReadCfg(const std::string &url) override;
};

#endif

