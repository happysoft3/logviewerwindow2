
#include "defaultCfgCollector.h"
#include <list>
#include <fstream>

static std::list<std::string> s_defList = {
    "SDL_ENABLED = 1",
    "HIDE_CONFIG = 0",
    "SDL_KEEPASPECTRATIO = 0",
    "SDL_DRIVER = 0",
    "SDL_MOVIES = 0",
    "SDL_FULLSCREEN = 1",
    "SDL_GAMMA = 100",
    "NOX_SLEEP = 1",
    "SDL_QUALITY = 2",
    "NOX_LOCK_TEXTURES = 1",
    "SERVER_ENABLED = 0",
    "SERVER_NOVIDEO = 0",
    "CRASHRPT_ENABLED = 1",
    "MULTIPLE_INSTANCES = 0",
    "MOUSE_SENSITIVITY = 100",
};


DefaultCfgCollector::DefaultCfgCollector()
    : CfgCollector()
{ }

DefaultCfgCollector::~DefaultCfgCollector()
{ }

void DefaultCfgCollector::writeDefaultSettings(const std::string &url)
{
    std::ofstream file(url);

    for (const auto &s : s_defList)
        file << s << std::endl;
}

bool DefaultCfgCollector::ReadCfg(const std::string &url)
{
    if (CfgCollector::ReadCfg(url))
        return true;
    writeDefaultSettings(url);
    return CfgCollector::ReadCfg(url);
}