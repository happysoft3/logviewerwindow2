
#include "cfgParser.h"
#include "cfgNode.h"
#include "common/utils/tokenizer/tokenizertoken.h"
#include <fstream>

class CfgParser::CfgParserReader
{
private:
    std::ifstream m_file;

public:
    CfgParserReader()
    { }

    ~CfgParserReader()
    { }

    std::ifstream *operator->()
    {
        return &m_file;
    }
    std::ifstream &operator*()
    {
        return m_file;
    }
};

CfgParser::CfgParser()
    : StringTokenizer()
{ }

CfgParser::~CfgParser()
{ }

bool CfgParser::skipHypen(int c)
{
    if (peek() != '-')
        return false;

    while (c != EOF)
    {
        if (c != '\n')
        {
            push(c);
            break;
        }
        c = pop();
    }
    return true;
}

std::unique_ptr<TokenizerToken> CfgParser::doReadToken(int c)
{
    switch (c)
    {
    case '-':
        if (skipHypen(c))
            return std::make_unique<SpaceToken>();
    default:
        return StringTokenizer::doReadToken(c);
    }
}

bool CfgParser::readOneline()
{
    std::string buff;
    std::string s;
    
    buff.reserve(256);
    std::getline(**m_reader, s);
    //(**m_reader) >> s;
    if (s.empty())
        return false;
    buff += s;
    /*(**m_reader) >> s;
    buff += s;
    (**m_reader) >> s;
    buff += s;*/
    resetSeek();
    StreamPut(buff);
    return true;
}

std::unique_ptr<CfgNode> CfgParser::fetchNodeOne()
{
    auto ident= ReadToken();

    if (!ident->TypeExpect(TokenizerToken::Type::IdentType))
        return{ };

    auto equal = ReadToken();

    if (strcmp(equal->String(), "="))
        return{ };

    auto node = std::make_unique<CfgNode>();

    node->PutKey(std::move(ident), ReadToken());
    return node;
}

std::unique_ptr<TokenizerToken> CfgParser::ReadToken()
{
    while (skipSpace());
    return StringTokenizer::ReadToken();
}

bool CfgParser::PutUrl(const std::string &url)
{
    auto rd = std::make_unique<CfgParserReader>();

    (*rd)->open(url);
    if ((*rd)->is_open())
    {
        m_reader = std::move(rd);
        return true;
    }
    return false;
}

std::unique_ptr<CfgNode> CfgParser::GetNode()
{
    if (!m_reader)
        return{ };

    if (!readOneline())
        return{ };

    return fetchNodeOne();
}

