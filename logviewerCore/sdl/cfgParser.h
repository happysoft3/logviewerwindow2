
#ifndef CFG_PARSER_H__
#define CFG_PARSER_H__

#include "common/utils/tokenizer/stringTokenizer.h"

class CfgNode;

class CfgParser : public StringTokenizer
{
    class CfgParserReader;
private:
    std::unique_ptr<CfgParserReader> m_reader;

public:
    CfgParser();
    ~CfgParser() override;

private:
    bool skipHypen(int c);
    std::unique_ptr<TokenizerToken> doReadToken(int c);
    bool readOneline();
    std::unique_ptr<CfgNode> fetchNodeOne();
    std::unique_ptr<TokenizerToken> ReadToken() override;

public:
    bool PutUrl(const std::string &url);
    std::unique_ptr<CfgNode> GetNode();
};

#endif

