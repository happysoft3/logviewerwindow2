
#include "cfgNode.h"
#include "common/utils/tokenizer/tokenizertoken.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

CfgNode::CfgNode()
{ }

CfgNode::CfgNode(const std::string &key, const std::string &value)
{
    m_cfgkey = std::make_unique<IdentToken>(key);
    m_value = std::make_unique<IdentToken>(value);
}

CfgNode::~CfgNode()
{ }

const char *CfgNode::KeyName() const
{
    if (!m_cfgkey)
        return "(null)";

    return m_cfgkey->String();
}

void CfgNode::PutKey(std::unique_ptr<TokenizerToken> key, std::unique_ptr<TokenizerToken> value)
{
    m_cfgkey = std::move(key);
    if (value)
        m_value = std::move(value);
}

void CfgNode::ReplaceValue(std::unique_ptr<TokenizerToken> value)
{
    if (value)
        m_value = std::move(value);
}

void CfgNode::ChangeValueString(const std::string &s)
{
    if (m_value)
        m_value->PutString(s);
}

void CfgNode::GetKeyValueString(std::string &key, std::string &value) const
{
    if (m_cfgkey)
        key = m_cfgkey->String();
    if (m_value)
        value = m_value->String();
}

void CfgNode::WriteOnBuffer(std::ostream &os)
{
    os << stringFormat("%s=%s\n", m_cfgkey->String(), m_value->String());
}

