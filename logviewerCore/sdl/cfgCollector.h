
#ifndef CFG_COLLECTOR_H__
#define CFG_COLLECTOR_H__

#include <string>
#include <map>
#include <memory>
#include <list>

class CfgNode;

class CfgCollector
{
private:
    std::string m_url;
    using cfg_node_list_ty = std::list<std::unique_ptr<CfgNode>>;
    cfg_node_list_ty m_nodeList;
    std::map<std::string, cfg_node_list_ty::iterator> m_nodeMap;

public:
    CfgCollector();
    virtual ~CfgCollector();

private:
    bool queryValue(const std::string &key, std::string &getValue, const char *setValue = nullptr);
    void putNode(const std::string &key, std::unique_ptr<CfgNode> node);
    void fetchNodeValue(CfgNode &node, std::string &getValue);
    void nodeChangeValue(CfgNode &node, const char *setValue);

public:
    virtual bool ReadCfg(const std::string &url = { });
    bool WriteCfg(const std::string &url = { });
    bool GetValue(const std::string &key, std::string &value);
    bool GetValueAsNumber(const std::string &key, int &nValue);
    bool SetValue(const std::string &key, const std::string &value);
    bool SetValueAsNumber(const std::string &key, const int &n);
};

#endif

