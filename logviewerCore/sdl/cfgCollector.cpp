
#include "cfgCollector.h"
#include "cfgParser.h"
#include "cfgNode.h"
#include "common/utils/tokenizer/tokenizertoken.h"
#include <fstream>
#include <sstream>
#include <iostream>

CfgCollector::CfgCollector()
{ }

CfgCollector::~CfgCollector()
{ }

void CfgCollector::fetchNodeValue(CfgNode &node, std::string &getValue)
{
    std::string getkey;

    node.GetKeyValueString(getkey, getValue);
}

void CfgCollector::nodeChangeValue(CfgNode &node, const char *setValue)
{
    if (setValue)
        node.ChangeValueString(setValue);
}

bool CfgCollector::queryValue(const std::string &key, std::string &getValue, const char *setValue)
{
    auto iter = m_nodeMap.find(key);

    if (iter == m_nodeMap.cend())
        return false;
    
    nodeChangeValue(**iter->second, setValue);
    fetchNodeValue(**iter->second, getValue);
    return true;
}

void CfgCollector::putNode(const std::string &key, std::unique_ptr<CfgNode> node)
{
    auto iter = m_nodeMap.find(key);

    if (iter == m_nodeMap.cend())
    {
        m_nodeList.push_back(std::move(node));
        m_nodeMap[key] = std::prev(m_nodeList.end());
        return;
    }
    iter->second->swap(node);
}

bool CfgCollector::ReadCfg(const std::string &url)
{
    CfgParser parser;

    if (!parser.PutUrl(url))
        return false;

    m_url = url;
    do
    {
        auto cfg = parser.GetNode();

        if (!cfg)
            break;
        putNode(cfg->KeyName(), std::move(cfg));
    }
    while (true);
    return true;
}

bool CfgCollector::WriteCfg(const std::string &url)
{
    std::ofstream output(url.empty() ? m_url : url);

    if (!output)
        return false;

    for (auto &node : m_nodeList)
        node->WriteOnBuffer(output);
    return true;
}

bool CfgCollector::GetValue(const std::string &key, std::string &value)
{
    return queryValue(key, value);
}

bool CfgCollector::GetValueAsNumber(const std::string &key, int &nValue)
{
    std::string value;

    if (!queryValue(key, value))
        return false;

    if (value.length() > 8)
        return false;

    for (const auto &c : value)
    {
        switch (c)
        {
        case '0': case '1':case '2':case '3':case '4':
        case '5':case '6':case '7':case '8':case '9':
            break;

        default:
            return false;
        }
    }
    std::stringstream ss(value);

    ss >> nValue;
    return true;
}

bool CfgCollector::SetValue(const std::string &key, const std::string &value)
{
    std::string getvalue;

    if (!queryValue(key, getvalue, value.c_str()))
        putNode(key, std::make_unique<CfgNode>(key,value));
    return queryValue(key, getvalue);
}

bool CfgCollector::SetValueAsNumber(const std::string &key, const int &n) {
    std::string value = std::to_string(n);
    
    return SetValue(key, value);
}


