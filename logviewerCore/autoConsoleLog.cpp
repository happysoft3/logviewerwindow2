
#include "autoConsoleLog.h"
#include "autoLogFile.h"
#include "consoleLogItem.h"

#include <sstream>

AutoConsoleLog::AutoConsoleLog(CCObject *parent)
    : CCObject(parent)
{
    m_autoLogFile = std::make_unique<AutoLogFile>(this);
    m_OnQueueAppendItem.Connection(&AutoConsoleLog::SlotQueueItemAppend, this);
    m_autoLogLimit = 128;
}

AutoConsoleLog::~AutoConsoleLog()
{ }

void AutoConsoleLog::SlotQueueItemAppend(std::weak_ptr<ConsoleLogItem> item)
{
    m_logQueue.push_back(item);
}

std::string AutoConsoleLog::ReadjustmentAutoLogFileName(const std::string &datetime)
{
    std::string res;

    res.reserve(datetime.size());
    std::transform(datetime.cbegin(), datetime.cend(), std::insert_iterator<std::string>(res, res.begin()), [](const auto &c)
    {
        switch (c)
        {
        case ':': return '-';
        case ' ': return '_';
        default: return c;
        }
    });
    return res;
}

bool AutoConsoleLog::CreateAutoLogFile()
{
    auto first = m_logQueue.front().expired() ? nullptr : m_logQueue.front().lock();

    if (!first)
        return false;

    return m_autoLogFile->MakeFile(ReadjustmentAutoLogFileName(first->CreatedTime()));
}

bool AutoConsoleLog::AutoLogWriteAll()
{
    if (!m_autoLogFile)
        return false;

    for (auto &weaklog : m_logQueue)
    {
        const auto strongLog = weaklog.lock();

        if (strongLog)
            m_autoLogFile->CommitLog(strongLog->GetContent(), strongLog->CreatedTime());
    }
    return true;
}

void AutoConsoleLog::Append(std::weak_ptr<ConsoleLogItem> log)
{
    m_OnQueueAppendItem.QueueEmit( log);
}

void AutoConsoleLog::SlotQueueAddedCompleted(bool forced)
{
    if (m_logQueue.empty())
        return;

    if ((m_logQueue.size() > m_autoLogLimit) || forced)
    {
        if (!CreateAutoLogFile())
            m_autoLogFile.reset();
        if (AutoLogWriteAll())
            m_autoLogFile->StartWrite();
        m_logQueue.clear();
        //!Todo! 한도가 되었으므로 여기에서 자동로그 파일을 생성하고 로그를 파일에 씁니다//
        //로그 내용을 파일 큐에 전달하고
        //한도를 다 전달했으면 완료처리 하여 파일이 큐를 꺼내어 쓰도록
    }
}

std::string AutoConsoleLog::ModifierAutoLogPath(const std::string &setPath)
{
    if (setPath.length())
        m_autoLogFile->SetAutoLogPath(setPath);

    return m_autoLogFile->AutoLogPath();
}

bool AutoConsoleLog::CheckValidLimitation(const size_t &limit)
{
    return (limit >= 128) && (limit <= 32768);
}

std::string AutoConsoleLog::ModifierAutoLogLimit(const std::string &setLimit)
{
    if (setLimit.length())
    {
        size_t value = 0;
        std::stringstream ss(setLimit);
        
        ss >> value;
        if (CheckValidLimitation(value))
            m_autoLogLimit = value;
    }
    return std::to_string(m_autoLogLimit);
}
