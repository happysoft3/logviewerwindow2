
#include "wndHandle.h"
#include <windows.h>

WndHandle::WndHandle(raw_handle_ty handle)
    : m_handle(handle)
{ }

WndHandle::~WndHandle()
{
    ::CloseHandle(m_handle);
}

void WndHandle::PutChild(raw_handle_ty hand)
{
    if (!hand)
        return;
    if (hand == INVALID_HANDLE_VALUE)
        return;

    m_child.push_back(std::make_unique<WndHandle>(hand));
}
