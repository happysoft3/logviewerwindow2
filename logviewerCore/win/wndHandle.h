
#ifndef WND_HANDLE_H__
#define WND_HANDLE_H__

#include <list>
#include <memory>

class WndHandle
{
public:
    using raw_handle_ty = void *;

private:
    raw_handle_ty m_handle;
    std::list<std::unique_ptr<WndHandle>> m_child;

public:
    explicit WndHandle(raw_handle_ty handle);
    ~WndHandle();

    operator raw_handle_ty()
    {
        return m_handle;
    }
    void PutChild(raw_handle_ty hand);
};

#endif

