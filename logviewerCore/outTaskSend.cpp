
#include "outTaskSend.h"
#include "processMemory.h"
#include "noxCommandLine.h"
#include "noxConsolePrint.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

OutTaskSend::OutTaskSend(CCObject *parent)
    : CCObject(parent)
{ }

OutTaskSend::~OutTaskSend()
{ }

void OutTaskSend::TaskAppend(std::unique_ptr<ProcessCodePage> task)
{
    task->BindParams();
    std::lock_guard<std::mutex> locker(m_lock);
    m_execTaskList.push_back(std::move(task));
}

bool OutTaskSend::DoOutTask(std::weak_ptr<ProcessMemory> weakProcess, std::shared_ptr<ProcessCodePage> task)
{
    auto proc = weakProcess.lock();

    if (!proc)
        return false;

    uint32_t ingamePtr = 0;

    proc->GetMemory(0x6d8530, ingamePtr);

    return (ingamePtr != 0) ? proc->ExternExecute(task) : false;
}

void OutTaskSend::Clear()
{
    std::lock_guard<std::mutex> locker(m_lock);

    if (m_execTaskList.size())
        m_execTaskList.clear();
}

bool OutTaskSend::MakeTaskConsolePrint(const std::string &message, int color)
{
    if (message.empty())
        return false;

    NoxConsolePrint *printTask = new NoxConsolePrint;

    printTask->SetPrintMessage(message);
    TaskAppend(std::unique_ptr<ProcessCodePage>(printTask));
    return true;
}

bool OutTaskSend::MakeTaskExecCmd(const std::string &cmd, int mode)
{
    if (cmd.empty())
        return false;

    std::unique_ptr<NoxCommandLine> cmdTask(new NoxCommandLine);

    if (!cmdTask->SetCommandMessage(cmd, mode))
        return false;

    TaskAppend(std::move(cmdTask));
    return true;
}

bool OutTaskSend::MakeTaskChatMessage(const std::string &chat)
{
    return MakeTaskExecCmd(stringFormat("say %s", chat));
}

void OutTaskSend::CheckTaskResult()
{
    if (m_prevTaskResult.valid())
    {
        bool res = m_prevTaskResult.get();

        if (!res && m_prevTask)
        {
            if (m_prevTask->CanReuse())
                TaskAppend(std::unique_ptr<ProcessCodePage>(m_prevTask->Clone()));
            m_prevTask.reset();
        }
    }
}

void OutTaskSend::DequeueTaskOne(std::shared_ptr<ProcessMemory> process)
{
    std::unique_ptr<ProcessCodePage> one;

    {
        std::lock_guard<std::mutex> locker(m_lock);

        if (m_execTaskList.size())
        {
            one = std::move(m_execTaskList.front());
            m_execTaskList.pop_front();
        }
    }

    CheckTaskResult();

    if (one)
    {
        std::weak_ptr<CCObject::Core> alive = m_core;
        std::weak_ptr<ProcessMemory> weakProcess = process;

        m_prevTask = std::move(one);

        m_prevTaskResult = std::async(std::launch::async, 
            [this, alive, weakProcess](std::shared_ptr<ProcessCodePage> task) {
            return alive.expired() ? false : this->DoOutTask(weakProcess, task); }, m_prevTask);
    }
}


