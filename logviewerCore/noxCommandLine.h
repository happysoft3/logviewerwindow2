
#ifndef NOX_COMMAND_LINE_H__
#define NOX_COMMAND_LINE_H__

#include "processCodePage.h"
#include <array>

class NoxCommandLine : public ProcessCodePage
{
private:
    int m_cmdMode;
    std::array<uint16_t, 256> m_cmdMsg;

public:
    NoxCommandLine();
    ~NoxCommandLine() override;

private:
    static void ExecuteCommandImpl(const uint32_t *args);
    static void ExecuteCommandSub();

    uint32_t ParamLength() const override
    {
        return sizeof(m_cmdMode) + (m_cmdMsg.max_size()*sizeof(uint16_t));
    }
    uint32_t FieldLength() const override;
    uint32_t *ExecutionBase() const override;

    bool BindParams() override;
    NoxCommandLine *Clone() override;

public:
    bool SetCommandMessage(const std::string &cmdMsg, int mode = 1);
};

#endif

