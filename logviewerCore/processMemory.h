
#ifndef PROCESS_MEMORY_H__
#define PROCESS_MEMORY_H__

#include "binaryStream.h"
#include <memory>
#include <string>

class ProcessCodePage;
class WndHandle;

class ProcessMemory : public BinaryStream
{
private:
    std::string m_processName;
    std::unique_ptr<WndHandle> m_handle;
    std::unique_ptr<WndHandle> m_executor;

    enum validationCheck
    {
        PTR = 0x5BF870, VALUE = 0x49544341
    };

public:
    ProcessMemory();
    ~ProcessMemory() override;

private:
    bool checkValid();
    void doClose();

public:
    bool IsOpened();
    void Bind(std::unique_ptr<WndHandle> handle);

private:
    bool ReadMemoryImpl(const uint32_t &targetAddress, const size_t &size);
    bool WriteMemoryImpl(const uint32_t &targetAddress);

public:
    template <class T>
    bool GetMemory(const uint32_t &targetAddress, T &dest)
    {
        if (!ReadMemoryImpl(targetAddress, sizeof(T)))
            return false;

        return GetStreamChunk(dest, 0);
    }

    template <class T>
    bool SetMemory(const uint32_t &targetAddress, const T &src)
    {
        BufferResize(sizeof(T));
        if (!SetStreamChunk(src, 0))
            return false;
        return WriteMemoryImpl(targetAddress);
    }

    template <class Container>
    bool WriteStreamMemory(const uint32_t &targetAddress, const Container &src)
    {
        BufferResize(src.size());
        if (!PutStream(src))
            return false;
        return WriteMemoryImpl(targetAddress);
    }

    template <class Container>
    bool GetStreamMemory(const uint32_t &targetAddress, Container &dest)
    {
        if (!ReadMemoryImpl(targetAddress, dest.size()))
            return false;

        uint8_t *stream = nullptr;
        size_t streamLength = 0;

        GetStreamInfo(stream, streamLength);
        std::copy(stream, stream + streamLength, dest.begin());
        return true;
    }

private:
    bool externExecuteImpl(std::shared_ptr<ProcessCodePage> codepage);

public:
    bool ExternExecute(std::shared_ptr<ProcessCodePage> codepage);
    uint32_t AllocMemory(uint32_t size);
};

#endif

