
#include "consoleLogReader.h"
#include "consoleLogManager.h"
#include "logNotifier.h"
#include "consoleLogItemList.h"
#include "consoleLogItem.h"
#include "processMemory.h"

static constexpr size_t s_rawLogStreamLength = 524;
static constexpr size_t s_rawLogCheckFieldOffset = 516;     //522->516 으로 교체 word 형. 평시 무조건 제로

ConsoleLogReader::ConsoleLogReader()
    : CCObject()
{
    m_rawLogStorage.resize(s_rawLogStreamLength);
}

ConsoleLogReader::~ConsoleLogReader()
{ }

bool ConsoleLogReader::GetConsoleLogInfo(uint32_t &consoleDetail)
{
    if (!m_processHandler)
        return false;

    uint32_t consoleInfo = 0;

    if (!m_processHandler->GetMemory(console_base_offset, consoleInfo))
        return false;

    return m_processHandler->GetMemory(consoleInfo + 32, consoleDetail);
}

bool ConsoleLogReader::GetConsoleLogLineCount(int &lineCount)
{
    uint32_t consoleDetail = 0;

    if (!GetConsoleLogInfo(consoleDetail))
        return false;

    if (!consoleDetail)
        return false;

    uint16_t wdline = 0;

    if (!m_processHandler->GetMemory(consoleDetail + 44, wdline))
        return false;

    lineCount = static_cast<int>(wdline);
    return true;
}

bool ConsoleLogReader::GetConsoleLogAddress(uint32_t &consolePtr)
{
    uint32_t consoleDetail = 0;

    if (!GetConsoleLogInfo(consoleDetail))
        return false;

    return m_processHandler->GetMemory(consoleDetail + 24, consolePtr);
}

uint32_t ConsoleLogReader::GetConsoleLogAddressWithLine(const int &lineCount)
{
    uint32_t logptr = 0;

    if (!GetConsoleLogAddress(logptr))
        return 0;

    return logptr + ((lineCount - 1) * s_rawLogStreamLength);
}

bool ConsoleLogReader::IsNewLog(const uint32_t &logptr, uint16_t &lowbit)
{
    uint32_t check = 0;

    if (!m_processHandler->GetMemory(logptr + s_rawLogCheckFieldOffset, check))
        return false;

    lowbit = check & 0xffff;
    return lowbit == (check >> 0x10);
}

void ConsoleLogReader::MarkNewLog(const uint32_t &logptr, const uint16_t &lowbit)
{
    //static constexpr uint8_t marked = 1;

    m_processHandler->SetMemory(logptr + s_rawLogCheckFieldOffset, lowbit | ((lowbit+1) << 0x10));
}

bool ConsoleLogReader::GetConsoleLogRawStream(const uint32_t &logptr)
{
    return m_processHandler->GetStreamMemory(logptr, m_rawLogStorage);
}

bool ConsoleLogReader::GetConsoleLogPredict()
{
    int lineCount = 0;

    if (!GetConsoleLogLineCount(lineCount))
        return false;

    while (lineCount)
    {
        uint32_t logptr = GetConsoleLogAddressWithLine(lineCount);

        if (!logptr)
            break;

        uint16_t lowbit = 0;

        if (IsNewLog(logptr, lowbit))
        {
            MarkNewLog(logptr, lowbit);
            if (!GetConsoleLogRawStream(logptr))
                break;
            AppendLogItem();
        }
        lineCount--;
    }
    return true;
}

void ConsoleLogReader::AppendLogItem()
{
    std::unique_ptr<ConsoleLogItem> item(new ConsoleLogItem);

    if (item->ParseItem(m_rawLogStorage))
        m_itemList->Append(std::move(item));
}

void ConsoleLogReader::Initialize(std::shared_ptr<ConsoleLogManager> logManager)
{
    m_logManager = logManager;
    m_itemList = std::make_shared<ConsoleLogItemList>();

    m_itemList->OnContainedNotify().Connection(&ConsoleLogManager::SlotListContained, m_logManager.get());
    m_logManager->SetItemListPtr(m_itemList);

    m_logNotifier = std::make_shared<LogNotifier>(this);
    m_logManager->SetLogNotifier(m_logNotifier);
}

ConsoleLogReader::LogTaskResult ConsoleLogReader::LogReaderTask(std::shared_ptr<ProcessMemory> proc)
{
    LogTaskResult res = LogTaskResult::DONE;

    for (;;)
    {
        if (!proc->IsOpened())
        {
            res = LogTaskResult::DISCONNECT;
            break;
        }

        m_processHandler = proc;
        if (!GetConsoleLogPredict())
        {
            res = LogTaskResult::LOGPTR_NULL;
            break;
        }

        if (m_itemList->IsContained())
            m_itemList->Notify();
        break;
    }
    m_processHandler = { };
    return res;
}

LogNotifier *ConsoleLogReader::GetNotifier()
{
    return (m_logNotifier != nullptr) ? m_logNotifier.get() : nullptr;
}

