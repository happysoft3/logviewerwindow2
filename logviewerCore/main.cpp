
#define DO_UNIT_TESTING
///����//
#ifndef DO_UNIT_TESTING

#include "gameWatcher.h"
#include "eventEngine.h"
#include <iostream>

int main()
{
    EventEngine::Instance().Start();
    {
        std::unique_ptr<GameWatcher> game(new GameWatcher);

        if (!game->WatcherInit())
            std::cout << "fail to watcher init" << std::endl;
        /*else
            game->Startup();*/

        std::getchar();
    }
    EventEngine::Instance().Stop();

    return 0;
}

#else

#include "sdl/cfgCollector.h"
#include <iostream>

#pragma comment(lib, "common.lib")
#pragma comment(lib, "systemfileparser.lib")

int main(int argc, const char **argv)
{
    CfgCollector cfg;

    cfg.ReadCfg("C:/Program Files (x86)/Nox/noxgui.cfg");

    return 0;
}

#endif