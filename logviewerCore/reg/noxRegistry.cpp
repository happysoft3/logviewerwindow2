
#include "noxRegistry.h"
#include "registryModifier.h"
#include "common/utils/pathmanager.h"

NoxRegistry::NoxRegistry()
{ }

NoxRegistry::~NoxRegistry()
{ }

bool NoxRegistry::LoadPath()
{
    RegistryModifier reg;
    auto pathman = std::make_unique<PathManager>();

    if (reg.Open<RegistryModifier::RegMainKey::REG_HKEY_LOCAL_MACHINE>("SOFTWARE\\WOW6432Node\\Westwood\\Nox"))
    {
        pathman->SetUrl(reg.GetString("InstallPath"));
        m_pathman = std::move(pathman);
        return true;
    }
    return false;
}

std::unique_ptr<PathManager> NoxRegistry::GetPath() const
{
    if (!m_pathman)
        return{ };

    auto rel = std::make_unique<PathManager>();

    rel->SetUrl(m_pathman->FullPath());
    return rel;
}

