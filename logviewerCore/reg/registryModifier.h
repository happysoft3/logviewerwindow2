
#ifndef REGISTRY_MODIFIER_H__
#define REGISTRY_MODIFIER_H__

#include <memory>
#include <string>
#include <vector>

class RegistryModifier
{
    class RegistryPrivate;

public:
    enum class RegMainKey
    {
        REG_HKEY_CLASSES_ROOT,
        REG_HKEY_CURRENT_USER,
        REG_HKEY_LOCAL_MACHINE,
        REG_HKEY_USERS,
        REG_HKEY_CURRENT_CONFIG
    };

private:
    std::unique_ptr<RegistryPrivate> m_regCore;

public:
    RegistryModifier();
    ~RegistryModifier();

private:
    template <RegistryModifier::RegMainKey mainKey>
    struct RegMainKeyValidate;

    template <>
    struct RegMainKeyValidate<RegistryModifier::RegMainKey::REG_HKEY_LOCAL_MACHINE>
    {
        static constexpr int accessIndex = 2;
    };

private:
    bool RegOpenImpl(int accessKey, const std::string &subKey);

public:
    template <RegistryModifier::RegMainKey mainKey>
    bool Open(const std::string &subKey)
    {
        return RegOpenImpl(RegMainKeyValidate<mainKey>::accessIndex, subKey);
    }

    std::string GetString(const std::string &valueId);
};

#endif

