
#ifndef NOX_REGISTRY_H__
#define NOX_REGISTRY_H__

#include <string>
#include <memory>

class PathManager;

class NoxRegistry
{
private:
    std::unique_ptr<PathManager> m_pathman;

public:
    NoxRegistry();
    ~NoxRegistry();

    bool LoadPath();
    std::unique_ptr<PathManager> GetPath() const;
};

#endif

