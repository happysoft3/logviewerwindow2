
#include "registryModifier.h"
#include "common/utils/stringhelper.h"
#include <iostream>
#include <windows.h>

using namespace _StringHelper;

static std::vector<HKEY> s_hKeyVector = { HKEY_CLASSES_ROOT, HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE, HKEY_USERS, HKEY_CURRENT_CONFIG };

class RegistryModifier::RegistryPrivate
{
private:
    HKEY m_hKey;

public:
    RegistryPrivate(HKEY hKey)
        : m_hKey(hKey)
    { }
    ~RegistryPrivate()
    {
        if (m_hKey != nullptr)
        {
            ::RegCloseKey(m_hKey);
            m_hKey = nullptr;
        }
    }

    HKEY& Get()
    {
        return m_hKey;
    }
};

RegistryModifier::RegistryModifier()
{ }

RegistryModifier::~RegistryModifier()
{
    if (m_regCore)
        m_regCore.reset();
}

bool RegistryModifier::RegOpenImpl(int accessKey, const std::string &subKey)
{
    HKEY getHKey = nullptr;

    try
    {
        getHKey = s_hKeyVector.at(accessKey);
    }
    catch (const std::out_of_range &oor)
    {
        std::cout << oor.what() << std::endl;
        return false;
    }

    HKEY result = nullptr;

    if (::RegOpenKeyA(getHKey, toArray(subKey), &result) != ERROR_SUCCESS)
        return false;

    m_regCore = std::make_unique<RegistryPrivate>(result);
    return true;
}

std::string RegistryModifier::GetString(const std::string &valueId)
{
    if (!m_regCore)
        return{ };

    std::vector<char> buffer(255);
    DWORD dType = 0, dSize = buffer.size();

    ::RegGetValueA(m_regCore->Get(), nullptr, toArray(valueId), RRF_RT_ANY, &dType, buffer.data(), &dSize);

    std::string str;

    str.reserve(buffer.size());
    for (const auto &c : buffer)
    {
        if (!c)
            return str;
        str.push_back(c);
    }
    return str;
}