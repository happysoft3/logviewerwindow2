
#include "pathManagerEx.h"

PathManagerEx::PathManagerEx()
    : PathManager()
{ }

PathManagerEx::~PathManagerEx()
{ }

bool PathManagerEx::paramValidCheck(const std::string &param) const
{
    for (const auto &c : param)
    {
        if (c >= 'a'&&c <= 'z')
            return true;
        if (c >= 'A'&&c <= 'Z')
            return true;
        if ([](char c) {
            switch (c)
            {
            case '0':case'1':case'2':case'3':case'4':case'5':
            case '6':case'7':case'8':case'9':case'-':
                return true;
            default:
                return false;
            }
        }(c)) return true;
    }
    return false;
}

void PathManagerEx::SetParams(const std::string &param)
{
    do
    {
        if (param.empty())
            break;

        static constexpr size_t run_param_max_length = 32;
        if (param.length() > run_param_max_length)
            break;
        if (!paramValidCheck(param))
            break;

        m_params = param;
        return;
    }
    while (false);
    m_params.clear();
}
const char *const PathManagerEx::Params() const
{
    if (m_params.empty())
        return "";

    return m_params.c_str();
}

void PathManagerEx::LoadPath(const PathManager &path)
{
    SetUrl(path.FullPath());
}

std::unique_ptr<PathManagerEx> PathManagerEx::Clone() const
{
    PathManagerEx *clone = new PathManagerEx(*this);

    return std::unique_ptr<PathManagerEx>(clone);
}
