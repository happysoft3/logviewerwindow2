
#include "patchBlockProxyUser.h"
#include "../processMemory.h"
#include <iostream>
/*

0051ceb8 -- 입장 관련


inc esi
push esi // 여기에서 +128 한곳에 웨온 아이디 있음
push ebx //00
call 004dd320
add esp, 08



--patched 0051ceb8--

mov eax, [esi+80]
test eax, eax
je l1
call 004dd320
l1:
push 0051cebd
ret

8B 86 80 00 00 00 85 C0 74 05 E8 11 C3 D8 FF 68 BD CE 51 00 C3 90


push 04 //kick type
push 31 //index
call 004deab0
add esp 08

+40-xwisid

*/

static std::vector<uint8_t> s_code = {
    0x8A, 0x86, 0x80, 0x00, 0x00, 0x00, 0x85, 0xC0, 0x74, 0x1C, 0x8D, 0x86, 0x80, 0x00, 0x00, 0x00, 0x50, 0xE8, 
    0x3C, 0x40, 0xDB, 0xFF, 0x83, 0xC4, 0x04, 0x85, 0xC0, 0x75, 0x07, 0xE8, 0x02, 0xC3, 0xD8, 0xFF, 0xEB, 0x02, 0x31, 0xC0, 0x68, 0xBD, 0xCE, 0x51, 0x00, 0xC3
};

static std::vector<uint8_t> s_runcode = { 0xE9, 0x43,0x41,0x23, 00 };

PatchBlockProxyUser::PatchBlockProxyUser()
    : DumpChunk()
{ }

PatchBlockProxyUser::~PatchBlockProxyUser()
{ }

typedef struct _banUser
{
    struct _banUser *next;
    struct _banUser *prev;
    int unknown8;
    uint16_t userName[28];
} BanUser;

#define BANUSER_FIRST 0x62f0c0
int PatchBlockProxyUser::fixBlackListCode(char *loginId)
{
    BanUser *pBan = *(BanUser **)BANUSER_FIRST;

    while (pBan != (BanUser *)BANUSER_FIRST)
    {
        int ban = 1;
        for (int i = 0 ; i < 20 ; i++)
        {
            if (pBan->userName[i] && loginId[i])
            {
                if (pBan->userName[i] != loginId[i])
                {
                    ban = 0;
                    break;
                }
            }
        }
        if (ban)
            return true;
        pBan = pBan->next;
    }
    return false;
}

void PatchBlockProxyUser::fixBlackListCodeSub()
{
    std::cout << "PatchBlockProxyUser::fixBlackListCodeSub";
}

int PatchBlockProxyUser::computeLength() const
{
    return s_code.size() +
        reinterpret_cast<uint32_t>(&PatchBlockProxyUser::fixBlackListCodeSub) - reinterpret_cast<uint32_t>(&PatchBlockProxyUser::fixBlackListCode);
}

void PatchBlockProxyUser::writeRuncode(uint32_t off, ProcessMemory &proc)
{
    BinaryBuffer runcode;

    runcode.StreamSet(s_runcode);
    runcode.SetC(calcCallNode(0x51ceb8, off), 1);

    std::vector<uint8_t> out;
    runcode.StreamMove(out);
    proc.WriteStreamMemory(0x51ceb8, out);
}

bool PatchBlockProxyUser::preDump(uint32_t off, ProcessMemory &proc)
{
    StreamSet(s_code);
    SetC(calcCallNode(off + 0x11, off+s_code.size()), 0x12);
    SetC(calcCallNode(off + 0x1d, 0x4DD320), 0x1e);
    writeRuncode(off, proc);

    uint8_t *codeOff = reinterpret_cast<uint8_t*>(&PatchBlockProxyUser::fixBlackListCode);
    uint32_t codeLength = reinterpret_cast<uint32_t>(&PatchBlockProxyUser::fixBlackListCodeSub) - reinterpret_cast<uint32_t>(&PatchBlockProxyUser::fixBlackListCode);

    std::vector<uint8_t> ccode;

    for (uint32_t u = 0 ; u < codeLength ; u ++)
        ccode.push_back(codeOff[u]);
    StreamPush(ccode);
    return true;
}


