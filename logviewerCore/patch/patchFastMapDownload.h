
#ifndef PATCH_FAST_MAP_DOWNLOAD_H__
#define PATCH_FAST_MAP_DOWNLOAD_H__

#include "dumpChunk.h"

class PatchFastMapDownload : public DumpChunk
{
private:
    uint8_t m_speed;

public:
    PatchFastMapDownload();
    ~PatchFastMapDownload() override;

private:
    int computeLength() const override;
    void writeRunCode(uint32_t off, ProcessMemory &proc);
    bool preDump(uint32_t off, ProcessMemory &proc) override;

public:
    void SetDownloadSpeed(uint8_t speed);
    int GetDownloadSpeed() const;
};

#endif

