
#include "patchGameScreenshot.h"
#include "../processMemory.h"
#include <chrono>
#include <algorithm>

static std::vector<uint8_t> s_opcodeScreenshot = {
    0x56, 0x55, 0x8B, 0xEC, 0x83, 0xEC, 0x10, 0x8B, 0xF4, 0x56, 0xA1, 0x4C, 0x11, 0x58, 0x00, 0xFF, 0xD0, 
    0x66, 0x8B, 0x46, 0x0C, 0x50, 0x66, 0x8B, 0x46, 0x0A, 0x50, 0x66, 0x8B, 0x46, 0x08, 0x50, 0x66, 0x8B, 
    0x46, 0x06, 0x50, 0x66, 0x8B, 0x46, 0x02, 0x50, 0x66, 0x8B, 0x06, 0x50, 0x68, 0x60, 0x10, 0x75, 0x00, 
    0xFF, 0x75, 0x0C, 0xE8, 0xEC, 0x4B, 0xE1, 0xFF, 0x83, 0xC4, 0x20, 0x8B, 0xE5, 0x5D, 0x5E, 0xC3, 0x90
};

static std::vector<uint8_t> s_runcode = {
    0x52, 0xE8, 0x79, 0x36, 0x2E, 0x00, 0x5A, 0xEB, 0x1C, 0x90, 0x90, 0x90
};

PatchGameScreenshot::PatchGameScreenshot()
    : DumpChunk()
{ }

PatchGameScreenshot::~PatchGameScreenshot()
{ }

void PatchGameScreenshot::InitialLoad()
{
    m_ssNameFormat = "%d%02d%02d%02d%02d%02d.bmp--";
    std::transform(m_ssNameFormat.cbegin(), m_ssNameFormat.cend(), m_ssNameFormat.begin(), [](const auto &c) { return (c == '-') ? '\0' : c; });
    m_sFormatOff = s_opcodeScreenshot.size();
}

int PatchGameScreenshot::computeLength() const
{
    return m_ssNameFormat.length() + s_opcodeScreenshot.size();
}

void PatchGameScreenshot::writeRuncode(uint32_t off, ProcessMemory &proc)
{
    BinaryBuffer runcode;

    runcode.StreamSet(s_runcode);
    runcode.SetC(calcCallNode(0x46d982, off), 2);
    std::vector<uint8_t> out;

    runcode.StreamMove(out);
    proc.WriteStreamMemory(0x46d981, out);
}

bool PatchGameScreenshot::preDump(uint32_t off, ProcessMemory &proc)
{
    //proc.SetMemory(0x46db61, off);
    proc.WriteStreamMemory(off + m_sFormatOff, m_ssNameFormat);
    writeRuncode(off, proc);
    StreamSet(s_opcodeScreenshot);
    SetC(calcCallNode(off + 0x36, 0x565c27), 0x37);
    SetC(off + m_sFormatOff,  47);
    StreamPush(m_ssNameFormat);
    return true;
}

