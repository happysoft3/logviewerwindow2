
#ifndef PATCH_MAP_DOWNLOAD_FIX_H__
#define PATCH_MAP_DOWNLOAD_FIX_H__

#include "dumpChunk.h"

class PatchMapDownloadFix : public DumpChunk
{
public:
    PatchMapDownloadFix();
    ~PatchMapDownloadFix() override;

private:
    static void fixDownloadMap();
    static void fixDownloadMapSub();
    int computeLength() const override;
    void writeRuncode(uint32_t off, ProcessMemory &proc);
    bool preDump(uint32_t off, ProcessMemory &proc) override;
};

#endif

