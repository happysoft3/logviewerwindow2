
#include "patchMapDownloadFix.h"
#include "../processMemory.h"
#include <iostream>

typedef struct _downloadMapData
{
    uint16_t field00;
    uint16_t downloading;		//true or false
    uint32_t field04;
    uint32_t field08;
    uint32_t nxzFileSize;			//다운받을 nxz파일의 크기
    uint32_t field10;
    uint32_t field14;
    uint32_t sendNxzSize;		//보낸 nxz파일의 크기
    uint32_t field1c;		//평상시 02
    uint32_t field20;
    uint32_t field24;
    uint32_t field28;
    uint32_t field2c;
}DownloadMapData;

PatchMapDownloadFix::PatchMapDownloadFix()
    : DumpChunk()
{ }

PatchMapDownloadFix::~PatchMapDownloadFix()
{ }

//원본코드//
//void PatchMapDownloadFix::fixDownloadMap()
//{
//    using vfunction_proto_type1 = int (_cdecl *)(int);
//    vfunction_proto_type1 function00519930 = reinterpret_cast<vfunction_proto_type1>(0x519930);
//    using vfunction_proto_type2 = void (_cdecl *)(DownloadMapData*);
//    vfunction_proto_type2 function005199f0 = reinterpret_cast<vfunction_proto_type2>(0x5199f0);
//    using vfunction_proto_type3 = void (_cdecl *)(void);
//    vfunction_proto_type3 function00519eb0 = reinterpret_cast<vfunction_proto_type3>(0x519eb0);
//    int *downloadUserCount = (int *)0x81b83c;
//    int *nxzFileSize = (int *)0x81b838;
//
//    if (*downloadUserCount == 0)
//        return;
//
//    if (!(*nxzFileSize))
//        function00519eb0(); //->여기에서 nxz 가져오는 함수로 빠진다
//
//    int pIndex = 0; //edi //플레이어 인덱스 값인듯
//    DownloadMapData *ptr = (DownloadMapData *)0x81b260; //esi
//    DownloadMapData *end = &ptr[31];
//    int v1 = 0;	//eax
//    uint8_t v2;
//
//    for (;;)
//    {
//        if (ptr->downloading == 1)
//        {
//            v1 = function00519930(pIndex + 1);
//            v2 = (uint8_t)ptr->field1c;
//            if (v1 > v2)
//                ++ptr->field20;
//            else
//                function005199f0(ptr);
//        }
//        ++ptr;
//        ++pIndex;
//        if (ptr == end)
//            break;
//    }
//}

#define CALL_EXTERN_FUNCTION(type, off, ...) reinterpret_cast<type>(off)(__VA_ARGS__)
void PatchMapDownloadFix::fixDownloadMap()
{
    using vfunction_proto_type1 = int (_cdecl *)(int);
    using vfunction_proto_type2 = void (_cdecl *)(DownloadMapData*);
    using vfunction_proto_type3 = void (_cdecl *)(void);
    int *downloadUserCount = (int *)0x81b83c;
    int *nxzFileSize = (int *)0x81b838;

    CALL_EXTERN_FUNCTION(vfunction_proto_type1, 0x519930, 30);
    if (*downloadUserCount == 0)
        return;

    if (!(*nxzFileSize))
        CALL_EXTERN_FUNCTION(vfunction_proto_type3, 0x519eb0); //->여기에서 nxz 가져오는 함수로 빠진다

    int pIndex = 0; //edi //플레이어 인덱스 값인듯
    DownloadMapData *ptr = (DownloadMapData *)0x81b260; //esi
    DownloadMapData *end = &ptr[31];
    int v1 = 0;	//eax
    uint8_t v2;
    int wholeRep = 16;

    for (;;)
    {
        if (ptr->downloading == 1)
        {
            v1 = CALL_EXTERN_FUNCTION(vfunction_proto_type1, 0x519930, pIndex + 1);
            v2 = (uint8_t)ptr->field1c;
            if (v1 > v2)
                ++ptr->field20;
            else
                CALL_EXTERN_FUNCTION(vfunction_proto_type2, 0x5199f0, ptr);
        }
        ++ptr;
        ++pIndex;
        if (ptr == end)
        {
            if (--wholeRep >= 0)
            {
                pIndex = 0;
                ptr = (DownloadMapData *)0x81b260;
                continue;
            }
            break;
        }
    }
}

void PatchMapDownloadFix::fixDownloadMapSub()
{
    std::cout << "PatchMapDownloadFix::fixDownloadMapSub";
}

int PatchMapDownloadFix::computeLength() const
{
    return 
        reinterpret_cast<uint32_t>(&PatchMapDownloadFix::fixDownloadMapSub) - reinterpret_cast<uint32_t>(&PatchMapDownloadFix::fixDownloadMap);
}

void PatchMapDownloadFix::writeRuncode(uint32_t off, ProcessMemory &proc)
{
    BinaryBuffer buff;
    std::vector<uint8_t> run = { 0x68, 0xAC ,0xAC, 0xAC, 0 ,0xC3 };
    buff.StreamSet(run);
    //buff.SetC(calcCallNode(0x519990, off), 1);
    buff.SetC(off, 1);
    std::vector<uint8_t> out;
    buff.StreamMove(out);
    proc.WriteStreamMemory(0x519990, out);
}

bool PatchMapDownloadFix::preDump(uint32_t off, ProcessMemory &proc)
{
    uint8_t *codeOff = reinterpret_cast<uint8_t*>(&PatchMapDownloadFix::fixDownloadMap);
    uint32_t codeLength = reinterpret_cast<uint32_t>(&PatchMapDownloadFix::fixDownloadMapSub) - reinterpret_cast<uint32_t>(&PatchMapDownloadFix::fixDownloadMap);

    std::vector<uint8_t> ccode;

    for (uint32_t u = 0 ; u < codeLength ; u ++)
        ccode.push_back(codeOff[u]);
    StreamSet(ccode);
    writeRuncode(off, proc);
    return true;
}

