
#ifndef DUMP_CHUNK_H__
#define DUMP_CHUNK_H__

#include "common/utils/binarybuffer.h"
#include <cstdint>

class ProcessMemory;

class DumpChunk : public BinaryBuffer
{
public:
    DumpChunk();
    ~DumpChunk() override;

private:
    virtual int computeLength() const = 0;
    virtual bool preDump(uint32_t off, ProcessMemory &proc) = 0;

protected:
    static int calcCallNode(int curAddr, int targetAddr);

public:
    virtual void InitialLoad() {}
    int Length() const;
    void Dump(uint32_t off, ProcessMemory &proc);
};

#endif

