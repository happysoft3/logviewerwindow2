
#ifndef PATCH_KOR_CHAT_H__
#define PATCH_KOR_CHAT_H__

#include "dumpChunk.h"

class PatchKorChat : public DumpChunk
{
public:
    PatchKorChat();
    ~PatchKorChat() override;

private:
    int computeLength() const override;
    bool preDump(uint32_t off, ProcessMemory &proc) override;
};

#endif

