
#include "dumpLoader.h"
#include "dumpChunk.h"
#include "patchBlockProxyUser.h"
#include "patchXbowNoDelay.h"
#include "patchFixChangeWeapon.h"
#include "patchGameScreenshot.h"
#include "patchFixInventory.h"
#include "patchKorChat.h"
#include "patchMixed.h"
#include "patchMapList.h"
#include "../processMemory.h"

DumpLoader::DumpLoader()
{
    m_chunkList.push_back(std::make_unique<PatchMapList>());
    m_chunkList.push_back(std::make_unique<PatchBlockProxyUser>());
    m_chunkList.push_back(std::make_unique<PatchXbowNoDelay>());
    m_chunkList.push_back(std::make_unique<PatchFixChangeWeapon>());
    m_chunkList.push_back(std::make_unique<PatchFixInventory>());
    m_chunkList.push_back(std::make_unique<PatchGameScreenshot>());
    m_chunkList.push_back(std::make_unique<PatchKorChat>());
    m_chunkList.push_back(std::make_unique<MixPatch>());
}

DumpLoader::~DumpLoader()
{ }

int DumpLoader::computePadding(int offset, int align)
{
    return (offset % align == 0) ? 0 : align - offset % align;
}

uint32_t DumpLoader::allocOnTarget(ProcessMemory &proc)
{
    int extent = 0;

    for (auto &c : m_chunkList)
    {
        c->InitialLoad();
        extent += c->Length();
    }

    if (extent)
    {
        extent += computePadding(extent, 1024);
        return proc.AllocMemory(extent);
    }
    return 0;
}

void DumpLoader::doChunks(ProcessMemory &proc)
{
    uint32_t off = allocOnTarget(proc);

    for (auto &c : m_chunkList)
    {
        c->Dump(off, proc);
        off += c->Length();
    }
}

void DumpLoader::Push(std::unique_ptr<DumpChunk> code)
{
    m_chunkList.push_back(std::move(code));
}

void DumpLoader::Dump(std::shared_ptr<ProcessMemory> proc)
{
    if (proc)
    {
        if (m_chunkList.empty())
            return;

        doChunks(*proc);
    }
}


