
#ifndef PATCH_MIXED_H__
#define PATCH_MIXED_H__

#include "dumpChunk.h"

class MixPatch : public DumpChunk
{
public:
    MixPatch();
    ~MixPatch() override;

private:
    int computeLength() const override;
    bool preDump(uint32_t off, ProcessMemory &proc) override;
};

#endif

