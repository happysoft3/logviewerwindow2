
#ifndef PATCH_MAP_LIST_H__
#define PATCH_MAP_LIST_H__

#include "dumpChunk.h"

class PatchMapList : public DumpChunk
{
public:
    PatchMapList();
    ~PatchMapList() override;

private:
    int computeLength() const override;
    bool preDump(uint32_t off, ProcessMemory &proc) override;
};

#endif

