
#include "dumpChunk.h"
#include "../processMemory.h"

static constexpr int s_align = 8;
DumpChunk::DumpChunk()
    : BinaryBuffer()
{ }

DumpChunk::~DumpChunk()
{ }

int DumpChunk::calcCallNode(int curAddr, int targetAddr)
{
    return targetAddr - 5 - curAddr;
}

int DumpChunk::Length() const
{
    int off = computeLength();
    int real = ((off % s_align == 0) ? 0 : s_align - off % s_align);

    return off + real;
}

void DumpChunk::Dump(uint32_t off, ProcessMemory &proc)
{
    if (!preDump(off, proc))
        return;

    if (Empty())
        return;

    std::vector<uint8_t> stream;

    StreamMove(stream);
    proc.WriteStreamMemory(off, stream);
}

