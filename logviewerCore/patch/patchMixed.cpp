
#include "patchMixed.h"
#include "../processMemory.h"

MixPatch::MixPatch()
    : DumpChunk()
{ }

MixPatch::~MixPatch()
{ }

int MixPatch::computeLength() const
{
    return 0;
}

bool MixPatch::preDump(uint32_t off, ProcessMemory &proc)
{
    proc.SetMemory(0x4e3ba0, 0xc340c031); //0x4e3bad 오류 방지 목적.
    proc.SetMemory(0x53f974, 0x6a17eb); //EB 17 6A 00 //퀘스트 게임모드에서 비스롤 습득가능(소환술사 외 타 직업)
    //50 8B 80 78/ 01 00 00 3D/ AC AC AC AC/ 74 02 FF D0/ 83 C4 10 C3
    int *poff = (int *)0x46b4a8; //퀘스트 게임모드에서 직업 전환 시 팅김 방지 코드입니다//
    proc.SetMemory((uint32_t)poff++, 0x78808b50);
    proc.SetMemory((uint32_t)poff++, 0x3d000001);
    proc.SetMemory((uint32_t)poff++, 0xacacacac);
    proc.SetMemory((uint32_t)poff++, 0xd0ff0274);
    proc.SetMemory((uint32_t)poff++, 0xc310c483);
    return true;
}


