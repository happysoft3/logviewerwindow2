
#include "patchFastMapDownload.h"
#include "../processMemory.h"
#include <vector>

static std::vector<uint8_t> s_fastMapdownCode = {
    0xA1, 0x38, 0xB8, 0x81, 0x00, 0x85, 0xC0, 0x75, 0x05, 0xE8, 0xA2, 0x8E, 0xDC, 0xFF, 0x56, 0x57, 0x31, 0xFF, 0xBE, 0x80, 0xB2, 0x81, 0x00, 0x6A, 
    0x04, 0x66, 0x83, 0x7E, 0xE2, 0x01, 0x75, 0x3A, 0x8D, 0x47, 0x01, 0x50, 0xE8, 0x07, 0x89, 0xDC, 0xFF, 0x31, 0xC9, 0x83, 0xC4, 0x04, 0x8A, 0x4E, 
    0xFC, 0x39, 0xC0, 0x7F, 0x0E, 0x8D, 0x56, 0xE0, 0x52, 0xE8, 0xB2, 0x89, 0xDC, 0xFF, 0x83, 0xC4, 0x04, 0xEB, 0x02, 0xFF, 0x06, 0x8B, 0x46, 0xEC, 
    0x3B, 0x46, 0xF8, 0x74, 0x0D, 0x8B, 0x04, 0x24, 0x85, 0xC0, 0x74, 0x06, 0x48, 0x89, 0x04, 0x24, 0xEB, 0xBF, 0x83, 0xC6, 0x30, 0x47, 0x81, 0xFE, 
    0x50, 0xB8, 0x81, 0x00, 0x7C, 0xB3, 0x5F, 0x5F, 0x5E, 0x68, 0x9E, 0x99, 0x51, 0x00, 0xC3, 0x90 };
static std::vector<uint8_t> s_runcode = { 0x74, 0x05, 0xE9, 0x62, 0x76, 0x23, 0x00, 0xC3, 0x90 };

PatchFastMapDownload::PatchFastMapDownload()
    : DumpChunk()
{
    m_speed = 2;
}

PatchFastMapDownload::~PatchFastMapDownload()
{ }

int PatchFastMapDownload::computeLength() const
{
    return s_fastMapdownCode.size();
}

void PatchFastMapDownload::writeRunCode(uint32_t off, ProcessMemory &proc)
{
    BinaryBuffer runcode;

    runcode.StreamSet(s_runcode);
    runcode.SetC(calcCallNode(0x519999, off), 3);
    std::vector<uint8_t> out;

    runcode.StreamMove(out);
    proc.WriteStreamMemory(0x519997, out);
}

bool PatchFastMapDownload::preDump(uint32_t off, ProcessMemory &proc)
{
    StreamSet( s_fastMapdownCode );

    SetC(calcCallNode(off + 9, 0x519EB0), 10);
    SetC(calcCallNode(off + 0x24, 0x519930), 0x25);
    SetC(calcCallNode(off + 0x39, 0x5199F0), 0x3a);
    SetC(m_speed, 24); //mapdownload_speed
    writeRunCode(off, proc);
    return true;
}

void PatchFastMapDownload::SetDownloadSpeed(uint8_t speed)
{
    switch (speed)
    {
    case 1: case 2: case 3: case 4:
        m_speed = speed;
        break;

    default:
        m_speed = 2;
    }
}

int PatchFastMapDownload::GetDownloadSpeed() const
{
    return m_speed & 0xff;
}
