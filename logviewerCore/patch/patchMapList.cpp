
#include "patchMapList.h"
#include "../processMemory.h"

#define DEFAULT_MAP_META_DATA_SIZE 36
#define MAX_TITLE_LENGTH_BYTES 40
#define PADDING_BYTES 4
#define MAP_META_FIELD_TOTAL_SIZE DEFAULT_MAP_META_DATA_SIZE+MAX_TITLE_LENGTH_BYTES+PADDING_BYTES

static std::vector<uint8_t> s_opcodes = {
    0x56, 0x57, 0x51, 0xBE, 0x80, 0x48, 0x97, 0x00, 0x8D, 0x7D, 0x24, 0xB9, MAX_TITLE_LENGTH_BYTES+PADDING_BYTES, 0x00, 0x00, 0x00, 0xF3, 
    0xA4, 0x59, 0x5F, 0x5E, 0x8B, 0x0D, 0xF0, 0x4D, 0x97, 0x00, 0x89, 0x4D, 0x1C, 0x8A, 0x15, 0xF5, 0x4D, 
    0x97, 0x00, 0x88, 0x55, 0x21, 0xA0, 0xF4, 0x4D, 0x97, 0x00, 0x88, 0x45, 0x20, 0x68, 0x34, 0x09, 0x4D, 
    0x00, 0xC3, 0x90, 0x90, 0x90, };

static std::vector<uint8_t> s_mapListFmt = { '%', 0, 'S', 0, '\t', 0, '%', 0, 'S',0,0,0 }; // L"%S\t%S";
static constexpr uint32_t buffer_size = 256;
//static constexpr uint32_t null_char_off = 4;

PatchMapList::PatchMapList()
    : DumpChunk()
{ }
PatchMapList::~PatchMapList()
{ }

int PatchMapList::computeLength() const
{
    return s_opcodes.size() + buffer_size + s_mapListFmt.size();
}

bool PatchMapList::preDump(uint32_t off, ProcessMemory &proc)
{
    std::vector<uint8_t> buffer(buffer_size);
    int fmtOff = off + s_opcodes.size();
    int buffOff = fmtOff + s_mapListFmt.size();// + null_char_off;

    StreamSet(s_opcodes);
    StreamPush(s_mapListFmt);
    StreamPush(buffer);
    //proc.SetMemory(0x46a4dc, calcCallNode(0x46a4db, off));
    proc.SetMemory(0x4d08e0, 0x2c7ae800 | MAP_META_FIELD_TOTAL_SIZE);
    //proc.SetMemory(0x458370+1, 0x5a9624);
    //proc.SetMemory(0x458369 + 1, 0x5a9600);
    //proc.SetMemory(0x458299, 0x83246b8d);

    proc.SetMemory(0x4d091a, 0xb8);
    proc.SetMemory(0x4d091a + 1, off);
    proc.SetMemory(0x4d091f, 0x9090e0ff); //B8 00 10 75 00 / FF E0 90 90

    proc.SetMemory(0x45835e, 0x5124538d);
    proc.SetMemory(0x458364, 0x68);
    proc.SetMemory(0x458364+1, fmtOff);
    proc.SetMemory(0x458369, 0x68);
    proc.SetMemory(0x458369+1, buffOff);
    proc.SetMemory(0x45836e, 0x246816eb);
    proc.SetMemory(0x458391, 0xb9); //B9 30 10 75 /00 90 90
    proc.SetMemory(0x458391+1, buffOff);
    proc.SetMemory(0x458396, 0x51569090); //90 90 56 51
    //SetC(calcCallNode(off + 4, 0x56688d), 5);
    return true;
}

