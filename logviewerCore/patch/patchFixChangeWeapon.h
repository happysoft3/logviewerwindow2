
#ifndef PATCH_FIX_CHANGE_WEAPON_H__
#define PATCH_FIX_CHANGE_WEAPON_H__

#include "dumpChunk.h"

class PatchFixChangeWeapon : public DumpChunk
{
public:
    PatchFixChangeWeapon();
    ~PatchFixChangeWeapon() override;

private:
    int computeLength() const override;
    void writeRunCode(uint32_t off, ProcessMemory &proc);
    bool preDump(uint32_t off, ProcessMemory &proc) override;
};

#endif

