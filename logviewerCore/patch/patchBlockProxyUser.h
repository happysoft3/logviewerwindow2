
#ifndef PATCH_BLOCK_PROXY_USER_H__
#define PATCH_BLOCK_PROXY_USER_H__

#include "dumpChunk.h"

class PatchBlockProxyUser : public DumpChunk
{
public:
    PatchBlockProxyUser();
    ~PatchBlockProxyUser() override;

private:
    static int fixBlackListCode(char *loginId);
    static void fixBlackListCodeSub();
    int computeLength() const override;
    void writeRuncode(uint32_t off, ProcessMemory &proc);
    bool preDump(uint32_t off, ProcessMemory &proc) override;
};

#endif

