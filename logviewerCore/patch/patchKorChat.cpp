
#include "patchKorChat.h"
#include "../processMemory.h"

static std::vector<uint8_t> s_opcodes = {
    0xFF, 0x74, 0x24, 0x04, 0xE8, 0x84, 0x58, 0xE1, 0xFF, 0x59, 0x55, 0x8B, 0xA9, 0x02, 0x02, 0x00, 0x00, 0x85, 0xED, 0x75, 
    0x11, 0x31, 0xED, 0x66, 0x8B, 0xA9, 0x00, 0x02, 0x00, 0x00, 0x89, 0x2C, 0x41, 0x85, 0xED, 0x74, 0x01, 0x40, 0x5D, 0xC3
};

PatchKorChat::PatchKorChat()
    : DumpChunk()
{ }
PatchKorChat::~PatchKorChat()
{ }

int PatchKorChat::computeLength() const
{
    return s_opcodes.size();
}
bool PatchKorChat::preDump(uint32_t off, ProcessMemory &proc)
{
    StreamSet(s_opcodes);
    proc.SetMemory(0x46a4dc, calcCallNode(0x46a4db, off));
    SetC(calcCallNode(off+4, 0x56688d), 5);
    return true;
}

