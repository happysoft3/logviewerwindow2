
#ifndef PATCH_FIX_INVENTORY_H__
#define PATCH_FIX_INVENTORY_H__

#include "dumpChunk.h"

class PatchFixInventory : public DumpChunk
{
public:
    PatchFixInventory();
    ~PatchFixInventory() override;

private:
    int computeLength() const override;
    bool preDump(uint32_t off, ProcessMemory &proc) override;
};

#endif

