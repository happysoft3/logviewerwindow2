
#ifndef PATCH_XBOW_NO_DELAY_H__
#define PATCH_XBOW_NO_DELAY_H__

#include "dumpChunk.h"

class PatchXbowNoDelay : public DumpChunk
{
public:
    PatchXbowNoDelay();
    ~PatchXbowNoDelay() override;

private:
    int computeLength() const override;
    void writeRunCode(uint32_t off, ProcessMemory &proc);
    bool preDump(uint32_t off, ProcessMemory &proc) override;
};

#endif

