
#ifndef DUMP_LOADER_H__
#define DUMP_LOADER_H__

#include <memory>
#include <list>

class ProcessMemory;
class DumpChunk;

class DumpLoader
{
private:
    std::list<std::unique_ptr<DumpChunk>> m_chunkList;

public:
    DumpLoader();
    ~DumpLoader();

private:
    int computePadding(int offset, int align);
    uint32_t allocOnTarget(ProcessMemory &proc);
    void doChunks(ProcessMemory &proc);

public:
    void Push(std::unique_ptr<DumpChunk> code);
    void Dump(std::shared_ptr<ProcessMemory> proc);
};

#endif

