
#ifndef PATCH_GAME_SCREENSHOT_H__
#define PATCH_GAME_SCREENSHOT_H__

#include "dumpChunk.h"

class PatchGameScreenshot : public DumpChunk
{
private:
    std::string m_ssNameFormat;
    uint32_t m_sFormatOff;

public:
    PatchGameScreenshot();
    ~PatchGameScreenshot() override;

private:
    void InitialLoad() override;
    int computeLength() const override;
    void writeRuncode(uint32_t off, ProcessMemory &proc);
    bool preDump(uint32_t off, ProcessMemory &proc) override;
};

#endif

