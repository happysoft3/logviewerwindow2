
#include "logNotifier.h"
#include "consoleLogItem.h"

LogNotifier::LogNotifier(CCObject *parent)
    : CCObject(parent)
{ }

LogNotifier::~LogNotifier()
{ }

void LogNotifier::Append(std::weak_ptr<ConsoleLogItem> log)
{
    if (log.expired())
        return;

    m_logQueue.push_back(log);
}

void LogNotifier::SlotNotifyLog(bool /*forced*/)
{
    auto copiedList = m_logQueue;

    if (copiedList.empty())
        return;

    m_OnReleaseLogList.QueueEmit( copiedList);
    m_logQueue.clear();
}


