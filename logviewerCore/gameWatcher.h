﻿
#ifndef GAME_WATCHER_H__
#define GAME_WATCHER_H__

#include "common/ccobject/ccobject.h"
#include <thread>
#include <future>
#include <mutex>

class ConsoleLogReader;
class ProcessMemory;
class LogNotifier;
class OutTaskSend;
class ConsoleLogManager;
class PathManagerEx;
class PathManager;

class TaskQueue;
class NoxHandleThread;
class DumpChunk;

class GameWatcher : public CCObject
{
private:
    enum class GameState
    {
        Failure,
        Succeed,
        AlreadyConnection
    };

private:
    std::shared_ptr<ProcessMemory> m_process;
    std::unique_ptr<ConsoleLogReader> m_logReader;
    std::shared_ptr<int> m_logThreadState;
    std::unique_ptr<OutTaskSend> m_sender;
    std::future<bool> m_workResult;    
    std::shared_ptr<ConsoleLogManager> m_logManager;
    std::unique_ptr<NoxHandleThread> m_handleThread;

    std::weak_ptr<TaskQueue> m_taskQueue;

    int m_downloadMap;

public:
    explicit GameWatcher();
    ~GameWatcher() override;

private:
    bool DoLogTask();
    bool watcherWork(std::weak_ptr<int> state);
    GameState doConnect(std::shared_ptr<ProcessMemory> proc);

public:
    bool WatcherInit();
    LogNotifier *GetLogNotifier();

private:
    void startLogthread();

public:
    void ForceWriteAutoLog();
    void CommitOutTask(const std::string &taskName, const std::string &argMessage);
    std::string AccessLogParam(const std::string &paramType, const std::string &value = { });

private:
    void setDefaultPath();

public:
    std::unique_ptr<PathManager> GetAlternativePath() const;

private:
    void slotEndProcess();
    void slotGotProcess(std::shared_ptr<ProcessMemory> proc);
    bool runAppDetail(const PathManagerEx &targPath, std::string &err);

public:
    bool RunApp(const PathManagerEx &targPath, std::string &errorMessage);
    void DoPatch();
    void TerminateAppCompletely();
    bool IsRunning() const;
    void ChangeMapDownloadSpeed(int n)
    {
        m_downloadMap = n;
    }

public:
    DECLARE_SIGNAL(OnDisconnected)
    DECLARE_SIGNAL(OnConnected)

private:
    std::mutex m_waitlock;
};

#endif


