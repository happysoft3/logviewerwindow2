
#ifndef OUT_TASK_SEND_H__
#define OUT_TASK_SEND_H__

#include "common/ccobject/ccobject.h"
#include <future>

class ProcessCodePage;
class ProcessMemory;

class OutTaskSend : public CCObject
{
private:
    std::list<std::unique_ptr<ProcessCodePage>> m_execTaskList;
    std::future<bool> m_prevTaskResult;
    std::shared_ptr<ProcessCodePage> m_prevTask;

public:
    OutTaskSend(CCObject *parent = nullptr);
    ~OutTaskSend() override;

private:
    void TaskAppend(std::unique_ptr<ProcessCodePage> task);
    bool DoOutTask(std::weak_ptr<ProcessMemory> weakProcess, std::shared_ptr<ProcessCodePage> task);

public:
    void Clear();
    bool MakeTaskConsolePrint(const std::string &messge, int color);
    bool MakeTaskExecCmd(const std::string &cmd, int mode = 1);
    bool MakeTaskChatMessage(const std::string &chat);

private:
    void CheckTaskResult();

public:
    void DequeueTaskOne(std::shared_ptr<ProcessMemory> process);

private:
    std::mutex m_lock;
};

#endif

