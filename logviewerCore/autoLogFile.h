
#ifndef AUTO_LOG_FILE_H__
#define AUTO_LOG_FILE_H__

#include "common/ccobject/ccobject.h"
#include <string>
#include <future>

class IOFileStream;

class AutoLogFile : public CCObject
{
private:
    std::unique_ptr<IOFileStream> m_logFile;
    std::list<std::string> m_contentList;
    std::future<bool> m_writeLogResult;
    std::string m_autoLogPath;

public:
    AutoLogFile(CCObject *parent = nullptr);
    ~AutoLogFile() override;

    bool MakeFile(const std::string &datetime);
    void CommitLog(const std::string &content, const std::string &datetime);

private:
    bool WriteStringToBinary(const std::string &str);
    bool WriteLogFile();

public:
    void StartWrite();

    std::string AutoLogPath() const
    {
        return m_autoLogPath;
    }
    void SetAutoLogPath(const std::string &path)
    {
        m_autoLogPath = path;
    }
};

#endif

