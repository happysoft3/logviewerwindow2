
#ifndef AUTO_CONSOLE_LOG_H__
#define AUTO_CONSOLE_LOG_H__

#include "common/ccobject/ccobject.h"

class ConsoleLogItem;
class AutoLogFile;

class AutoConsoleLog : public CCObject
{
private:
    std::list<std::weak_ptr<ConsoleLogItem>> m_logQueue;
    std::unique_ptr<AutoLogFile> m_autoLogFile;
    size_t m_autoLogLimit;

public:
    AutoConsoleLog(CCObject *parent = nullptr);
    ~AutoConsoleLog() override;

private:
    void SlotQueueItemAppend(std::weak_ptr<ConsoleLogItem> item);
    std::string ReadjustmentAutoLogFileName(const std::string &datetime);
    bool CreateAutoLogFile();
    bool AutoLogWriteAll();

public:
    void Append(std::weak_ptr<ConsoleLogItem> log);
    void SlotQueueAddedCompleted(bool forced);
    std::string ModifierAutoLogPath(const std::string &setPath);

private:
    bool CheckValidLimitation(const size_t &limit);

public:
    std::string ModifierAutoLogLimit(const std::string &setLimit);

private: DECLARE_SIGNAL(OnQueueAppendItem, std::weak_ptr<ConsoleLogItem>)
};

#endif

