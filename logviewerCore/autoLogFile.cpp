
#include "autoLogFile.h"
#include "ioFileStream.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

AutoLogFile::AutoLogFile(CCObject *parent)
    : CCObject(parent)
{
    m_autoLogPath = "autolog";
}

AutoLogFile::~AutoLogFile()
{ }

bool AutoLogFile::MakeFile(const std::string &datetime)
{
    std::string filename = stringFormat("%s\\autolog_%s.txt", m_autoLogPath, datetime);

    m_logFile = std::make_unique<IOFileStream>(filename);

    if (IOFileStream::Exist(m_autoLogPath) ? true : m_logFile->MakeDirectory())
        return m_logFile->Open(IOFileStream::OpenMode::WriteOnly);

    return false;
}

void AutoLogFile::CommitLog(const std::string &content, const std::string &datetime)
{
    m_contentList.emplace_back(stringFormat("[%s] %s\n", datetime, content));
}

bool AutoLogFile::WriteStringToBinary(const std::string &str)
{
    std::vector<uint8_t> bin(str.cbegin(), str.cend());

    return m_logFile->Write(bin);
}

bool AutoLogFile::WriteLogFile()
{
    while (m_contentList.size())
    {
        if (!WriteStringToBinary(m_contentList.front()))
            return false;

        m_contentList.pop_front();
    }
    return true;
}

void AutoLogFile::StartWrite()
{
    m_writeLogResult = std::async(std::bind(&AutoLogFile::WriteLogFile, this));
}
