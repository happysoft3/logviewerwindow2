
#include "netService.h"

NetService::NetService(CCObject *parent)
    : CCObject()
{ }

NetService::~NetService()
{ }

void NetService::OnInitializeOnce()
{ }

bool NetService::OnInitialize()
{
    return true;
}

bool NetService::OnStarted()
{
    return true;
}

void NetService::DeInitialize()
{ }

void NetService::OnHalted()
{ }

bool NetService::Startup()
{
    return OnInitialize() ? OnStarted() : false;
}

void NetService::Shutdown()
{
    DeInitialize();
    OnHalted();
}