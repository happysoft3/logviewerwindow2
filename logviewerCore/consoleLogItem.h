
#ifndef CONSOLE_LOG_ITEM_H__
#define CONSOLE_LOG_ITEM_H__

#include "binaryStream.h"
#include <array>
#include <string>

class ConsoleLogItem : public BinaryStream
{
private:
    int m_checkSum;
    std::array<uint16_t, 256> m_uniMessage;
    int m_color;
    int m_etx;

    std::string m_createdTime;

public:
    ConsoleLogItem();
    ~ConsoleLogItem() override;

private:
    std::string CurrentLocalTime();
    uint32_t Rgb565To888(uint16_t rgb565);
    uint32_t AdvanceToRgb(uint16_t src, bool preciseUpscale = true);

public:
    size_t FieldLength() const;
    bool ParseItem(const std::vector<uint8_t> &src);
    std::string GetContent();
    std::string CreatedTime() const
    {
        return m_createdTime;
    }
    uint32_t GetTextColor();
};

#endif

