
#ifndef NET_SERVICE_H__
#define NET_SERVICE_H__

#include "common/ccobject/ccobject.h"
#include <atomic>

class NetService : public CCObject
{
public:
    explicit NetService(CCObject *parent);
    ~NetService() override;

private:
    virtual void OnInitializeOnce();
    virtual bool OnInitialize();
    virtual bool OnStarted();
    virtual void DeInitialize();
    virtual void OnHalted();

public:
    bool Startup();
    void Shutdown();

private:
    std::mutex m_lock;
};

#endif

