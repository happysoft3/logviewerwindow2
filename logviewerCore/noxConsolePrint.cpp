
#include "noxConsolePrint.h"
#include "stringEncordUtil.h"
#include <iostream>
#include <cassert>

NoxConsolePrint::NoxConsolePrint()
    : ProcessCodePage()
{
    m_printMsg.fill(0);
    m_printColorId = static_cast<int>(PrintColorId::color_white);
}

NoxConsolePrint::~NoxConsolePrint()
{ }

void NoxConsolePrint::PrintConsoleImpl(const uint32_t *args)
{
    using vfunction_proto_type = void (_cdecl *)(int, const uint16_t *);
    uint32_t color = args[0];
    const uint16_t *message = reinterpret_cast<const uint16_t *>(&args[1]);
    vfunction_proto_type func = reinterpret_cast<vfunction_proto_type>(0x450b90);

    func(color, message);
}

void NoxConsolePrint::PrintConsoleSub()
{
    std::cout << "NoxConsolePrint::PrintConsoleSub" << std::endl;
}

uint32_t NoxConsolePrint::FieldLength() const
{
    return reinterpret_cast<uint32_t>(PrintConsoleSub) - reinterpret_cast<uint32_t>(PrintConsoleImpl);
}

uint32_t *NoxConsolePrint::ExecutionBase() const
{
    return reinterpret_cast<uint32_t *>(PrintConsoleImpl);
}

bool NoxConsolePrint::BindParams()
{
    try
    {
        WriteCtx(m_printColorId);

        for (const auto &us : m_printMsg)
            WriteCtx(us);
    }
    catch (const bool &fail)
    {
        assert(fail);
        return fail;
    }
    return true;
}

NoxConsolePrint *NoxConsolePrint::Clone()
{
    NoxConsolePrint *clone = new NoxConsolePrint;

    clone->m_printColorId = m_printColorId;
    clone->m_printMsg = m_printMsg;

    return clone;
}

void NoxConsolePrint::SetPrintMessage(const std::string &printMsg)
{
    std::wstring dest;
    StringEncordUtil::AnsiToUnicode(printMsg, dest);

    if (dest.length() >= m_printMsg.max_size())
        dest.resize(dest.length() - 1);

    std::copy(dest.cbegin(), dest.cend(), m_printMsg.begin());
}

void NoxConsolePrint::SetPrintColor(NoxConsolePrint::PrintColorId colorId)
{
    m_printColorId = static_cast<int>(colorId);
}
