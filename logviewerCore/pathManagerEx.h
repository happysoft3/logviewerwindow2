
#ifndef PATH_MANAGER_EX_H__
#define PATH_MANAGER_EX_H__

#include "common/utils/pathManager.h"
#include <memory>

class PathManagerEx : public PathManager
{
private:
    std::string m_params;

public:
    PathManagerEx();
    ~PathManagerEx();

private:
    bool paramValidCheck(const std::string &param) const;

public:
    void SetParams(const std::string &params);
    const char *const Params() const;
    void LoadPath(const PathManager &path);
    std::unique_ptr<PathManagerEx> Clone() const;
};

#endif

