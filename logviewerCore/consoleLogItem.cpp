
#include "consoleLogItem.h"
#include "stringEncordUtil.h"
#include <chrono>
#include <iostream>

ConsoleLogItem::ConsoleLogItem()
    : BinaryStream(1024)
{
    m_checkSum = 0;
    m_color = 0;
    m_etx = 0;
    m_uniMessage.fill(0);
}

ConsoleLogItem::~ConsoleLogItem()
{ }

size_t ConsoleLogItem::FieldLength() const
{
    return sizeof(m_uniMessage._Elems);
}

std::string ConsoleLogItem::CurrentLocalTime()
{
    std::chrono::system_clock::time_point n = std::chrono::system_clock::now();

    time_t currentTime = std::chrono::system_clock::to_time_t(n);

    char buffer[256] = { };

    ctime_s(buffer, sizeof(buffer), &currentTime);
    return buffer;
}

uint32_t ConsoleLogItem::Rgb565To888(uint16_t rgb565)
{
    uint32_t red = rgb565 & 0xf800 >> 11;
    uint32_t grn = (rgb565 & 0x7e0) >> 5;
    uint32_t blu = rgb565 & 0x1f;

    if (red)
        red = (red == 0x1f) ? 0xff : (red + 1) << 3;
    if (grn)
        grn = (grn == 0x3f) ? 0xff : (grn + 1) << 2;
    if (blu)
        blu = (blu == 0x1f) ? 0xff : (blu + 1) << 3;

    return (0xff << 0x18) | (red << 0x10) | (grn << 8) | blu;
}

uint32_t ConsoleLogItem::AdvanceToRgb(uint16_t src, bool preciseUpscale)
{
    uint32_t r = static_cast<uint8_t>((src & 0xFC00) >> 10);
    uint32_t g = static_cast<uint8_t>((src & 0x03E0) >> 5);
    uint32_t b = static_cast<uint8_t>(src & 0x1F);

    // Normalize color space 
    if (preciseUpscale) // Slower and more precise version
    {
        r = static_cast<uint32_t>(r / 31.0 * 255.0);
        g = static_cast<uint32_t>(g / 31.0 * 255.0);
        b = static_cast<uint32_t>(b / 31.0 * 255.0);
    }
    else
    {
        r <<= 3;
        g <<= 3;
        b <<= 3;
    }
    return static_cast<uint32_t>(0xFF000000) | (static_cast<uint32_t>(b) << 16) | (static_cast<uint32_t>(g) << 8) | static_cast<uint32_t>(r);
}

bool ConsoleLogItem::ParseItem(const std::vector<uint8_t> &src)
{
    PutStream(src);
    SetSeekpoint(0);

    try
    {
        ReadCtx(m_checkSum);
        for (auto &c : m_uniMessage)
            ReadCtx(c);
        ReadCtx(m_color);
        ReadCtx(m_etx);
    }
    catch (const bool &fail)
    {
        return fail;
    }
    m_createdTime = CurrentLocalTime();
    if (m_createdTime.back() == '\n')
        m_createdTime.resize(m_createdTime.size() - 1);
    return true;
}

std::string ConsoleLogItem::GetContent()
{
    std::wstring uniMsg;

    std::copy_if(m_uniMessage.cbegin(), m_uniMessage.cend(), std::insert_iterator<std::wstring>(uniMsg, uniMsg.begin()), [](const auto &us) { return us != 0; });

    std::string decode;

    StringEncordUtil::UnicodeToAnsi(uniMsg, decode);
    return decode;
}

uint32_t ConsoleLogItem::GetTextColor()
{
    return (AdvanceToRgb(m_color & 0xffff, false)) & 0xffffff;
}
