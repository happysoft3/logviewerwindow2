
#ifndef NOX_HANDLE_THREAD_H__
#define NOX_HANDLE_THREAD_H__

#include "common/taskthread/taskthread.h"

class PathManagerEx;
class ProcessMemory;
class HandleThreadLog;

class NoxHandleThread : public TaskThread
{
private:
    std::unique_ptr<PathManagerEx> m_runPath;
    std::unique_ptr<HandleThreadLog> m_log;

public:
    NoxHandleThread();
    ~NoxHandleThread() override;

private:
    void putTask(TaskQueue &queue);
    bool ensureCorrectExcutable();
    bool forceHideSdlPopupIfEnabled();
    bool retryEnsureCorrectApp();
    bool beforeExec(std::shared_ptr<TaskQueue> queue) override; //여기에서 테스크 추가함
    void afterExec(TaskResultHash &resultMap) noexcept override;

public:
    bool PutRunPath(const PathManagerEx &runPath);

    //시그널 2개
    ///- 녹스 켜짐완료(프로세스 생성완료)
    ///- 녹스 종료완료
public:
    DECLARE_SIGNAL(OnProcessCreated, std::shared_ptr<ProcessMemory>)

public:
    DECLARE_SIGNAL(OnProcessTerminated)
};

#endif

