
#include "noxHandleThread.h"
#include "noxExecTask.h"
#include "noxExecResult.h"
#include "../processMemory.h"
#include "../sdl/defaultCfgCollector.h"
#include "common/taskthread/taskqueue.h"
#include "common/taskthread/taskresulthash.h"
#include "../pathManagerEx.h"
#include "common/utils/stringhelper.h"
#include <fstream>

using namespace _StringHelper;

const auto s_hideConfig = "HIDE_CONFIG";
const auto s_multiInstance = "MULTIPLE_INSTANCES";

class HandleThreadLog
{
private:
    std::ofstream m_stream;

public:
    HandleThreadLog()
    { }
    ~HandleThreadLog()
    { }

    bool Open(const std::string &url)
    {
        m_stream = std::ofstream(url);

        return m_stream.is_open();
    }
    
    void Write(const std::string &content)
    {
        m_stream << content << std::endl;
    }
};

NoxHandleThread::NoxHandleThread()
    : TaskThread()
{
    m_log = std::make_unique<HandleThreadLog>();
    m_log->Open("runlog.txt");
}

NoxHandleThread::~NoxHandleThread()
{ }

void NoxHandleThread::putTask(TaskQueue &queue)
{
    auto execTask = std::make_unique<NoxExecTask>();

    execTask->PutRunPath(std::move(m_runPath));
    queue.Push(std::move(execTask));
}

bool NoxHandleThread::ensureCorrectExcutable()
{
    static constexpr auto app_version = "1.2b";
    std::ifstream executable(m_runPath->FullPath(), std::ios::binary);

    executable >> std::noskipws;
    if (!executable)
    {
        m_log->Write("exe forbid");
        return false;
    }

    executable.seekg(0x1870b4);
    std::string ver;

    executable >> ver;
    ver.resize(sizeof(app_version));
    if (ver == app_version)
        return true;

    m_log->Write("version mismatched");
    return false;
}

bool NoxHandleThread::forceHideSdlPopupIfEnabled()
{
    std::unique_ptr<CfgCollector> cfg = std::make_unique<DefaultCfgCollector>();

    if (!cfg->ReadCfg(stringFormat("%snoxgui.cfg", m_runPath->BasePath())))
        return false;

    m_log->Write("read cfg ok");
    std::string value;

    if (!cfg->GetValue(s_hideConfig, value))
        return false;
    m_log->Write("read cfg value ok");
    bool changed = false;
    if (value != "1")
    {
        cfg->SetValue(s_hideConfig, "1");
        changed = true;
    }

    if (!cfg->GetValue(s_multiInstance, value))
        return false;
    m_log->Write("read cfg value ok-2");
    if (value != "1")
    {
        cfg->SetValue(s_multiInstance, "1");
        changed = true;
    }
    if (changed)
        cfg->WriteCfg();
    return true;
}

bool NoxHandleThread::retryEnsureCorrectApp()
{
    m_runPath->SetSubName("game.exe");
    m_log->Write(m_runPath->FullPath());
    return ensureCorrectExcutable();
}

bool NoxHandleThread::beforeExec(std::shared_ptr<TaskQueue> queue)
{
    if (!m_runPath)
        return false;

    m_log->Write("runpath ok");

    //여기에서 하는 작업//
    //실행파일을 읽어서, 올바른 실행파일인지 검사=>아니면 false 반환
    if (!ensureCorrectExcutable())
    {
        if (!retryEnsureCorrectApp())
            return false;
    }

    m_log->Write("ensurecorrectexecutable-pass");
    if (!forceHideSdlPopupIfEnabled())  //show sdl popup 이 true 이면, false 으로 바꿔주기
        return false;

    m_log->Write("forceHideSdlPopupIfEnabled-pass");
    
    putTask(*queue);
    return true;
}

void NoxHandleThread::afterExec(TaskResultHash &resultMap) noexcept
{
    if (!resultMap.Empty())
    {
        auto res = resultMap.PopAsValidType<NoxExecResult>();
        std::shared_ptr<ProcessMemory> proc = res->DetachProcess();

        if (proc)
        {
            m_OnProcessCreated.QueueEmit(proc);
            res->WaitApp();
        }
        m_log->Write(res->LastError());
    }    
    m_OnProcessTerminated.QueueEmit();
}

bool NoxHandleThread::PutRunPath(const PathManagerEx &runPath)
{
    if (runPath.IsEmpty())
        return false;

    m_runPath = runPath.Clone();
    return true;
}


