
#ifndef NOX_EXEC_RESULT_H__
#define NOX_EXEC_RESULT_H__

#include "common/taskthread/taskresult.h"

class ExecDataParam;
class PathManagerEx;
class ProcessMemory;

class NoxExecResult : public TaskResult
{
private:
    std::unique_ptr<ExecDataParam> m_execParam;
    std::unique_ptr<ProcessMemory> m_process;
    std::string m_lastErrorMessage;

public:
    NoxExecResult();
    ~NoxExecResult() override;

    bool RunApp(const PathManagerEx &runPath);
    void WaitApp(); //it must be run at another thread
    std::unique_ptr<ProcessMemory> DetachProcess();
    const char *LastError() const;
};

#endif

