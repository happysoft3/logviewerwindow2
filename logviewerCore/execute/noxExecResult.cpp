
#include "noxExecResult.h"
#include "../processMemory.h"
#include "../win/wndHandle.h"
#include "../pathManagerEx.h"
#include "common/utils/stringhelper.h"
#include <windows.h>

using namespace _StringHelper;

class ExecDataParam
{
private:
    STARTUPINFOA m_startupInfo;
    PROCESS_INFORMATION m_theProcess;
    std::unique_ptr<WndHandle> m_handle;

public:
    ExecDataParam()
    {
        m_startupInfo = { };
        m_startupInfo.cb = sizeof(STARTUPINFOA);
        m_theProcess = { };
    }
    ~ExecDataParam()
    { }

private:
    operator LPSTARTUPINFOA()
    {
        return &m_startupInfo;
    }
    operator LPPROCESS_INFORMATION()
    {
        return &m_theProcess;
    }
    std::string convParams(const PathManagerEx &runPath)
    {
        std::string params = runPath.Params();

        if (params.empty())
            return{ };

        return stringFormat(" %s", params);
    }

public:
    bool Create(const PathManagerEx &runPath, std::string &err)
    {
        if (m_handle)
            return false;

        std::string cmd = runPath.FullPath();
        std::string params = convParams(runPath);

        if (!::CreateProcessA(toArray(cmd), params.empty() ? nullptr : toArray(params), nullptr, nullptr, 0, 0, nullptr, runPath.BasePath(), *this, *this))
        {
            err = stringFormat("error-id: %d",  ::GetLastError());
            return false;
        }
        m_handle = std::make_unique<WndHandle>(m_theProcess.hProcess);
        m_handle->PutChild(m_theProcess.hThread);
        return true;
    }

    void DoWait()
    {
        if (m_theProcess.hProcess)
        {
            ::WaitForSingleObject(m_theProcess.hProcess, INFINITE);
        }
    }

    std::unique_ptr<WndHandle> DetachHandle()
    {
        if (!m_handle)
            return{ };

        return std::move(m_handle);
    }
};

NoxExecResult::NoxExecResult()
    : TaskResult()
{ }

NoxExecResult::~NoxExecResult()
{ }

bool NoxExecResult::RunApp(const PathManagerEx &runPath)
{
    auto execParam = std::make_unique<ExecDataParam>();

    if (execParam->Create(runPath, m_lastErrorMessage))
    {
        m_execParam = std::move(execParam);
        m_process = std::make_unique<ProcessMemory>();
        return true;
    }
    return false;
}

void NoxExecResult::WaitApp()
{
    if (m_execParam)
    {
        m_execParam->DoWait();
    }
}

std::unique_ptr<ProcessMemory> NoxExecResult::DetachProcess()
{
    if (!m_execParam)
        return{ };

    if (!m_process)
        return{ };

    auto handle = m_execParam->DetachHandle();

    if (!handle)
        return{ };

    m_process->Bind(std::move(handle));
    return std::move(m_process);
}

const char *NoxExecResult::LastError() const
{
    if (m_lastErrorMessage.empty())
        return "NOERROR";

    return m_lastErrorMessage.c_str();
}
