
#ifndef NOX_EXEC_TASK_H__
#define NOX_EXEC_TASK_H__

#include "common/taskthread/taskunit.h"

class PathManagerEx;
class NoxExecResult;

class NoxExecTask : public TaskUnit
{
private:
    std::unique_ptr<PathManagerEx> m_runPath;
    std::unique_ptr<NoxExecResult> m_execResult;

public:
    NoxExecTask();
    ~NoxExecTask() override;

private:
    void runApp(const PathManagerEx &runPath);
    void Run() override;
    std::unique_ptr<TaskResult> GetResult() override;

public:
    void PutRunPath(std::unique_ptr<PathManagerEx> targetPath);
};

#endif

