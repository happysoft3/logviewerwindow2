
#ifndef EXEC_DUMP_TASK_H__
#define EXEC_DUMP_TASK_H__

#include "common/taskthread/taskunit.h"

class ExecDumpTask : public TaskUnit
{
public:
    ExecDumpTask();
    ~ExecDumpTask() override;
};

#endif

