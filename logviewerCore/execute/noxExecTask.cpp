
#include "noxExecTask.h"
#include "noxExecResult.h"
#include "../pathManagerEx.h"
#include <windows.h>

NoxExecTask::NoxExecTask()
    : TaskUnit()
{ }

NoxExecTask::~NoxExecTask()
{ }

void NoxExecTask::runApp(const PathManagerEx &runPath)
{
    m_execResult = std::make_unique<NoxExecResult>();
    m_execResult->RunApp(runPath);
}

void NoxExecTask::Run()
{
    std::unique_ptr<PathManagerEx> runPath = m_runPath ? std::move(m_runPath) : nullptr;

    if (m_execResult)
        m_execResult.reset();
    if (runPath)
        runApp(*runPath);
}

std::unique_ptr<TaskResult> NoxExecTask::GetResult()
{
    if (!m_execResult)
        return{ };

    return std::move(m_execResult);
}

void NoxExecTask::PutRunPath(std::unique_ptr<PathManagerEx> targetPath)
{
    if (!targetPath)
        return;

    if (targetPath->IsEmpty())
        return;

    m_runPath = std::move(targetPath);
}

