
#ifndef BINARY_BUFFER_H__
#define BINARY_BUFFER_H__

#include <vector>
#include <iterator>

class BinaryBuffer
{
	using container_type = std::vector<uint8_t>;
	using container_pointer_type = container_type * ;

private:
	container_type m_binBuffer;
	container_pointer_type m_pBuffer;

public:
	BinaryBuffer();
	virtual ~BinaryBuffer();

protected:
	void setBufferTarget(container_type &targ)
	{
		m_pBuffer = &targ;
	}

private:
	template <size_t N>
	struct stream_chunk_modifier
	{
		static constexpr size_t lshift_count = (N - 1) * 8;

		template <class Ty>
		static void Get(const container_type &srcBuffer, Ty &dest, size_t index)
		{
			stream_chunk_modifier<N - 1>::Get(srcBuffer, dest, index - 1);

			dest |= (srcBuffer[index] << lshift_count);
		}

		template <class Ty>
		static void Set(container_type &destBuffer, const Ty &src, size_t index)
		{
			stream_chunk_modifier<N - 1>::Set(destBuffer, src, index - 1);

			destBuffer[index] = (src >> lshift_count) & 0xff;
		}
	};

	template <>
	struct stream_chunk_modifier<1>
	{
		template <class Ty>
		static void Get(const container_type &srcBuffer, Ty &dest, size_t index)
		{
			dest |= srcBuffer[index];
		}

		template <class Ty>
		static void Set(container_type &destBuffer, const Ty &src, size_t index)
		{
			destBuffer[index] = src & 0xff;
		}
	};

public:
	template <class Ty, class = typename std::enable_if<std::is_arithmetic<Ty>::value>::type>
	bool GetC(Ty &dest, size_t index)
	{
		static constexpr size_t type_size = sizeof(Ty);

		if (index + type_size > m_binBuffer.size())
			return false;

		stream_chunk_modifier<type_size>::Get(m_binBuffer, dest, index + type_size - 1);
		return true;
	}

	template <class Ty, class = typename std::enable_if<std::is_arithmetic<Ty>::value>::type>
	bool SetC(const Ty &src, size_t index)
	{
		static constexpr size_t type_size = sizeof(Ty);

		if (index + type_size > m_binBuffer.size())
			return false;

		stream_chunk_modifier<type_size>::Set(m_binBuffer, src, index + type_size - 1);
		return true;
	}

	virtual void onBufferHas() {}
	virtual void onBufferPlaced() {}

public:
	template <class Container>
	void StreamPush(const Container &src)
	{
		if (src.empty())
			return;

		m_binBuffer.insert(m_binBuffer.end(), src.cbegin(), src.cend());
		onBufferHas();
	}

	template <class Container>
	void StreamPush(const Container &src, size_t length)
	{
		if (src.empty())
			return;

		length = (length == 0) ? src.size() : length;
		m_binBuffer.reserve(m_binBuffer.size() + length);
		std::copy_n(src.cbegin(), length, std::insert_iterator<container_type>(m_binBuffer, m_binBuffer.end()));
		onBufferHas();
	}

	template <class Container>
	void StreamSet(const Container &src)
	{
		m_binBuffer.resize(src.size());
		std::copy(src.cbegin(), src.cend(), m_binBuffer.begin());
		onBufferPlaced();
	}

	template <class Container>
	void StreamMove(Container &dest)
	{
		if (m_binBuffer.empty())
		{
			dest.clear();
			return;
		}
		if (dest.size() != m_binBuffer.size())
			dest.resize(m_binBuffer.size());
		std::copy(m_binBuffer.cbegin(), m_binBuffer.cend(), dest.begin());
		m_binBuffer.clear();
	}
	void Clear();
	bool Empty() const
	{
		return m_pBuffer->empty();
	}

private:
	virtual void onBufferClear() {}
	void debugLineImpl(const std::string &debug);
	virtual std::string debugLine(const std::string &debugString);

public:
	virtual bool CreateComponent();

	size_t Size() const
	{
		return m_pBuffer->size();
	}
	container_type::const_iterator Begin() const
	{
		return m_pBuffer->cbegin();
	}
	container_type::const_iterator End() const
	{
		return m_pBuffer->cend();
	}
};

#endif

