
#ifndef IP_TO_BYTE_H__
#define IP_TO_BYTE_H__

#include <string>
#include <vector>
#include <list>

class IpToByte
{
private:
	std::string m_ipAddress;
	std::string m_lastErrorMsg;
	std::list<char> m_tokens;

public:
	explicit IpToByte(const std::string &ipAddress = {});
	~IpToByte();

private:
	int ReadC();
	void UnreadC(int c);
	int ReadNumeric(int c);

public:
	void SetIpAddress(const std::string &ipAddr);

private:
	bool dispatchErr(const std::string &msg);

public:
	bool ToByteStream(std::vector<char> &dest);
	std::string LastError() const
	{
		return m_lastErrorMsg;
	}
};

#endif
