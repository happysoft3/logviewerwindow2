
#include "randomString.h"
#include <random>

constexpr const char ascii_charset_table[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

class RandomGenCore
{
private:
	std::random_device m_device;
	std::mt19937 m_generator;
	std::uniform_int_distribution<> m_range;

public:
	RandomGenCore()
		: m_generator(m_device()), m_range(0, sizeof(ascii_charset_table) - 2)
	{ }

	~RandomGenCore()
	{ }

	int Pull()
	{
		return m_range(m_generator);
	}
};

static RandomGenCore s_randomCore;

RandomString::RandomString()
{ }

RandomString::~RandomString()
{ }

void RandomString::Generate(std::string &s)
{
#if 0   //thread unsafety
	for (auto &c : s)
		c = ascii_charset_table[s_randomCore.Pull()];
#endif
    RandomGenCore rndgen;

    for (auto &c : s)
        c = ascii_charset_table[rndgen.Pull()];
}

std::string RandomString::Generate(size_t length)
{
	std::string out(length, '\0');

	Generate(out);
	return out;
}
