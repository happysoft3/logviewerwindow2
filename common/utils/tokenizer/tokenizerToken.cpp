
#include "tokenizerToken.h"
#include "../stringhelper.h"
#include <sstream>

using namespace _StringHelper;

TokenizerToken::TokenizerToken(TokenizerToken::Type ty)
{
	m_ty = ty;
}

TokenizerToken::~TokenizerToken()
{ }

StringStorageToken::StringStorageToken(TokenizerToken::Type ty, const std::string &s)
	: TokenizerToken(ty)
{
	m_str = s;
}

NumericToken::NumericToken(const std::string &s)
	: StringStorageToken(TokenizerToken::Type::NumberType, s)
{
	putNumber(s);
}

void NumericToken::putNumber(const std::string &s)
{
	std::stringstream ss;

	ss << s;
	ss >> m_number;
}

void NumericToken::PutString(const std::string &s)
{
    StringStorageToken::PutString(s);
    if (s.empty())
    {
        m_number = 0;
        return;
    }

    for (const auto &c : s)
    {
        if (c >= '0'&&c <= '9')
            continue;
        return;
    }
    putNumber(s);
}

KeywordToken::KeywordToken(int key)
	: TokenizerToken(TokenizerToken::Type::KeywordType)
{
	m_keyword = key;
}

const char *const KeywordToken::String() const
{
    return reinterpret_cast<const char *>(&m_keyword);
}

void KeywordToken::PutString(const std::string &s)
{
    if (s.empty())
        return;

    m_keyword = static_cast<int>( s.front() );
}

IdentToken::IdentToken(const std::string &s)
	: StringStorageToken(TokenizerToken::Type::IdentType, s)
{ }

StringToken::StringToken(const std::string &s)
	: StringStorageToken(TokenizerToken::Type::StringType, s)
{
    storeString(s);
}

void StringToken::storeString(const std::string &s)
{
    m_literal = stringFormat("\"%s\"", s);
}

const char *const StringToken::String() const
{
    if (m_literal.empty())
        return "\"\"";
    return toArray(m_literal);
}
