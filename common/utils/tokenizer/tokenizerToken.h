
#ifndef TOKENIZER_TOKEN_H__
#define TOKENIZER_TOKEN_H__

#include <string>

class TokenizerToken
{
public:
	enum class Type
	{
		SpaceType,
		NewlineType,
		EOFType,
		NumberType,
		KeywordType,
		IdentType,
		StringType,
	};

private:
	Type m_ty;

public:
	TokenizerToken(Type ty);
	virtual ~TokenizerToken();

	Type Class() const
	{
		return m_ty;
	}
	virtual const char *const String() const
	{
		return "";
	}
    virtual void PutString(const std::string &s)
    { }
	virtual int Value() const
	{
		return 0;
	}
	bool TypeExpect(Type ty)
	{
		return m_ty == ty;
	}
};

class SpaceToken : public TokenizerToken
{
public:
	SpaceToken()
		: TokenizerToken(TokenizerToken::Type::SpaceType)
	{ }
    ~SpaceToken() override
    { }

private:
	const char *const String() const override
	{
		return " ";
	}
};

class NewlineToken : public TokenizerToken
{
public:
	NewlineToken()
		: TokenizerToken(TokenizerToken::Type::NewlineType)
	{ }
    ~NewlineToken() override
    { }
};

class EofToken : public TokenizerToken
{
public:
	EofToken()
		: TokenizerToken(TokenizerToken::Type::EOFType)
	{ }
    ~EofToken() override
    { }
};

class StringStorageToken : public TokenizerToken
{
private:
	std::string m_str;

public:
	StringStorageToken(Type ty, const std::string &s);
    ~StringStorageToken() override
    { }

protected:
	const char *const stringData() const
	{
		return m_str.c_str();
	}
	virtual const char *const String() const
	{
		return stringData();
	}
    void PutString(const std::string &s) override
    {
        m_str = s;
    }
};

class NumericToken : public StringStorageToken
{
private:
	int m_number;

public:
	explicit NumericToken(const std::string &s);
    ~NumericToken() override
    { }

private:
	void putNumber(const std::string &s);
	int Value() const override
	{
		return m_number;
	}
    void PutString(const std::string &s) override;
};

class KeywordToken : public TokenizerToken
{
private:
	int m_keyword;

public:
	explicit KeywordToken(int key);
    ~KeywordToken() override
    { }
    const char *const String() const override;
	int Value() const override
	{
		return m_keyword;
	}
    void PutString(const std::string &s) override;
};

class IdentToken : public StringStorageToken
{
public:
	explicit IdentToken(const std::string &s);
    ~IdentToken() override
    { }
};

class StringToken : public StringStorageToken
{
private:
    std::string m_literal;

public:
	explicit StringToken(const std::string &s);
    ~StringToken() override
    { }

private:
    void storeString(const std::string &s);
    const char *const String() const override;
};

#endif

