
#include "cflush.h"

CFlush::CFlush(Context *context)
	: CCObject(context)
{
}

CFlush::~CFlush()
{
	Flush();
}

void CFlush::Flush()
{
	Invoke(getParent(), &Context::Put, m_pendingSlot.str());
}

CFlush::Context::Context()
	: CCObject()
{
}

CFlush::Context::~Context()
{
}


