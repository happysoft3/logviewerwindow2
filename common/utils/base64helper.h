
#ifndef BASE64_HELPER_H__
#define BASE64_HELPER_H__

#include "binaryHelper.h"

class Base64Helper : public BinaryHelper
{
private:
	std::string m_decodeQueue;
	std::vector<char> m_resultStream;

public:
	Base64Helper();
	~Base64Helper() override;

	static bool IsBase64Symbol(int c);
private:
	int decodePop();
	bool fillDecodeQueue(int &padCount);
	bool decodeImpl();

private:
	template <class Container, class Method>
	void commonExecution(Container &dest, Method &&fn)
	{
		while ((this->*std::forward<Method>(fn))());

		if (m_resultStream.empty())
		{
			dest.clear();
			return;
		}
		if (m_resultStream.size() != dest.size())
			dest.resize(m_resultStream.size());
		std::copy(m_resultStream.cbegin(), m_resultStream.cend(), dest.begin());
		m_resultStream.clear();
		Clear();
	}

public:
	template <class Container>
	void Decode(Container &dest)
	{
		commonExecution(dest, &Base64Helper::decodeImpl);
	}

private:
	int encodePop(int &padCount);
	bool encodeImpl();

public:
	template <class Container>
	void Encode(Container &dest)
	{
		commonExecution(dest, &Base64Helper::encodeImpl);
	}
	//static constexpr char s_base64_symbol[] = "base64";
};

#endif

