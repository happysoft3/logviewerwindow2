
#ifndef PATH_MANAGER_H__
#define PATH_MANAGER_H__

#include <string>

class PathManager
{
private:
    std::string m_targetUrl;
	std::string m_subTerm;

public:
    PathManager();
    ~PathManager();

    bool IsEmpty() const;
    const char *const SubName() const;
    void GetSubNameAndExt(std::string &subName, std::string &ext) const;
	void SetSubName(const std::string &subName);
    void SetUrl(const std::string &url);
    const char *BasePath() const;
    std::string FullPath(const std::string &subUrl) const;
	std::string FullPath() const;
};

#endif

