
#include "binaryHelper.h"

BinaryHelper::BinaryHelper()
	: BinaryBufferEx<char>()
{
	m_readPos = 0;
}

BinaryHelper::~BinaryHelper()
{ }

void BinaryHelper::onBufferClean()
{
	BinaryBufferEx<char>::onBufferClean();
	resetSeek();
}

bool BinaryHelper::next(int expect)
{
	if (peek() == expect)
	{
		pop();
		return true;
	}
	return false;
}

int BinaryHelper::pop()
{
	uint8_t uc = 0;

	if (!Getc(uc, m_readPos))
		return EOF;

	++m_readPos;
	return static_cast<int>(uc);
}

void BinaryHelper::push(int value)
{
	if (value == EOF)
	{
		return;
	}
	uint8_t uc = static_cast<uint8_t>(value);

	if (Setc(uc, m_readPos - 1))
		--m_readPos;
}

int BinaryHelper::peek()
{
	uint8_t uc = 0;

	if (!Getc(uc, m_readPos))
		return EOF;

	return uc;
}

void BinaryHelper::resetSeek()
{
	m_readPos = 0;
}

