
#include "runClock.h"
#include "stringhelper.h"
#include <time.h>

using namespace _StringHelper;

RunClock::RunClock()
{
	m_initClock = Sync();
	m_fmt = "end of process %s sec";
}

RunClock::~RunClock()
{ }

std::string RunClock::Get(bool reset)
{
	double cClock = static_cast<double>(clock());
	std::string str = stringFormat(m_fmt, std::to_string((cClock - (reset ? m_latestClock : m_initClock)) / CLOCKS_PER_SEC));

	if (reset)
		Sync();
	return str;
}

bool RunClock::SetFormat(const std::string &fmt)
{
	static constexpr char str_fmt[] = "%s";
	size_t pos = fmt.find(str_fmt);
	
	if (pos != std::string::npos)
	{
		if (fmt.find(str_fmt, pos + sizeof(str_fmt)) == std::string::npos)
		{
			m_fmt = fmt;
			return true;
		}
	}
	return false;
}

double RunClock::Sync()
{
	clock_t currentClock = clock();

	m_latestClock = currentClock;
	return currentClock;
}

