
#include "baseEncode64.h"

static std::vector<char> s_encodingTable =
{
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
	'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
	'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
	'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	'w', 'x', 'y', 'z', '0', '1', '2', '3',
	'4', '5', '6', '7', '8', '9', '+', '/' 
};

static std::vector<char> s_decodeTable(255);

class DecodeTableInit
{
public:
	DecodeTableInit()
	{
		char inc = 0;

		for (const char &c : s_encodingTable)
			s_decodeTable[c] = inc++;
	}
};

static DecodeTableInit s_makeDecodeTable;

class BaseEncode64::InvokeImpl
{
public:
	class EncodeInvoke;
	class DecodeInvoke;
private:
	BaseEncode64 *m_parent;

public:
	InvokeImpl(BaseEncode64 *parent)
		: m_parent(parent)
	{ }

protected:
	BaseEncode64 *operator->() const
	{
		return m_parent;
	}

public:
	virtual int operator()(int c) = 0;
	virtual void operator()() = 0;
};

class BaseEncode64::InvokeImpl::EncodeInvoke : public BaseEncode64::InvokeImpl
{
public:
	EncodeInvoke(BaseEncode64 *parent)
		: InvokeImpl(parent)
	{ }

private:
	int operator()(int c) override
	{
		return (*this)->encodePop(c);
	}
	void operator()() override
	{
		(*this)->encodePush();
	}
};

class BaseEncode64::InvokeImpl::DecodeInvoke : public BaseEncode64::InvokeImpl
{
public:
	DecodeInvoke(BaseEncode64 *parent)
		: InvokeImpl(parent)
	{ }

private:
	int operator()(int c) override
	{
		return (*this)->decodePop(c);
	}
	void operator()() override
	{
		(*this)->decodePush();
	}
};

static constexpr size_t s_base64_align = 3;

BaseEncode64::BaseEncode64(size_t reserveSize)
	: BinaryHelper(), m_entryVec(s_base64_align)		//항상, 3바이트//
{
	m_entryVecIter = m_entryVec.rbegin();
	m_resultVec.reserve(reserveSize);
	m_decodeInvoke = std::make_shared<InvokeImpl::DecodeInvoke>(this);
	m_encodeInvoke = std::make_shared<InvokeImpl::EncodeInvoke>(this);

	m_temporary = 0;
	m_bitCount = 0;
}

BaseEncode64::~BaseEncode64()
{ }

int BaseEncode64::pop()
{
	return (*m_currentInvoke)(BinaryHelper::pop());
}

void BaseEncode64::push(int value)
{
	BinaryHelper::push(value);

	(*m_currentInvoke)();
}

int BaseEncode64::encodePop(int c)
{
	*(m_entryVecIter++) = c;
	if (m_entryVecIter == m_entryVec.rend())
		m_entryVecIter = m_entryVec.rbegin();

	return c;
}

void BaseEncode64::encodePush()
{
	if (--m_entryVecIter == m_entryVec.rbegin())
		m_entryVecIter = std::prev(m_entryVec.rend());
}

void BaseEncode64::calcEncode()
{
	union Field
	{
		int dword;
		char bytes[4];
	} copyField;
	int k = 0, pad = -1;
	
	for (const auto &c : m_entryVec)
		copyField.bytes[k++] = (c == EOF) ? ((++pad) * 0) : c;

	int rShift = 18;
	for (; k >= 0; --k)
	{
		m_resultVec.push_back((pad >= k) ? '=' : s_encodingTable[(copyField.dword >> rShift) & 0x3f]);
		rShift -= 6;
	}
}

void BaseEncode64::encodeImpl()
{
	m_currentInvoke = m_encodeInvoke;

	for (;;)
	{
		for (const auto & u: m_entryVec)
			pop();

		calcEncode();
		if (peek() == EOF)
			break;
	}
}

int BaseEncode64::decodePop(int c)
{
	switch (c)
	{
	case '=': return EOF;
	case 0x0d: return pop();
	case 0x0a: return pop();
	default: return c;
	}
}

void BaseEncode64::decodePush()
{ }

void BaseEncode64::writeDecodeBuffer(std::vector<int> &buffer, size_t &bitCount, int &temp)
{
	std::vector<char> copyBuffer;

	while (buffer.size())
	{
		int c = buffer.back();

		buffer.pop_back();
		temp |= (((c == EOF) ? 0 : s_decodeTable[c]) << bitCount);
		bitCount += 6;
		if (bitCount >= 8)
		{
			copyBuffer.push_back(static_cast<char>(temp));
			temp >>= 8;
			bitCount -= 8;
		}
	}
	
	m_resultVec.insert(m_resultVec.end(), copyBuffer.rbegin(), copyBuffer.rend());
	buffer.clear();
}

void BaseEncode64::decodeImpl()
{
	m_currentInvoke = m_decodeInvoke;
	int c = 0, temp = 0;
	size_t bitCount = 0;
	std::vector<int> buffer;

	resetSeek();

	buffer.reserve(sizeof(int));

	do
	{
		for (int i = 4; i; i--)
		{
			c = pop();
			buffer.push_back(c);
			if (c == EOF)
				break;
		}
		writeDecodeBuffer(buffer, bitCount, temp);
	} while (c != EOF);
}

void BaseEncode64::writeDecodeBufferEx(std::vector<int> &buffer)
{
	std::vector<char> copyBuffer;

	while (buffer.size())
	{
		int c = buffer.back();

		buffer.pop_back();
		m_temporary |= (((c == EOF) ? 0 : s_decodeTable[c]) << m_bitCount);
		m_bitCount += 6;
		if (m_bitCount >= 8)
		{
			copyBuffer.push_back(static_cast<char>(m_temporary));
			m_temporary >>= 8;
			m_bitCount -= 8;
		}
	}

	m_resultVec.insert(m_resultVec.end(), copyBuffer.rbegin(), copyBuffer.rend());
	buffer.clear();
}

void BaseEncode64::decodeImplEx()
{
	m_currentInvoke = m_decodeInvoke;
	int c = 0;
	std::vector<int> buffer;

	buffer.reserve(sizeof(int));

	do
	{
		for (int i = 4; i; i--)
		{
			c = pop();
			buffer.push_back(c);
			if (c == EOF)
				break;
		}
		writeDecodeBufferEx(buffer);
	} while (c != EOF);
}

void BaseEncode64::resetSeek()
{
	BinaryHelper::resetSeek();

	m_bitCount = 0;
	m_temporary = 0;
}

