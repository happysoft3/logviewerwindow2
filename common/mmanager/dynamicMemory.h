
#ifndef DYNAMIC_MEMORY_H__
#define DYNAMIC_MEMORY_H__

#include <string>
#include <sstream>
#include <functional>

class DynamicMemory
{
public:
	DynamicMemory();
	virtual ~DynamicMemory();

protected:
	void *createMemory(size_t sz);
	void deleteMemory(void *ptr);

private:
	virtual void convAddress(std::stringstream &ss) const
	{
		ss << 0;
	}
	std::string loadAddressImpl(std::function<void(std::stringstream&)> &&f) const;

protected:
	std::string loadAddress(DynamicMemory &other);
	template <class Ty>
	std::string loadAddressWithPTR(Ty *ptr)
	{
		return loadAddressImpl([=](std::stringstream &ss)
		{
			ss << ptr;
		});
	}
};

template <class Ty>
class DynamicRaw : public DynamicMemory
{
private:
	Ty *m_raw;

public:
	DynamicRaw(size_t sz = 0)
		: DynamicMemory()
	{
		m_raw = reinterpret_cast<Ty *>(createMemory(sz ? sz : sizeof(Ty)));
	}
	~DynamicRaw() override
	{
		deleteMemory(reinterpret_cast<void *>(m_raw));
	}

private:
	void convAddress(std::stringstream &ss) const override
	{
		ss << m_raw;
	}

public:
	Ty *Get() const
	{
		return m_raw;
	}
};

template <class Ty>
class DynamicObject : public DynamicMemory
{
private:
	Ty *m_object;

public:
	template <class... Args>
	DynamicObject(Args&& ...args)
	{
		m_object = new Ty(std::forward<Args>(args)...);
	}
	~DynamicObject() override
	{
		delete m_object;
	}

private:
	void convAddress(std::stringstream &ss) const override
	{
		ss << m_object;
	}

public:
	Ty *Get() const
	{
		return m_object;
	}
};

#endif

