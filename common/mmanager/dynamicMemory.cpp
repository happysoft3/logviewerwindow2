
#include "dynamicMemory.h"
#include <cstdlib>

DynamicMemory::DynamicMemory()
{ }

DynamicMemory::~DynamicMemory()
{ }

void *DynamicMemory::createMemory(size_t sz)
{
	return malloc(sz);
}

void DynamicMemory::deleteMemory(void *ptr)
{
	free(ptr);
}

std::string DynamicMemory::loadAddressImpl(std::function<void(std::stringstream&)> &&f) const
{
	std::string sTarget;
	std::stringstream ss(sTarget);
	std::string address;

	f(ss);
	ss >> address;
	return address;
}

std::string DynamicMemory::loadAddress(DynamicMemory &other)
{
	return loadAddressImpl([&other](std::stringstream &ss)
	{
		other.convAddress(ss);
	});
}
