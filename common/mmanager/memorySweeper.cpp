
#include "memorySweeper.h"

MemorySweeper::MemorySweeper()
	: DynamicMemory()
{ }

MemorySweeper::~MemorySweeper()
{
	if (!m_memMap.empty())
		m_memMap.clear();
	if (!m_memList.empty())
		m_memList.clear();
}

void MemorySweeper::putDynamic(std::unique_ptr<DynamicMemory> mem)
{
	std::string key = loadAddress(*mem);

	m_memList.push_back(std::move(mem));
	m_memMap[key] = std::prev(m_memList.end());
}

std::unique_ptr<DynamicMemory> MemorySweeper::releaseDynamic(const std::string &address)
{
	auto memIterator = m_memMap.find(address);

	if (memIterator == m_memMap.cend())
		return {};

	std::unique_ptr<DynamicMemory> ret = std::move(*memIterator->second);

	m_memList.erase(memIterator->second);
	m_memMap.erase(memIterator);
	return ret;
}
