
#ifndef TASK_RESULT_QUEUE_H__
#define TASK_RESULT_QUEUE_H__

#include "taskObject.h"
#include <map>
#include <memory>
#include <string>

class TaskResult;

class TaskResultHash : public TaskObject
{
private:
	std::map<std::string, std::unique_ptr<TaskResult>> m_rets;

public:
	TaskResultHash();
	~TaskResultHash() override;

	bool Empty() const
	{
		return m_rets.empty();
	}
	int Count() const
	{
		return m_rets.size();
	}
	void Put(const std::string &key, std::unique_ptr<TaskResult> res);
	void Preorder(std::function<void(TaskResult &)> &&fn);
	std::unique_ptr<TaskResult> Pop();

	template <class Ty, class = typename std::enable_if<std::is_base_of<TaskResult, Ty>::value, Ty>::type>
	std::unique_ptr<Ty> PopAsValidType()
	{
		auto src = Pop();

		if (!src)
			return {};

		auto test = dynamic_cast<Ty *>(src.release());

		if (!test)
			throw std::exception("bad pointer cast");
		return std::unique_ptr<Ty>(test);
	}

	void Merge(TaskResultHash &other);
};

#endif

