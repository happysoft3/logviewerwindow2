
#include "taskThread.h"
#include "taskQueue.h"
#include "taskResultHash.h"
#include "taskResult.h"
#include "taskUnit.h"

TaskThread::TaskThread()
	: TaskObject()
{ }

TaskThread::~TaskThread()
{ }

void TaskThread::doTask(TaskUnit &task)
{
	task.Run();
}

void TaskThread::popTaskQueue(TaskResultHash &resultMap, TaskQueue &queue)
{
	for (;;)
	{
		auto task = queue.Pop();

		if (!task)
			break;

		doTask(*task);

		std::unique_ptr<TaskResult> ret = task->GetResult();

		if (ret)
			resultMap.Put(task->Index(), std::move(ret));
	}
}

TaskThread::task_result_ty TaskThread::doPerform(std::shared_ptr<TaskQueue> queue)
{
	std::lock_guard<std::mutex> guard(m_lock);
	auto resultMap = std::make_unique<TaskResultHash>();

	try
	{
		if (beforeExec(queue))
			popTaskQueue(*resultMap, *queue);
	}
	catch (std::exception &e)
	{
		PutError(e);
	}
	afterExec(*resultMap);
	return resultMap;
}

void TaskThread::Perform(std::shared_ptr<TaskQueue> queue)
{
	if (!queue)
		return;

	auto fut = std::async(std::launch::async, &TaskThread::doPerform, this, std::move(queue));

	m_returnTask = std::make_unique<std::future<task_result_ty>>(std::move(fut));
}

TaskThread::task_result_ty TaskThread::GetResult()
{
	if (!m_returnTask)
		return {};

	task_result_ty out = m_returnTask->get();

	m_returnTask.reset();
	return out;
}

void TaskThread::Wait()
{
	std::lock_guard<std::mutex> guard(m_lock);


}
