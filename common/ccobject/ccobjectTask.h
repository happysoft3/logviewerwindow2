
#ifndef CCOBJECT_TASK_H__
#define CCOBJECT_TASK_H__

#include "taskthread/taskUnit.h"

class CCObjectTask : public TaskUnit
{
private:
	std::weak_ptr<CCObject::Core> m_target;
	std::function<void()> m_invoke;
	std::unique_ptr<TaskResult> m_result;

public:
	CCObjectTask(std::weak_ptr<CCObject::Core> target, std::function<void()> &&slotInvoke);
	~CCObjectTask() override;

private:
	void Run() override;
	std::unique_ptr<TaskResult> GetResult() override;
};

#endif

