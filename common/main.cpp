﻿// common.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "utils/base64helper.h"
#include "utils/qPrintEncode.h"
#include <iostream>
#include <string>

int main()
{
	Base64Helper encode;
	/*std::string src = "ManVsWild";

	encode.StreamSet(src);
	std::string encodeDest;

	encode.Encode(encodeDest);

	std::cout << "encode: " << encodeDest << std::endl;*/
	/////

	std::string decodeDest;
	std::string encSrc = "ODgvRSAxNzE0NjkvTiA5L1QgNTEyMjIwL0ggWyA1MDUgMjY3XT4+DWVuZG9iag0gICAgICAg";
	encode.StreamPut(encSrc);
	encode.Decode(decodeDest);

	std::cout << "decode: " << decodeDest << std::endl;

	encode.Decode(decodeDest);
	std::cout << "decode: " << decodeDest << std::endl;

	QPrintEncode qEncode;

	//std::string qPrintSrc = "=?UTF-8?Q?IDB_ATTACH_REPLY=21.bmp?=";
	std::string qPrintSrc = "=?UTF-8?Q?=EC=9B=90=EA=B2=A9=EC=A0=91=EC=86=8D=ED=94=84=EB=A1=9C=EA=B7=B8=EB=9E=A8=EB=A7=A4=EB=89=B4=EC=96=BC_20200227.pdf?=";

	qEncode.StreamPut(qPrintSrc);
	if (qEncode.Decode())
	{
		std::cout << qEncode.Type() << std::endl;
		std::cout << qEncode.Symbol() << std::endl;
		std::cout << qEncode.Value() << std::endl;
	}
	else
		std::cout << qEncode.GetError() << std::endl;

	return 0;
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
