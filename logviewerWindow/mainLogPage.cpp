
#include "stdafx.h"
#include "mainLogPage.h"
#include "notifyDialog.h"

#include "resource.h"

IMPLEMENT_DYNAMIC(MainLogPage, CTabPage)

void MainLogPage::DerivedCom::SlotReceiveListItem(const std::list<std::shared_ptr<ListElement>> &itemlist)
{
    auto wndCore = m_wndCore.lock();

    if (!wndCore)
        return;

    MainLogPage *page = dynamic_cast<MainLogPage *>(wndCore->m_parentWnd);

    if (page == nullptr)
        return;

    page->ViewInsert(itemlist);
}

MainLogPage::MainLogPage(CWnd *parent)
    : CTabPage(IDD_LOG_PANEL, parent)
{ }

MainLogPage::~MainLogPage()
{ }

void MainLogPage::OnClickedOutputButton(const std::string &outputType)
{
    std::string content = m_textInput.GetTextString();

    if (content.length())
    {
        m_textInput.SetWindowTextA({ });

        GetCommunication()->OnClickButton().QueueEmit( outputType, content);
    }
}

std::unique_ptr<CTabPage::PageCom> MainLogPage::MakeCommunication()
{
    return std::make_unique<DerivedCom>();
}

void MainLogPage::InitialCreateLogMessagebox()
{
    m_logMsgbox = std::make_unique<NotifyDialog>(this);

    m_logMsgbox->InitialCreate();
    m_logMsgbox->ToggleCopyMsgFeature(true);
}

void MainLogPage::OnInitialUpdate()
{
    CTabPage::OnInitialUpdate();

    InitialCreateLogMessagebox();

    LogListViewer::ListColumn listHeader;
    CRect viewArea;

    m_logViewer.GetClientRect(&viewArea);

    int width = viewArea.right;
    auto setWidthFx = [&width](int columnWidth)
    {
        width = (width < columnWidth) ? 0 : width - columnWidth;
        return (width == 0) ? width : columnWidth;
    };

    listHeader.Append("Message", setWidthFx(520));
    listHeader.Append("Datetime", setWidthFx(180));
    listHeader.Append("", width);

    m_logViewer.AttachListColumn(listHeader);

    m_textInput.SetBackgroundColor(RGB(130, 172, 185));
    m_textInput.SetTextColor(RGB(163, 73, 64));

    m_sendChatBtn.SetCallback([this]() { this->OnClickedOutputButton("chat"); });
    m_sendCmdBtn.SetCallback([this]() { this->OnClickedOutputButton("comd"); });
    m_logViewer.SetDoubleClickCallback([this](int col, int row) { this->OpenLogMessageBox(col, row); });
    m_textInput.SetCallbackOnPressedEnter(std::bind(&MainLogPage::OnClickedOutputButton, this, "chat"));
}

void MainLogPage::InitialDDX(std::function<void(int, CWnd&)> &&exchanger)
{
    m_sendChatBtn.ModifyWndName("Send");
    m_sendCmdBtn.ModifyWndName("Command");

    exchanger(LOGPAGE_BTN_CHAT, m_sendChatBtn);
    exchanger(LOGPAGE_BTN_CMD, m_sendCmdBtn);
    exchanger(LOGPAGE_LIST, m_logViewer);
    exchanger(LOGPAGE_INPUT, m_textInput);
}

void MainLogPage::ViewInsert(const std::list<std::shared_ptr<ListElement>> &items)
{
    m_logViewer.AppendList(items);
}

void MainLogPage::OpenLogMessageBox(int columnId, int rowId)
{
    if (rowId == 0)
    {
        CString logmsg = m_logViewer.GetItemText(columnId, 0);

        m_logMsgbox->Emerge(std::string(logmsg.GetBuffer()));
    }
}

void MainLogPage::ValidAreaSetting(CDC &cdc)
{
    SetValidArea(cdc, m_logViewer);
    SetValidArea(cdc, m_textInput);
    SetValidArea(cdc, m_sendChatBtn);
    SetValidArea(cdc, m_sendCmdBtn);
}

void MainLogPage::DrawStuff(CDC &cdc)
{
    CTabPage::DrawStuff(cdc);
}

bool MainLogPage::onEraseBackground(CDC *pDC)
{
    CRect area;

    m_logViewer.GetWindowRect(area);
    ScreenToClient(area);
    pDC->ExcludeClipRect(area);
    DrawStuff(*pDC);
    return true;
}

MainLogPage::DerivedCom *MainLogPage::GetCommunication() const
{
    return (m_com == nullptr) ? nullptr : dynamic_cast<DerivedCom *>(m_com.get());
}
