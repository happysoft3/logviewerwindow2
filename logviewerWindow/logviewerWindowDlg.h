
// logviewerWindowDlg.h : 헤더 파일
//

#pragma once

#include <memory>
#include <string>

class PageManager;
class CoreUi;
class NotifyDialog;
class CWnd;

// ClogviewerWindowDlg 대화 상자
class ClogviewerWindowDlg : public CDialogEx
{
    class MainWndCC;
    friend MainWndCC;

private:
    static constexpr UINT c_mainWndUserMessage = WM_USER + 1;
    static constexpr UINT c_userMessageEndProcess = 0;
    static constexpr UINT c_userMessageStartProcess = 1;

private:
    std::unique_ptr<CBrush> m_wndColor;
    std::unique_ptr<PageManager> m_controlPageMan;
    std::unique_ptr<PageManager> m_infoPageMan;
    std::unique_ptr<PageManager> m_mainPageMan;
    std::unique_ptr<PageManager> m_titlePageMan;
    std::unique_ptr<MainWndCC> m_wndcc;
    std::unique_ptr<CoreUi> m_coreui;
    std::shared_ptr<NotifyDialog> m_msgbox;
    int m_titleEdgeWidth;
    int m_titleHeight;
    CBitmap m_bkBmp;
    CSize m_bkbmpSize;
    std::string m_appVer;
    std::unique_ptr<CDialog> m_miniWnd;

// 생성입니다.
public:
	ClogviewerWindowDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
    ~ClogviewerWindowDlg() override;

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LOGVIEWERWINDOW_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

private:
    void InitPageManager();
    void InitScreenInstance();
    void LastSaveAutolog();
    void DeInitPageManager();
    void InitialMessageBoxCreate();
    void InitialLoadBackground();
    bool InitMainWindow();

public:
    void SetAppVersion(const std::string &ver)
    {
        m_appVer = ver;
    }

private:
    void MainScreenSwitching(const std::string &screenId);
    void DrawMainWindow(CDC *pDC);
    void showPopupMessage(const std::string &msg);

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
    afx_msg BOOL OnEraseBkgnd(CDC *pDC);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
    afx_msg void OnDestroy();
    afx_msg void OnClose();
    afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);
    BOOL PreTranslateMessage(MSG *pMsg);
	DECLARE_MESSAGE_MAP()
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg HRESULT OnUserMessageReceived(WPARAM wParam, LPARAM lParam);
};
