
// logviewerWindow.h : PROJECT_NAME 응용 프로그램에 대한 주 헤더 파일입니다.
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH에 대해 이 파일을 포함하기 전에 'stdafx.h'를 포함합니다."
#endif

#include "resource.h"		// 주 기호입니다.
#include <string>

// ClogviewerWindowApp:
// 이 클래스의 구현에 대해서는 logviewerWindow.cpp을 참조하십시오.
//

class ClogviewerWindowApp : public CWinApp
{
private:
    ULONG_PTR m_gdiplusToken;
    HANDLE m_hMutex;
    std::string m_version;

public:
	ClogviewerWindowApp();

private:
    void runUpdate(const std::string &app);
    bool doCheck();
    bool checkUpdateVersion();

// 재정의입니다.
public:
	virtual BOOL InitInstance();
    virtual int ExitInstance();

// 구현입니다.

	DECLARE_MESSAGE_MAP()
};

extern ClogviewerWindowApp theApp;