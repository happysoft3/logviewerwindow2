#ifndef PAGE_MANAGER_H__
#define PAGE_MANAGER_H__

#include "common/ccobject/ccobject.h"
#include <string>
#include <map>

class PageManager : public CCObject
{
private:
    CCreateContext m_context;
    CRect m_screenArea;
    CWnd *m_parentWnd;

    using page_deleter_ty = std::function<void(CWnd *)>;
    std::map<std::string, std::unique_ptr<CWnd, page_deleter_ty>> m_objectMap;
    std::string m_currentScreenName;
    std::weak_ptr<CCObject> m_self;

public:
    explicit PageManager(const CWnd &target, CWnd *parent);
    PageManager(const PageManager &) = delete;
    PageManager(PageManager &&) = delete;
    virtual ~PageManager() override;
    PageManager &operator=(const PageManager &) = delete;

    bool MakePage(const std::string &pageKey, std::unique_ptr<CWnd> wndObject);
    bool ShowPage(const std::string &pageKey);

    //friend struct ConcretePageManager;
    void DestroyAll();

private:
    void SetSelf(std::shared_ptr<CCObject> self);

    CWnd *FindScreen(const std::string &screenName);

    //���ø� �߰�??
    //��ũ���̸�, ����PTR
public:
    template <class DestScreenPointer, class = typename std::enable_if<std::is_pointer<DestScreenPointer>::value>::type>
    bool GetPageInstance(const std::string &screenName, DestScreenPointer &dest)
    {
        auto wnd = FindScreen(screenName);

        if (wnd == nullptr)
            return false;

        auto castedWnd = dynamic_cast<DestScreenPointer>(wnd);

        if (castedWnd == nullptr)
            return false;

        dest = castedWnd;
        return true;
    }

    std::string CurrentPageName() const
    {
        return m_currentScreenName;
    }

    bool GetPageRect(CRect &rect, CWnd *parentWnd = nullptr);
    //void DrawCurrentPage(CDC *pDC, CWnd *parentWnd);
};

#endif
