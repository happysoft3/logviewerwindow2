#include "stdafx.h"
#include "imageButton.h"

ImageButton::ImageButton()
    : CMFCButton()
{
    m_imgType = "PNG";
    m_resourceId = 0;

    m_receiver = nullptr;
    m_onClicked = nullptr;
}

ImageButton::~ImageButton()
{ }

void ImageButton::UpdateImage(UINT resourceId)
{
    m_resourceId = resourceId;
    CImage resourceImg;
    HRESULT hResult = resourceImg.Load(CreateResourceStream());

    if (hResult != S_OK)
        return;
    HBITMAP hBit;

    hBit = resourceImg.Detach();
    m_nFlatStyle = CMFCButton::BUTTONSTYLE_NOBORDERS;
    m_bTransparent = TRUE;
    m_bDrawFocus = FALSE;

    SetImage(hBit, TRUE, nullptr);
    SetMouseCursorHand();
}

void ImageButton::SetClickCallback(CWnd *receiver, clickCallbackType method)
{
    m_receiver = receiver;
    m_onClicked = method;
}

IStream *ImageButton::CreateResourceStream()
{
    IStream * ipStream = NULL;
    LPCSTR lpName = MAKEINTRESOURCE(m_resourceId);

    HRSRC hrsrc = FindResource(NULL, lpName, m_imgType);
    if (hrsrc == NULL)
        goto Return;

    DWORD dwResourceSize = SizeofResource(NULL, hrsrc);
    HGLOBAL hglbImage = LoadResource(NULL, hrsrc);
    if (hglbImage == NULL)
        goto Return;

    LPVOID pvSourceResourceData = LockResource(hglbImage);
    if (pvSourceResourceData == NULL)
        goto Return;

    HGLOBAL hgblResourceData = GlobalAlloc(GMEM_MOVEABLE, dwResourceSize);
    if (hgblResourceData == NULL)
        goto Return;

    LPVOID pvResourceData = GlobalLock(hgblResourceData);

    if (pvResourceData == NULL)
        goto FreeData;

    CopyMemory(pvResourceData, pvSourceResourceData, dwResourceSize);

    GlobalUnlock(hgblResourceData);

    if (SUCCEEDED(CreateStreamOnHGlobal(hgblResourceData, TRUE, &ipStream)))
        goto Return;

FreeData:
    GlobalFree(hgblResourceData);

Return:
    return ipStream;
}

void ImageButton::OnLButtonDown(UINT nFlags, CPoint point)
{
    CMFCButton::OnLButtonDown(nFlags, point);

    if (m_onClicked != nullptr)
        m_onClicked(m_receiver);
}

BEGIN_MESSAGE_MAP(ImageButton, CMFCButton)
    ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()
