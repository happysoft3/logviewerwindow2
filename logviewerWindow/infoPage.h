
#ifndef INFO_PAGE_H__
#define INFO_PAGE_H__

#include "cTabPage.h"
#include "textlabel.h"
#include "imagebutton.h"

class InfoPage : public CTabPage
{
    DECLARE_DYNAMIC(InfoPage)

public:
    class DerivedCom : public CTabPage::PageCom
    {
        friend InfoPage;

    public:
        void SlotConnectStatChanged(bool state);
    };

private:
    TextLabel m_infoTitleText;
    TextLabel m_infoStatusText;
    ImageButton m_infoIconBtn;

public:
    explicit InfoPage(UINT nIdTemplate, CWnd *parent);
    ~InfoPage() override;

private:
    void OnInitialUpdate() override;
    std::unique_ptr<PageCom> MakeCommunication() override;
    void InitialDDX(std::function<void(int, CWnd&)> &&dataExchanger) override;
    void ChangeStatusText(bool state);

public:
    DerivedCom *GetCommunication() const override;

//private:
//    void ValidAreaSetting(CDC &cdc) override;
};

#endif

