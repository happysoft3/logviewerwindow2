
#ifndef MENU_TITLE_PAGE_H__
#define MENU_TITLE_PAGE_H__

#include "cTabPage.h"
#include "textLabel.h"
#include <map>

class MenuTitlePage : public CTabPage
{
    DECLARE_DYNAMIC(MenuTitlePage)
private:
    std::map<std::string, std::string> m_pageTitleMap;

public:
    class DerivedCom : public CTabPage::PageCom
    {
        friend MenuTitlePage;

    public:
        void SlotScreenChanged(const std::string &screenId);
    };

private:
    TextLabel m_titleText;
    CFont m_pageFont;

public:
    explicit MenuTitlePage(UINT nIdTemplate, CWnd *parent);
    ~MenuTitlePage() override;

private:
    std::unique_ptr<PageCom> MakeCommunication() override;
    void OnInitialUpdate() override;
    void InitialDDX(std::function<void(int, CWnd&)> &&changer) override;
    void ChangeTitle(const std::string &title);

public:
    DerivedCom *GetCommunication() const override;
    void AppendPageTitle(const std::string &pageName, const std::string &explain);

private:
    void DrawStuff(CDC &cdc) override;
};

#endif

