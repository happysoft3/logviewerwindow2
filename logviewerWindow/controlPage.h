
#ifndef CONTROL_PAGE_H__
#define CONTROL_PAGE_H__

#include "cTabPage.h"
#include "prettyButton.h"

class ControlPage : public CTabPage
{
    DECLARE_DYNAMIC(ControlPage)

    class DerivedCom : public CTabPage::PageCom
    {
    public:
        DECLARE_SIGNAL(OnChangeScreen, std::string)
    };

private:
    PrettyButton m_launcherPageButton;
    PrettyButton m_settingPageButton;
    PrettyButton m_logPageButton;
    PrettyButton m_sdlSettingButton;

public:
    explicit ControlPage(UINT nIdTemplate, CWnd *parent);
    ~ControlPage() override;

private:
    void PageSwitchTriggered(const std::string &screenId);
    void OnInitialUpdate() override;
    std::unique_ptr<PageCom> MakeCommunication() override;
    void InitialDDX(std::function<void(int, CWnd&)> &&dataExchanger) override;

public:
    DerivedCom *GetCommunication() const override;

private:
    void ValidAreaSetting(CDC &cdc) override;
};



#endif

