#include "stdafx.h"
#include "menuTitlePage.h"
#include "common/utils/stringhelper.h"
#include "resource.h"

using namespace _StringHelper;

void MenuTitlePage::DerivedCom::SlotScreenChanged(const std::string &screenId)
{
    auto wndCore = m_wndCore.expired() ? nullptr : m_wndCore.lock();

    if (!wndCore)
        return;

    MenuTitlePage *titlePage = dynamic_cast<MenuTitlePage *>(wndCore->m_parentWnd);

    if (titlePage == nullptr)
        return;

    titlePage->ChangeTitle(screenId);
}

IMPLEMENT_DYNAMIC(MenuTitlePage, CTabPage)

MenuTitlePage::MenuTitlePage(UINT nIdTemplate, CWnd *parent)
    : CTabPage(nIdTemplate, parent)
{ }

MenuTitlePage::~MenuTitlePage()
{ }

std::unique_ptr<CTabPage::PageCom> MenuTitlePage::MakeCommunication()
{
    return std::make_unique<DerivedCom>();
}

void MenuTitlePage::OnInitialUpdate()
{
    CTabPage::OnInitialUpdate();

    m_titleText.SetTextColor(RGB(0, 134, 191));
    m_titleText.SetWindowTextA("Ready to use");

    m_pageFont.CreateFontA(30, 20, 0, 0, FW_BOLD, FALSE, FALSE, 0,
        DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, "Calibri");
    m_titleText.SetFont(&m_pageFont);
}

void MenuTitlePage::InitialDDX(std::function<void(int, CWnd&)> &&changer)
{
    changer(MENUTITLE_TEXT, m_titleText);
}

void MenuTitlePage::ChangeTitle(const std::string &title)
{
    std::string pageExplain = "Untitled";
    auto iter = m_pageTitleMap.find(title);

    if (iter != m_pageTitleMap.cend())
        pageExplain = iter->second;

    m_titleText.SetWindowTextA(toArray(pageExplain));
}

MenuTitlePage::DerivedCom *MenuTitlePage::GetCommunication() const
{
    return (m_com == nullptr) ? nullptr : dynamic_cast<DerivedCom*>(m_com.get());
}

void MenuTitlePage::AppendPageTitle(const std::string &pageName, const std::string &explain)
{
    auto iter = m_pageTitleMap.find(pageName);
    
    if (iter != m_pageTitleMap.cend())
    {
        iter->second = explain;
        return;
    }
    m_pageTitleMap[pageName] = explain;
}

void MenuTitlePage::DrawStuff(CDC &cdc)
{
    CPen pen(PS_SOLID, 3, RGB(63, 72, 204));
    CBrush brush(RGB(28, 200, 200));
    CPen *oldPen = cdc.SelectObject(&pen);
    CBrush *oldBrush = cdc.SelectObject(&brush);
    CRect border;

    cdc.GetClipBox(&border);
    cdc.Rectangle(&border);

    cdc.SelectObject(oldPen);
    cdc.SelectObject(oldBrush);
}

