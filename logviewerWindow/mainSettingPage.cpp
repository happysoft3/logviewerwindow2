
#include "stdafx.h"
#include "mainSettingPage.h"

#include "logviewerCore/pathManagerEx.h"
#include "common/utils/stringhelper.h"
#include "resource.h"
#include <sstream>
#include <future>

using namespace _StringHelper;

IMPLEMENT_DYNAMIC(MainSettingPage, CTabPage)

void MainSettingPage::DerivedCom::SlotGetSyncData(const std::string &type, const std::string &value)
{
    auto wndCore = m_wndCore.lock();
    MainSettingPage *page = static_cast<MainSettingPage *>(wndCore->m_parentWnd);

    if (!wndCore)
        return;
    if (type == "limit")
        page->UpdateMaxLogCount(value);
    else if (type == "path")
        page->UpdateAutologPath(value);
}

MainSettingPage::MainSettingPage(CWnd *parent, std::shared_ptr<PathManagerEx> targPath)
    : CTabPage(IDD_SETTING_PANEL, parent)
{
    m_targPath = targPath;
}

MainSettingPage::~MainSettingPage()
{ }

std::unique_ptr<CTabPage::PageCom> MainSettingPage::MakeCommunication()
{
    return std::make_unique<DerivedCom>();
}

void MainSettingPage::TryConnect()
{
    DerivedCom *com = (m_com == nullptr) ? nullptr : dynamic_cast<DerivedCom *>(m_com.get());

    if (com == nullptr)
        return;

    com->OnTryConnect().QueueEmit(std::string( "connect") );
}

void MainSettingPage::ChangeRunParam()
{
    std::string input = m_paramInput.GetTextString();

    if (input.length() > 16)
        input.resize(16);
    GetCommunication()->OnRequestSetSyncData().QueueEmit(std::string( "param" ), input);
}

void MainSettingPage::ChangeMaxLogCount()
{
    std::string input = m_maxLogInput.GetTextString();

    if (input.empty())
        return;

    if (input.length() > 5)
        input.resize(5);

    for (const auto &c : input)
    {
        switch (c)
        {
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            break;
        default:
            return;
        }
    }

    GetCommunication()->OnRequestSetSyncData().QueueEmit(std::string( "limit" ), input);
    UpdateMaxLogCount(input);
    m_maxLogInput.SetWindowTextA({ });
}

void MainSettingPage::ChangeAutologPath()
{
    std::string input = m_autologPathInput.GetTextString();

    if (input.empty() || input.length() > 12)
        return;

    for (const auto &c : input)
    {
        if (!isalnum(c))
            return;
    }

    GetCommunication()->OnRequestSetSyncData().Emit("path", input);
}

void MainSettingPage::RequestOpenExplorer()
{
    GetCommunication()->OnRequestOpenLogFolder().Emit();
}

void MainSettingPage::InitialDDX(std::function<void(int, CWnd&)> &&dataExchanger)
{
    dataExchanger(SETPAGE_TOP, m_topBorder);
    dataExchanger(SETPAGE_CENTER, m_centerBorder);
    //dataExchanger(SETPAGE_BOTTOM, m_bottomBorder);
    //dataExchanger(SETPAGE_BTN_CONNECT, m_connectBtn);
    dataExchanger(IDC_EDIT1, m_gamePathText);
    dataExchanger(SETPAGE_CONNECT_TEXT, m_connectDescText);
    dataExchanger(SETPAGE_AUTOLOG_MAX_TEXT, m_maxLogText);
    dataExchanger(SETPAGE_AUTOLOGMAX_INPUT, m_maxLogInput);
    dataExchanger(SETPAGE_OPENDIR_TEXT, m_openDirText);
    dataExchanger(SETPAGE_BTN_OPENDIR, m_openDirBtn);
    dataExchanger(SETPAGE_AUTOLOGMAX_BTN, m_maxLogBtn);
    dataExchanger(SETPAGE_AUTOLOG_PATH_TEXT, m_autologPathText);
    dataExchanger(SETPAGE_AUTOLOGPATH_INPUT, m_autologPathInput);
    dataExchanger(SETPAGE_AUTOLOGPATH_BTN, m_autologPathBtn);

    dataExchanger(SETPAGE_PARAM_INPUT, m_paramInput);
    dataExchanger(SETPAGE_PARAM_TEXT, m_paramText);
    dataExchanger(SETPAGE_PARAM_BTN, m_paramBtn);
}

void MainSettingPage::OnInitialUpdate()
{
    CTabPage::OnInitialUpdate();

    //m_connectBtn.ModifyWndName("CONNECT");
    m_connectDescText.SetWindowTextA("Your game path");
    m_openDirText.SetWindowTextA("Open log file browser");
    m_openDirBtn.ModifyWndName("OPEN LOG FOLDER");
    UpdateMaxLogCount("0");
    m_maxLogBtn.ModifyWndName("Save");
    m_autologPathBtn.ModifyWndName("Save");
    m_autologPathText.SetLabelText("the log will saved here-->");
    m_maxLogInput.SetBackgroundColor(RGB(63, 72, 204));
    m_maxLogInput.SetTextColor(RGB(197, 234, 243));
    m_autologPathInput.SetBackgroundColor(RGB(63, 72, 204));
    m_autologPathInput.SetTextColor(RGB(197, 234, 243));
    m_maxLogBtn.SetCallback(std::bind(&MainSettingPage::ChangeMaxLogCount, this));
    //m_connectBtn.SetCallback(std::bind(&MainSettingPage::TryConnect, this));
    m_openDirBtn.SetCallback(std::bind(&MainSettingPage::RequestOpenExplorer, this));
    m_gamePathText.SetWindowTextA("nil");
    m_gamePathText.EnableWindow(FALSE);

    m_paramText.SetWindowTextA("App execute param");
    m_paramInput.SetBackgroundColor(RGB(63, 72, 204));
    m_paramInput.SetTextColor(RGB(197, 234, 243));
    m_paramBtn.ModifyWndName("Save");
    m_paramBtn.SetCallback([this]() { ChangeRunParam(); });
}

void MainSettingPage::OnEnterScreen()
{
    GetCommunication()->OnSettingPageUpdate().Emit();
    if (m_targPath)
    {
        m_gamePathText.SetWindowTextA(toArray(m_targPath->FullPath()));
        m_paramInput.SetWindowTextA(m_targPath->Params());
    }
}

void MainSettingPage::UpdateMaxLogCount(const std::string &limit)
{
    std::stringstream ss(limit);
    int num = 0;

    ss >> num;
    m_maxLogInput.SetWindowTextA(toArray(limit));
    m_maxLogText.SetLabelText(stringFormat("It will change a limitation for log messages: %d", num));
}

void MainSettingPage::UpdateAutologPath(const std::string &path)
{
    m_autologPathInput.SetWindowTextA(toArray(path));
}

MainSettingPage::DerivedCom *MainSettingPage::GetCommunication() const
{
    return (m_com == nullptr) ? nullptr : dynamic_cast<DerivedCom*>(m_com.get());
}

void MainSettingPage::ValidAreaSetting(CDC &cdc)
{
    SetValidArea(cdc, m_topBorder);
    SetValidArea(cdc, m_centerBorder);
    //SetValidArea(cdc, m_bottomBorder);
    SetValidArea(cdc, m_autologPathBtn);
    SetValidArea(cdc, m_autologPathInput);
    //SetValidArea(cdc, m_connectBtn);
    SetValidArea(cdc, m_maxLogBtn);
    SetValidArea(cdc, m_maxLogInput);
}
