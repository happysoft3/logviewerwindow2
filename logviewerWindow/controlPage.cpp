
#include "stdafx.h"
#include "controlPage.h"

#include "resource.h"

IMPLEMENT_DYNAMIC(ControlPage, CTabPage)

ControlPage::ControlPage(UINT nIdTemplate, CWnd *parent)
    : CTabPage(nIdTemplate, parent)
{ }

ControlPage::~ControlPage()
{ }

void ControlPage::PageSwitchTriggered(const std::string &screenId)
{
    DerivedCom *com = (m_com != nullptr) ? dynamic_cast<DerivedCom *>(m_com.get()) : nullptr;

    if (com == nullptr)
        return;

    com->OnChangeScreen().QueueEmit( screenId);
}

void ControlPage::OnInitialUpdate()
{
    CTabPage::OnInitialUpdate();

    m_launcherPageButton.ModifyWndName("LAUNCHER");
    m_logPageButton.ModifyWndName("LOG PAGE");
    m_settingPageButton.ModifyWndName("SETTING");
    m_sdlSettingButton.ModifyWndName("SDL SETTING");
}

std::unique_ptr<CTabPage::PageCom> ControlPage::MakeCommunication()
{
    return std::make_unique<DerivedCom>();
}

void ControlPage::InitialDDX(std::function<void(int, CWnd &)> &&dataExchanger)
{
    dataExchanger(CONTROL_SHOWLOG, m_logPageButton);
    dataExchanger(CONTROL_SHOWSETTING, m_settingPageButton);
    dataExchanger(CONTROL_LAUNCHER, m_launcherPageButton);
    dataExchanger(CONTROL_SDLSET, m_sdlSettingButton);

    m_logPageButton.SetCallback([this]() { this->PageSwitchTriggered("logListPage"); });
    m_settingPageButton.SetCallback([this]() { this->PageSwitchTriggered("settingPage"); });
    m_sdlSettingButton.SetCallback([this]() {this->PageSwitchTriggered("sdlOptionPage"); });
    m_launcherPageButton.SetCallback([this]() {this->PageSwitchTriggered("launcherPage"); });
}

ControlPage::DerivedCom *ControlPage::GetCommunication() const
{
    return (m_com == nullptr) ? nullptr : dynamic_cast<DerivedCom*>(m_com.get());
}

void ControlPage::ValidAreaSetting(CDC &cdc)
{
    SetValidArea(cdc, m_logPageButton);
    SetValidArea(cdc, m_settingPageButton);
}
