#include "stdafx.h"
#include "TextInput.h"
#include "cbufferdc.h"
#include <string>

TextInput::TextInput()
    : CEdit(), m_textColor(RGB(0, 0, 0))
{
    m_borderColor = RGB(255, 0, 0);
}

TextInput::~TextInput()
{ }

void TextInput::SetBackgroundColor(UINT rgb)
{
    m_bkColor = rgb;
    m_bkBrush = std::make_unique<CBrush>(m_bkColor);
}

void TextInput::SetTextColor(UINT textColor, UINT borderColor)
{
    if (borderColor != -1)
        m_borderColor = borderColor;

    m_textColor = textColor;
}

int TextInput::GetWindowTextNumber()
{
    CString numberString;

    GetWindowText(numberString);
    return _ttoi(numberString);
}

int TextInput::GetWindowTextNumber(std::function<int(int)> functor)
{
    CString numberString;

    GetWindowText(numberString);
    return functor(_ttoi(numberString));
}

void TextInput::SetWindowTextNumber(int number)
{
    std::string numberString = std::to_string(number);

    SetWindowText(CString(numberString.c_str()));
}

std::string TextInput::GetTextString()
{
    CString textString;

    GetWindowText(textString);
    return textString.GetBuffer();
}

void TextInput::PreSubclassWindow()
{
    SetBackgroundColor(RGB(255, 255, 255));
    FitTextLengthWithControl();
}

void TextInput::FitTextLengthWithControl()
{
    TEXTMETRIC tm;
    CDC *pDC = GetDC();
    CRect rect;

    pDC->GetTextMetrics(&tm);
    ReleaseDC(pDC);
    GetRect(&rect);
    LimitText(rect.Width() / tm.tmAveCharWidth);
}

void TextInput::DrawStuff(CDC *pDC, CBrush *paint)
{
    CPen pen(PS_SOLID, 1, m_borderColor);
    CPen *oldPen = pDC->SelectObject(&pen);
    CBrush *oldPaint = pDC->SelectObject(paint);

    CRect border;

    GetClientRect(&border);

    pDC->Rectangle(&border);
    pDC->SelectObject(oldPen);
    pDC->SelectObject(oldPaint);
}

BEGIN_MESSAGE_MAP(TextInput, CEdit)
    ON_WM_CTLCOLOR_REFLECT()
    ON_WM_CONTEXTMENU()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
END_MESSAGE_MAP()


HBRUSH TextInput::CtlColor(CDC* pDC, UINT nCtlColor)
{
    pDC->SetTextColor(m_textColor);
    pDC->SetBkColor(m_bkColor);

    return *m_bkBrush;
}

BOOL TextInput::PreTranslateMessage(MSG* pMsg)
{
    // TODO: Add your specialized code here and/or call the base class
    if (pMsg->message == WM_KEYDOWN)
    {
        switch (pMsg->wParam)
        {
        case 'v': case 'V':
        case 'c': case 'C':
            if (GetKeyState(VK_CONTROL) & 0x80)
            {
                SetSel(0, -1);
                return TRUE;
            }
            break;
        case VK_RETURN:
            if (m_onPressedEnter)
                m_onPressedEnter();
        default:
            break;
        }
    }

    return CEdit::PreTranslateMessage(pMsg);
}

void TextInput::OnContextMenu(CWnd* pWnd, CPoint point)
{ }

BOOL TextInput::OnEraseBkgnd(CDC *pDC)
{
    CBrush paint(m_bkColor);

    DrawStuff(pDC, &paint);
    return TRUE;
}

void TextInput::OnPaint()
{
    CBufferDC cdc(this);
    CBrush paint;

    paint.CreateStockObject(NULL_BRUSH);
    DefWindowProc(WM_PAINT, (WPARAM)cdc.m_hDC, (LPARAM)0);
    DrawStuff(&cdc, &paint);
}

