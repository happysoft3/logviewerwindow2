
#include "stdafx.h"
#include "coreUi.h"
#include "logviewerCore\gameWatcher.h"
#include "logviewerCore\logNotifier.h"
#include "logviewerCore\consoleLogItem.h"
#include "logItemElement.h"
#include "logviewerCore\systemFileModifier.h"
#include "logviewerCore/pathManagerEx.h"
#include "common/utils/myDebug.h"
#include "common/utils/stringhelper.h"

#include <sstream>

using namespace _StringHelper;

CoreUi::CoreUi(CCObject *parent)
    : CCObject(parent)
{
    m_sysFile = std::make_unique<SystemFileModifier>("appdata.txt");
    m_game = std::make_unique<GameWatcher>();
    m_targPath = std::make_shared<PathManagerEx>();
}

CoreUi::~CoreUi()
{ }

void CoreUi::InitCoreUi()
{
    m_game->WatcherInit();
    m_game->OnDisconnected().Connection(&CoreUi::SlotOnDisconnect, this);
    m_game->OnConnected().Connection(&CoreUi::slotOnConnected, this);

    LogNotifier *notifier = m_game->GetLogNotifier();

    if (notifier != nullptr)
        notifier->OnReleaseLogList().Connection(&CoreUi::SlotDestinateLogList, this);

    initialSettingSystemFile();
}

//std::string CoreUi::TryReconnect()
//{
//    GameWatcher::GameState state = m_game->DoConnect();
//
//    QUEUE_EMIT(m_OnConnectionChanged, state != GameWatcher::GameState::Failure);
//
//    switch (state)
//    {
//    case GameWatcher::GameState::AlreadyConnection: return "already_done";
//    case GameWatcher::GameState::Failure: return "failure";
//    case GameWatcher::GameState::Succeed: return "connect";
//    default: return{ };
//    }
//}

void CoreUi::SlotDestinateLogList(const std::list<std::weak_ptr<ConsoleLogItem>> &logList)
{
    std::list<std::shared_ptr<ListElement>> items;

    for (const auto &logItem : logList)
    {
        auto logReal = logItem.lock();

        if (logReal)
        {
            std::shared_ptr<ListElement> addedItem(new LogItemElement);

            addedItem->SetElement(LogItemElement::PropertyInfo::LogMessage, logReal->GetContent());
            addedItem->SetElement(LogItemElement::PropertyInfo::LogDatetime, logReal->CreatedTime());
            addedItem->SetTextColor(logReal->GetTextColor());

            items.push_back(addedItem);
        }
    }
    m_OnReleaseListItem.QueueEmit( items);
}

void CoreUi::SlotOnDisconnect()
{
    m_OnConnectionChanged.QueueEmit( false);
}

void CoreUi::slotOnConnected()
{
    m_OnConnectionChanged.QueueEmit(true);
}

void CoreUi::initialSettingSystemFile()
{
    std::string gamePath;
    std::string runParams;

    if (m_sysFile->Open())
    {
        size_t maxLog = 0;

        if (m_sysFile->GetMaxAutoLog(maxLog))
            m_game->AccessLogParam("limit", std::to_string(maxLog));
        m_sysFile->GetGamePath(gamePath);
        m_sysFile->GetRunParams(runParams);
        int download = 0;
        m_sysFile->GetMapdownloadSpeed(download);
        MY_PRINT() << __FUNCSIG__ << download;
        m_game->ChangeMapDownloadSpeed(download);
    }
    auto targPath = std::make_unique<PathManagerEx>();

    if (gamePath.empty())
    {
        auto altPath= m_game->GetAlternativePath();

        if (!altPath)
            MY_THROW() << "no alternative path";
        targPath->LoadPath(*altPath);
    }
    else
    {
        targPath->SetUrl(gamePath);
    }

    m_sysFile->SetGamePath(targPath->FullPath());
    *m_targPath = *targPath;
    if (!runParams.empty())
        m_targPath->SetParams(runParams);
}

void CoreUi::SaveAutolog()
{
    m_game->ForceWriteAutoLog();
    m_sysFile->Save();
}

void CoreUi::SlotSendOutputTask(const std::string &task, const std::string &message)
{
    if (message.empty())
        return;

    m_game->CommitOutTask(task, message);
}

void CoreUi::SlotUpdateSettingPage()
{
    std::string logLimit = m_game->AccessLogParam("limit");
    std::string path = m_game->AccessLogParam("path");

    m_OnSettingPageSyncData.Emit("limit", logLimit);
    m_OnSettingPageSyncData.Emit("path", path);
}

void CoreUi::SlotAutologSyncDataChanged(const std::string &type, const std::string &value)
{
    if (type == "limit")
    {
        std::string maxLogCountString = m_game->AccessLogParam("limit", value);
        size_t maxLog = 0;
        std::stringstream ss(maxLogCountString);

        ss >> maxLog;
        m_sysFile->SetMaxAutoLog(maxLog);
    }
    else if (type == "path")
    {
        m_sysFile->SetAutologPath(value);
    }
    else if (type == "param")
        m_targPath->SetParams(value);
}

void CoreUi::SlotOpenAutoLogFolderBrowser()
{
    const std::string logPath = m_game->AccessLogParam("path");

    ::WinExec(toArray(stringFormat("explorer %s", logPath)), SW_SHOW);
}

void CoreUi::SlotStartgame()
{
    std::string errMsg;

    if (!m_game->RunApp(*m_targPath, errMsg))
    {
        /////Todo. showing an error
        m_OnReleaseMessage.Emit(errMsg);
    }
}

void CoreUi::GetAppResult()
{
    m_game->TerminateAppCompletely();
    m_sysFile->SetRunParams(m_targPath->Params());
}

void CoreUi::TryAppPatch()
{
    m_game->DoPatch();
}

bool CoreUi::GameIsRunning()
{
    return m_game->IsRunning();
}

