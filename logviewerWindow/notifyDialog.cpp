
#include "stdafx.h"
#include "notifyDialog.h"

#include "common/utils/stringhelper.h"

#include "resource.h"

using namespace _StringHelper;

NotifyDialog::NotifyDialog(CWnd *parentWnd)
    : CDialogEx(/*IDD_NOTIFY_DLG, parentWnd*/)
{
    m_parentWindow = parentWnd;
    m_textCopied = false;
}

NotifyDialog::~NotifyDialog()
{ }

void NotifyDialog::DoDataExchange(CDataExchange *pDX)
{
    CDialogEx::DoDataExchange(pDX);

    DDX_Control(pDX, NOTIFY_DLG_TITLE_TEXT, m_titleText);
    DDX_Control(pDX, NOTIFY_DLG_TEXT, m_bodyText);
    DDX_Control(pDX, NOTIFY_DLG_TOP_BORDER, m_topBorder);
    DDX_Control(pDX, NOTIFY_DLG_BOTTOM_BORDER, m_bottomBorder);
    DDX_Control(pDX, NOTIFY_DLG_CLOSE, m_closeBtn);
    DDX_Control(pDX, NOTIFY_DLG_COPYTEXT_BTN, m_txtcopyBtn);
    DDX_Control(pDX, NOTIFY_DLG_COPYTEXT_TEXT, m_textcopyText);

    InitCControls();
}

BOOL NotifyDialog::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    CenterWindow();
    return TRUE;
}

void NotifyDialog::WindowCentering()
{
    if (m_parentWindow == nullptr)
        return;

    CRect parentArea, selfArea;

    m_parentWindow->GetWindowRect(&parentArea);
    GetWindowRect(&selfArea);

    int xpos = ((parentArea.right - parentArea.left) / 2) - ((selfArea.right - selfArea.left) / 2);
    int ypos = ((parentArea.bottom - parentArea.top) / 2) - ((selfArea.bottom - selfArea.top) / 2);

    MoveWindow(parentArea.left + xpos, parentArea.top + ypos, selfArea.Width(), selfArea.Height(), false);
}

void NotifyDialog::HideThisWindow()
{
    m_textCopied = false;
    ShowWindow(SW_HIDE);
}

void NotifyDialog::InitCControls()
{
    m_titleText.SetWindowTextA("Message Box");
    m_closeBtn.ModifyWndName("X");
    m_closeBtn.SetCallback([this]() { this->HideThisWindow(); });
    m_txtcopyBtn.ModifyWndName("Copy this message");
    m_textcopyText.SetLabelText("Done");
    m_textcopyText.ShowWindow(SW_HIDE);
    m_txtcopyBtn.SetCallback(std::bind(&NotifyDialog::MessageCopyToClipboard, this));
}

void NotifyDialog::InitialCreate()
{
    Create(IDD_NOTIFY_DLG, m_parentWindow);
    
    ToggleCopyMsgFeature(false);
}

void NotifyDialog::Emerge(const std::string &message)
{
    m_bodyText.SetWindowTextA(toArray(message));
    m_textcopyText.ShowWindow(SW_HIDE);
    WindowCentering();
    ShowWindow(SW_SHOW);
    SetForegroundWindow();
    SetActiveWindow();
    EnableWindow(true);
}

void NotifyDialog::ToggleCopyMsgFeature(bool use)
{
    m_txtcopyBtn.ShowWindow(use ? SW_SHOW : SW_HIDE);
    m_txtcopyBtn.EnableWindow(use);
}

void NotifyDialog::MessageCopyToClipboard()
{
    std::string message = m_bodyText.GetLabelText();

    if (message.empty())
        return;
    if (m_textCopied)
        return;

    if (message.length() > 255)
        message.resize(255);

    HGLOBAL clBoard;

    if (OpenClipboard())
    {
        m_textCopied = true;
        EmptyClipboard();
        size_t copylength = message.length() + sizeof(message.front());
        clBoard = GlobalAlloc(GMEM_DDESHARE, copylength);
        char *buffer = reinterpret_cast<char *>(GlobalLock(clBoard));
        memcpy_s(buffer, copylength, message.data(), copylength);
        GlobalUnlock(clBoard);
        SetClipboardData(CF_TEXT, clBoard);
        CloseClipboard();
        m_textcopyText.ShowWindow(SW_SHOW);
    }
}

BEGIN_MESSAGE_MAP(NotifyDialog, CDialogEx)
    ON_WM_PAINT()
    ON_WM_ACTIVATE()
END_MESSAGE_MAP()

void NotifyDialog::OnPaint()
{
    Default();
}

BOOL NotifyDialog::PreTranslateMessage(MSG *pMsg)
{
    if (pMsg->message == WM_KEYDOWN)
    {
        switch (pMsg->wParam)
        {
        case VK_RETURN:
        case VK_ESCAPE:
        case VK_F1:
            return TRUE;
        }
    }
    return CDialogEx::PreTranslateMessage(pMsg);
}

void NotifyDialog::OnActivate(UINT nState, CWnd *pWndOther, BOOL bMinimized)
{
    CDialogEx::OnActivate(nState, pWndOther, bMinimized);

    switch (nState)
    {
    case WA_INACTIVE:   //lost focus
        HideThisWindow();
        break;

    case WA_ACTIVE:     //get focus
    case WA_CLICKACTIVE:
        break;
    }
}

