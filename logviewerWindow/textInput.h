#ifndef TEXT_INPUT_H__
#define TEXT_INPUT_H__

#include <memory>
#include <functional>
#include <string>

class TextInput : public CEdit
{
private:
    std::unique_ptr<CBrush> m_bkBrush;
    UINT m_bkColor;
    UINT m_textColor;
    UINT m_borderColor;
    std::function<void()> m_onPressedEnter;

public:
    TextInput();
    virtual ~TextInput() override;

    void SetBackgroundColor(UINT rgb);
    void SetTextColor(UINT textColor, UINT borderColor = -1);
    void SetCallbackOnPressedEnter(std::function<void()> onPressedEnter)
    {
        m_onPressedEnter = onPressedEnter;
    }

    int GetWindowTextNumber();
    int GetWindowTextNumber(std::function<int(int)> functor);
    void SetWindowTextNumber(int number);

    void Empty() {
        SetWindowText({ });
    }
    std::string GetTextString();

private:
    void PreSubclassWindow() override;
    void FitTextLengthWithControl();
    void DrawStuff(CDC *pDC, CBrush *paint);

protected:
    DECLARE_MESSAGE_MAP()
    afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
    afx_msg BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
    afx_msg BOOL OnEraseBkgnd(CDC *pDC);
    afx_msg void OnPaint();

};

#endif
