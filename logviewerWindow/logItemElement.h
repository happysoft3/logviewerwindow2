
#ifndef LOG_ITEM_ELEMENT_H__
#define LOG_ITEM_ELEMENT_H__

#include "listElement.h"

class LogItemElement : public ListElement
{
public:
    struct PropertyInfo
    {
        enum PropertyId
        {
            LogMessage,
            LogDatetime,
            EndProperty
        };
    };

private:
    std::string m_logMessage;
    std::string m_dateTime;

public:
    LogItemElement();
    ~LogItemElement() override;

private:
    std::string GetElement(int index) override;
    void SetElement(int index, const std::string &value) override;
};

#endif

