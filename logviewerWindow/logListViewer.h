
#ifndef LOG_LIST_VIEWER_H__
#define LOG_LIST_VIEWER_H__

#include <list>
#include <tuple>
#include <memory>
#include <vector>
#include <mutex>
#include <functional>

class ListHeaderPanel;
class ListElement;

class LogListViewer : public CListCtrl
{
    DECLARE_DYNAMIC(LogListViewer)

    using list_element_ty = std::shared_ptr<ListElement>;
public:
    class ListColumn;

private:
    std::unique_ptr<ListHeaderPanel> m_headerPanel;
    std::vector<list_element_ty> m_elementVec;
    int m_selectedColumn;
    std::function<void(int, int)> m_onDbClick;

public:
    LogListViewer();
    ~LogListViewer() override;

public:
    void AttachListColumn(const ListColumn &columnData);
    void SetDoubleClickCallback(std::function<void(int, int)> onDbClick);

private:
    bool ListPulling(const size_t inputSize);
    void UpdateListItem(const size_t count, bool isFull);

public:
    void AppendList(const std::list<list_element_ty> &addList);

private:
    void EnableHighlighting(HWND hWnd, int row, bool bHighlight);
    bool IsRowSelected(HWND hWnd, int row);
    bool IsRowHighlighted(HWND hWnd, int row);
    void drawListBackground(CDC &cdc);

private:
    void drawListCell(CDC &cdc, const CRect &area);
    void drawList(CDC &cdc);

protected:
    void PreSubclassWindow() override;
    DECLARE_MESSAGE_MAP()
    afx_msg BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT *pResult);
    afx_msg void OnGetDisplayInfoList(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnNMDbClick(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg BOOL OnEraseBkgnd(CDC *pDC);
    afx_msg void OnPaint();

private:
    std::recursive_mutex m_lock;
};

class LogListViewer::ListColumn
{
    friend LogListViewer;
private:
    std::list<std::tuple<std::string, int>> m_columns;

public:
    ListColumn()
    { }

    ~ListColumn()
    { }

    void Append(const std::string &columnName, int width)
    {
        m_columns.emplace_back(columnName, width);
    }
};


#endif

