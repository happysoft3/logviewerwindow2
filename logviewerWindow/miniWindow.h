
#ifndef MINI_WINDOW_H__
#define MINI_WINDOW_H__

#include "textlabel.h"

class MiniWindow : public CDialogEx
{
private:
    std::unique_ptr<CBrush> m_defaultBrush;
    std::unique_ptr<CBrush> m_highlightBrush;
    TextLabel m_miniWndTxt;
    bool m_pressed;

public:
    explicit MiniWindow();
    ~MiniWindow() override;

private:
    BOOL OnInitDialog() override;
    void DoDataExchange(CDataExchange *pDX) override;
    DECLARE_MESSAGE_MAP()
    BOOL Create(UINT nIDTemplate, CWnd *pParentWnd) override;
    void alignWindow();
    afx_msg void OnLButtonDblClk(UINT, CPoint);
    afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);    
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
    afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);
};

#endif

