
#ifndef MAIN_SETTING_PAGE_H__
#define MAIN_SETTING_PAGE_H__

#include "cTabPage.h"
#include "groupBox.h"
#include "prettyButton.h"
#include "textLabel.h"
#include "textInput.h"

class PathManagerEx;

class MainSettingPage : public CTabPage
{
    DECLARE_DYNAMIC(MainSettingPage)

    class DerivedCom : public CTabPage::PageCom
    {
    public:
        void SlotGetSyncData(const std::string &type, const std::string &value);
        
    public: DECLARE_SIGNAL(OnTryConnect, std::string)
    public: DECLARE_SIGNAL(OnSettingPageUpdate)
    public: DECLARE_SIGNAL(OnRequestSetSyncData, const std::string, const std::string)
    public: DECLARE_SIGNAL(OnRequestOpenLogFolder)
    };

private:
    GroupBox m_topBorder;
    GroupBox m_centerBorder;
    //GroupBox m_bottomBorder;
    //PrettyButton m_connectBtn;
    PrettyButton m_openDirBtn;
    PrettyButton m_maxLogBtn;
    TextLabel m_connectDescText;
    TextLabel m_openDirText;
    TextLabel m_maxLogText;
    TextInput m_maxLogInput;
    TextLabel m_autologPathText;
    TextInput m_autologPathInput;
    PrettyButton m_autologPathBtn;
    TextInput m_gamePathText;
    std::shared_ptr<PathManagerEx> m_targPath;

    TextInput m_paramInput;
    TextLabel m_paramText;
    PrettyButton m_paramBtn;

public:
    explicit MainSettingPage(CWnd *parent, std::shared_ptr<PathManagerEx> targPath = { });
    ~MainSettingPage() override;

private:
    std::unique_ptr<PageCom> MakeCommunication() override;
    void TryConnect();
    void ChangeRunParam();
    void ChangeMaxLogCount();
    void ChangeAutologPath();
    void RequestOpenExplorer();

    void InitialDDX(std::function<void(int, CWnd&)> &&dataExchanger) override;
    void OnInitialUpdate() override;
    void OnEnterScreen() override;
    void UpdateMaxLogCount(const std::string &limit);
    void UpdateAutologPath(const std::string &path);

public:
    DerivedCom *GetCommunication() const override;

private:
    void ValidAreaSetting(CDC &cdc) override;
};

#endif

