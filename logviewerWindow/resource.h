//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// logviewerWindow.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CONTROL_PANEL               101
#define IDD_LOGVIEWERWINDOW_DIALOG      102
#define IDD_INFO_PANEL                  103
#define IDD_LOG_PANEL                   104
#define IDD_SETTING_PANEL               105
#define IDD_MENU_TITLE_PANEL            106
#define IDR_MAINFRAME                   128
#define IDD_NOTIFY_DLG                  138
#define IDD_SETSDL_PANEL                139
#define IDD_LAUNCHER_MAIN               140
#define IDB_PNG1                        142
#define IDB_PNG2                        143
#define IDB_BITMAP1                     144
#define IDB_BITMAP2                     146
#define IDB_BITMAP3                     148
#define IDD_DIALOG1                     149
#define IDB_BITMAP4                     153
#define MAINWND_MAINPANEL               1000
#define MAINWND_CONTROL_PANEL           1001
#define MAINWND_INFO_PANEL              1002
#define MAINWND_TITLE_PANEL             1003
#define CONTROL_SHOWLOG                 1004
#define CONTROL_SHOWSETTING             1005
#define INFO_STATUS_DESC                1005
#define INFO_STATUS                     1006
#define CONTROL_SDLSET                  1006
#define LOGPAGE_LIST                    1007
#define CONTROL_LAUNCHER                1007
#define LOGPAGE_INPUT                   1008
#define LOGPAGE_BTN_CHAT                1009
#define SETPAGE_TOP                     1010
#define LOGPAGE_BTN_CMD                 1010
#define SETPAGE_CENTER                  1011
#define SETPAGE_BOTTOM                  1012
#define SETPAGE_BTN_CONNECT             1013
#define SETPAGE_CONNECT_TEXT            1014
#define MENUTITLE_TEXT                  1015
#define SETPAGE_OPENDIR_TEXT            1015
#define NOTIFY_DLG_CLOSE                1016
#define SETPAGE_BTN_OPENDIR             1016
#define NOTIFY_DLG_TOP_BORDER           1017
#define SETPAGE_AUTOLOG_MAX_TEXT        1017
#define NOTIFY_DLG_BOTTOM_BORDER        1018
#define SETPAGE_BTN_OPENDIR2            1018
#define SETPAGE_AUTOLOGMAX_BTN          1018
#define NOTIFY_DLG_TEXT                 1019
#define SETPAGE_AUTOLOG_PATH_TEXT       1019
#define NOTIFY_DLG_TITLE_TEXT           1020
#define SETPAGE_AUTOLOGPATH_BTN         1020
#define SETPAGE_AUTOLOGMAX_INPUT        1021
#define SETPAGE_PARAM_TEXT              1022
#define SETPAGE_AUTOLOGPATH_INPUT       1023
#define NOTIFY_DLG_COPYTEXT_BTN         1023
#define NOTIFY_DLG_COPYTEXT_TEXT        1024
#define SETPAGE_PARAM_INPUT             1024
#define INFO_ICON_BTN                   1025
#define SETPAGE_PARAM_BTN               1025
#define IDC_CHECK1                      1026
#define IDC_CHECK2                      1027
#define IDC_CHECK3                      1028
#define IDC_CHECK4                      1029
#define IDC_CHECK5                      1030
#define IDC_CHECK6                      1031
#define IDC_CHECK7                      1032
#define IDC_CHECK8                      1033
#define IDC_CHECK9                      1034
#define IDC_CHECK10                     1035
#define IDC_SLIDER1                     1036
#define IDC_COMBO1                      1037
#define IDC_COMBO2                      1038
#define IDC_BUTTON1                     1038
#define IDC_STATIC_SCALE_QUAL           1039
#define IDC_STATIC_RENDERER             1040
#define IDC_STATIC_SENSITIVITY          1041
#define IDC_STATIC_GROUP_SETSDL         1042
#define IDC_EDIT1                       1043
#define IDC_STATIC_GAMMA                1043
#define IDC_SLIDER2                     1044
#define MINIWND_TEXT                    1046
#define IDC_BUTTON2                     1047

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        154
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1048
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
