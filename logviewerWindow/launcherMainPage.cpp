
#include "stdafx.h"
#include "launcherMainPage.h"
#include "resource.h"

IMPLEMENT_DYNAMIC(LauncherMainPage, CTabPage)

#define BASE_BACKGROUND_COLOR RGB(1,1,6)

LauncherMainPage::LauncherMainPage(CWnd *parent)
    : CTabPage(IDD_LAUNCHER_MAIN, parent)
{
    m_staticPtr = nullptr;
}

LauncherMainPage::~LauncherMainPage()
{ }

std::unique_ptr<CTabPage::PageCom> LauncherMainPage::MakeCommunication()
{
    m_staticPtr = new DerivedCom();
    return std::unique_ptr<CTabPage::PageCom>(m_staticPtr);
}

void LauncherMainPage::OnInitialUpdate()
{
    CTabPage::OnInitialUpdate();

    //m_startBtn.ModifyWndName("S.T.A.R.T.");
    m_startBtn.FetchImage({ }, IDB_BITMAP4);
    m_startBtn.SetCallback([this]() { this->onStartButtonTriggered(); });
    m_webBtn.FetchImage("launcher.bmp", IDB_BITMAP3);
}

void LauncherMainPage::InitialDDX(std::function<void(int, CWnd&)> &&exchanger)
{
    exchanger(IDC_BUTTON1, m_startBtn);
    exchanger(IDC_BUTTON2, m_webBtn);
}

void LauncherMainPage::OnEnterScreen()
{ }

void LauncherMainPage::onStartButtonTriggered()
{
    m_OnStartGame.Emit();
}

void LauncherMainPage::ValidAreaSetting(CDC &cdc)
{
    SetValidArea(cdc, m_startBtn);
    SetValidArea(cdc, m_webBtn);
}

void LauncherMainPage::DrawStuff(CDC &cdc)
{
    CPen pen(PS_SOLID, 4, RGB(3, 162, 232));
    CBrush brush(BASE_BACKGROUND_COLOR);
    CPen *oldPen = cdc.SelectObject(&pen);
    CBrush *oldBrush = cdc.SelectObject(&brush);
    CRect border;

    cdc.GetClipBox(&border);
    cdc.Rectangle(&border);

    cdc.SelectObject(oldPen);
    cdc.SelectObject(oldBrush);
}
