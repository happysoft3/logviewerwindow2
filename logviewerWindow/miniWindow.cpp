
#include "stdafx.h"
#include "miniWindow.h"
#include "resource.h"

MiniWindow::MiniWindow()
    : CDialogEx()
{
    m_defaultBrush = std::make_unique<CBrush>(RGB(255, 255, 233));
    m_highlightBrush = std::make_unique<CBrush>(RGB(153, 217, 234));
    m_pressed = false;
}

MiniWindow::~MiniWindow()
{ }

BOOL MiniWindow::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    m_miniWndTxt.SetLabelText("NOX Launcher mini window. Double click me!");
    m_miniWndTxt.SetTextColor(RGB(64, 128, 0));
    return TRUE;
}

void MiniWindow::DoDataExchange(CDataExchange *pDX)
{
    CDialogEx::DoDataExchange(pDX);

    DDX_Control(pDX, MINIWND_TEXT, m_miniWndTxt);
}

BEGIN_MESSAGE_MAP(MiniWindow, CDialogEx)
    ON_WM_LBUTTONDBLCLK()
    ON_WM_SHOWWINDOW()
    ON_WM_LBUTTONDOWN()
    ON_WM_LBUTTONUP()
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

BOOL MiniWindow::Create(UINT nIDTemplate, CWnd *pParentWnd)
{
    nIDTemplate = IDD_DIALOG1;
    return CDialogEx::Create(nIDTemplate, pParentWnd);    
}

void MiniWindow::alignWindow()
{
    int xpos = ::GetSystemMetrics(SM_CXSCREEN), ypos = ::GetSystemMetrics(SM_CYSCREEN);
    CRect area;

    GetWindowRect(area);
    int cx = area.Width(), cy = area.Height() + 50;

    ::SetWindowPos(m_hWnd, HWND_BOTTOM, xpos - cx, ypos - cy, cx, cy, SWP_NOSIZE);
}

void MiniWindow::OnLButtonDblClk(UINT, CPoint)
{
    CWnd *parent = GetParent();

    if (parent)
        parent->ShowWindow(SW_SHOW);
    ShowWindow(SW_HIDE);
}

void MiniWindow::OnShowWindow(BOOL bShow, UINT nStatus)
{
    if (bShow)
    {
        alignWindow();
        AnimateWindow(1000, AW_BLEND);
        return;
    }
    CWnd *parent = GetParent();

    AnimateWindow(1000, AW_BLEND | AW_HIDE);
    if (parent)
    {
        parent->ShowWindow(SW_SHOWNORMAL);
        parent->SetFocus();
    }
}

void MiniWindow::OnLButtonDown(UINT nFlags, CPoint point)
{
    CDialogEx::OnLButtonDown(nFlags, point);

    m_pressed = true;
    Invalidate();
}

void MiniWindow::OnLButtonUp(UINT nFlags, CPoint point)
{
    CDialogEx::OnLButtonUp(nFlags, point);

    m_pressed = false;
    Invalidate();
}

HBRUSH MiniWindow::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor)
{
    auto hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

    if (pWnd == this)
        return m_pressed ? *m_highlightBrush : *m_defaultBrush;

    return hbr;
}

