
#include "stdafx.h"
#include "sdlOptionPage.h"
#include "logviewerCore/sdl/cfgCollector.h"
#include "logviewerCore/pathManagerEx.h"
#include "common/utils/stringhelper.h"
#include "resource.h"

using namespace _StringHelper;
/*
#define SDL_PARAM_SDL_ENABLED = 1
#define SDL_PARAM_HIDE_CONFIG = 0
#define SDL_PARAM_SDL_KEEPASPECTRATIO = 0
#define SDL_PARAM_SDL_DRIVER = 0
#define SDL_PARAM_SDL_MOVIES = 0
#define SDL_PARAM_SDL_FULLSCREEN = 1
#define SDL_PARAM_SDL_GAMMA = 100
#define SDL_PARAM_NOX_SLEEP = 1
#define SDL_PARAM_SDL_QUALITY = 2
#define SDL_PARAM_NOX_LOCK_TEXTURES = 1
#define SDL_PARAM_SERVER_ENABLED = 0
#define SDL_PARAM_SERVER_NOVIDEO = 0
#define SDL_PARAM_CRASHRPT_ENABLED = 1
#define SDL_PARAM_MULTIPLE_INSTANCES = 0
#define SDL_PARAM_MOUSE_SENSITIVITY = 100
*/
#define DECLARE_SDL_PARAM(param_key) \
    static const char *s_##param_key = #param_key; \

DECLARE_SDL_PARAM(SDL_ENABLED)
DECLARE_SDL_PARAM(HIDE_CONFIG)
DECLARE_SDL_PARAM(SDL_KEEPASPECTRATIO)
DECLARE_SDL_PARAM(SDL_DRIVER)
DECLARE_SDL_PARAM(SDL_MOVIES)
DECLARE_SDL_PARAM(SDL_FULLSCREEN)
DECLARE_SDL_PARAM(SDL_GAMMA)
DECLARE_SDL_PARAM(NOX_SLEEP)
DECLARE_SDL_PARAM(SDL_QUALITY)
DECLARE_SDL_PARAM(NOX_LOCK_TEXTURES)
DECLARE_SDL_PARAM(SERVER_ENABLED)
DECLARE_SDL_PARAM(SERVER_NOVIDEO)
DECLARE_SDL_PARAM(CRASHRPT_ENABLED)
DECLARE_SDL_PARAM(MULTIPLE_INSTANCES)
DECLARE_SDL_PARAM(MOUSE_SENSITIVITY)

IMPLEMENT_DYNAMIC(SdlOptionPage, CTabPage)

SdlOptionPage::SdlOptionPage(CWnd *parent, std::shared_ptr<PathManagerEx> targPath)
    : CTabPage(IDD_SETSDL_PANEL, parent)
{
    m_targPath = targPath;
}

SdlOptionPage::~SdlOptionPage()
{ }

void SdlOptionPage::initCheckButton(CheckButton &check, const std::string &enabled, const std::string &disabled)
{
    check.SetConditionalText(enabled, disabled);
    check.SetConditionalTextColor(RGB(255, 242, 0), RGB(153, 217, 234));
    check.SetConditionalBkColor(RGB(248, 53, 114), RGB(112, 146, 190));
    check.SetFont(&m_checkFont);
}

void SdlOptionPage::initializeCheckFont()
{
    m_checkFont.CreateFontA(18, 10, 0, 0, FW_BOLD, FALSE, FALSE, 0,
        DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, "Arial");
}

void SdlOptionPage::OnInitialUpdate()
{
    CTabPage::OnInitialUpdate();

    initializeCheckFont();
    m_slider.SetRange(0, 180);
    m_gammaSlider.SetRange(0, 180);
    
    initCheckButton(m_checkFullScreen,"FULLSCREEN-ON", "FULLSCREEN-OFF");
    initCheckButton(m_checkShowMovies,"SHOW_MOVIE-ON", "SHOW_MOVIE-OFF");
    initCheckButton(m_checkKeepAspectRatio, "KeepAspectRatio", "Stretched");
    initCheckButton(m_checkUseCrashRpt,"CrashReport-ON", "CrashReport-OFF");
    initCheckButton(m_checkReduceCpuUsage,"Sleep-ON", "Sleep-OFF");
    initCheckButton(m_checkLockTexture,"LockTexture-ON", "LockTexture-OFF");
    initCheckButton(m_checkMultiInstance,"MultipleInstance-ON", "MultipleInstance-OFF");
    initCheckButton(m_checkServerModeOn, "ServerMode-ON", "ServerMode-OFF");
    initCheckButton(m_checkServerModeVideoOn, "ServerMode-Video-ON", "ServerMode-Video-OFF");
    m_txtRenderer.SetLabelText("Renderer");
    m_txtScaleQual.SetLabelText("Scale Quality");
    m_txtSensitivity.SetLabelText("Sensitivity");
    m_txtGamma.SetLabelText("Gamma");

    m_qualList.ChangeHeight(120);
    m_rendererList.ChangeHeight(120);
    m_qualList.UpdateItemList({ "Low", "Normal", "High" });
    m_rendererList.UpdateItemList({ "DirectX", "OpenGL", "Software" });

    m_checkSpare3.ShowWindow(SW_HIDE);
    m_checkSpare3.EnableWindow(false);
    m_checkMultiInstance.ShowWindow(SW_HIDE);
    m_checkMultiInstance.EnableWindow(false);
}

void SdlOptionPage::InitialDDX(std::function<void(int, CWnd&)> &&exchanger)
{
    exchanger(IDC_STATIC_GROUP_SETSDL, m_border);
    exchanger(IDC_SLIDER1, m_slider);
    exchanger(IDC_SLIDER2, m_gammaSlider);
    exchanger(IDC_STATIC_GAMMA, m_txtGamma);
    exchanger(IDC_CHECK1, m_checkFullScreen);
    exchanger(IDC_CHECK2, m_checkShowMovies);
    exchanger(IDC_CHECK3, m_checkKeepAspectRatio);
    exchanger(IDC_CHECK4, m_checkUseCrashRpt);
    exchanger(IDC_CHECK5, m_checkReduceCpuUsage);
    exchanger(IDC_CHECK6, m_checkLockTexture);
    exchanger(IDC_CHECK7, m_checkMultiInstance);
    exchanger(IDC_STATIC_SCALE_QUAL, m_txtScaleQual);
    exchanger(IDC_STATIC_RENDERER, m_txtRenderer);
    exchanger(IDC_STATIC_SENSITIVITY, m_txtSensitivity);
    exchanger(IDC_COMBO1, m_qualList);
    exchanger(IDC_COMBO2, m_rendererList);

    exchanger(IDC_CHECK8, m_checkServerModeOn);
    exchanger(IDC_CHECK9, m_checkServerModeVideoOn);
    exchanger(IDC_CHECK10, m_checkSpare3);
}

void SdlOptionPage::updateMouseSensitivity(CfgCollector &cfg)
{
    int value;

    if (!cfg.GetValueAsNumber(s_MOUSE_SENSITIVITY, value))
        return;

    m_slider.ChangePos(value);
    if (!cfg.GetValueAsNumber(s_SDL_GAMMA, value))
        return;
    m_gammaSlider.ChangePos(value);
}

void SdlOptionPage::updateLists(CfgCollector &cfg)
{
    int qual;

    if (!cfg.GetValueAsNumber(s_SDL_QUALITY, qual))
        return;

    int renderer = 0;

    cfg.GetValueAsNumber(s_SDL_DRIVER, renderer);
    m_qualList.SetCurSel(qual);
    m_rendererList.SetCurSel(renderer);
}

void SdlOptionPage::updateChecks(CfgCollector &cfg)
{
    int value;

    if (!cfg.GetValueAsNumber(s_SDL_FULLSCREEN, value))
        return;
    m_checkFullScreen.SetCheckStatus(value ? true : false);
    cfg.GetValueAsNumber(s_SDL_MOVIES, value);
    m_checkShowMovies.SetCheckStatus(value ? true : false);
    cfg.GetValueAsNumber(s_SDL_KEEPASPECTRATIO, value);
    m_checkKeepAspectRatio.SetCheckStatus(value ? true : false);
    cfg.GetValueAsNumber(s_CRASHRPT_ENABLED, value);
    m_checkUseCrashRpt.SetCheckStatus(value ? true : false);
    cfg.GetValueAsNumber(s_NOX_LOCK_TEXTURES, value);
    m_checkLockTexture.SetCheckStatus(value ? true : false);
    cfg.GetValueAsNumber(s_NOX_SLEEP, value);
    m_checkReduceCpuUsage.SetCheckStatus(value ? true : false);
    cfg.GetValueAsNumber(s_MULTIPLE_INSTANCES, value);
    m_checkMultiInstance.SetCheckStatus(value ? true : false);
    cfg.GetValueAsNumber(s_SERVER_ENABLED, value);
    m_checkServerModeOn.SetCheckStatus(value ? true : false);
    cfg.GetValueAsNumber(s_SERVER_NOVIDEO, value);
    m_checkServerModeVideoOn.SetCheckStatus(value ? true : false);
}

void SdlOptionPage::loadCfgDefaultData(CfgCollector &cfg)
{
    cfg.SetValueAsNumber(s_SDL_ENABLED, 1);
    cfg.SetValueAsNumber(s_HIDE_CONFIG, 1);
    cfg.SetValueAsNumber(s_SDL_KEEPASPECTRATIO, 0);
    cfg.SetValueAsNumber(s_SDL_DRIVER, 0);
    cfg.SetValueAsNumber(s_SDL_MOVIES, 0);
    cfg.SetValueAsNumber(s_SDL_FULLSCREEN, 1);
    cfg.SetValueAsNumber(s_SDL_GAMMA, 100);
    cfg.SetValueAsNumber(s_NOX_SLEEP, 1);
    cfg.SetValueAsNumber(s_SDL_QUALITY, 2);
    cfg.SetValueAsNumber(s_NOX_LOCK_TEXTURES, 1);
    cfg.SetValueAsNumber(s_SERVER_ENABLED, 0);
    cfg.SetValueAsNumber(s_SERVER_NOVIDEO, 0);
    cfg.SetValueAsNumber(s_CRASHRPT_ENABLED, 1);
    cfg.SetValueAsNumber(s_MULTIPLE_INSTANCES, 1);
    cfg.SetValueAsNumber(s_MOUSE_SENSITIVITY, 100);
}

void SdlOptionPage::updateParams(CfgCollector &cfg)
{
    updateChecks(cfg);
    updateMouseSensitivity(cfg);
    updateLists(cfg);
}

void SdlOptionPage::OnEnterScreen()
{
    std::string s = stringFormat("%s/noxgui.cfg", m_targPath ? m_targPath->BasePath() : "nil");
    auto cfg = std::make_unique<CfgCollector>();

    if (!cfg->ReadCfg(s)) {
        loadCfgDefaultData(*cfg);
        cfg->WriteCfg(s);
    }
    updateParams(*cfg);
    m_cfgSettings = std::move(cfg);
}

void SdlOptionPage::storeLists(CfgCollector &cfg)
{
    int selRender = m_rendererList.GetCurSel();
    switch (selRender)
    {
    case 0: case 1: case 2:
        cfg.SetValue(s_SDL_DRIVER, std::to_string(selRender));
    }
    int selQual = m_qualList.GetCurSel();
    switch (selQual)
    {
    case 0: case 1: case 2:
        cfg.SetValue(s_SDL_QUALITY, std::to_string(selQual));
    }
}

void SdlOptionPage::storeChecks(CfgCollector &cfg)
{
    static constexpr char *boolToString[] = { "0", "1" };

    cfg.SetValue(s_NOX_LOCK_TEXTURES, boolToString[m_checkLockTexture.GetCheckStatus()]);
    cfg.SetValue(s_NOX_SLEEP, boolToString[m_checkReduceCpuUsage.GetCheckStatus()]);
    cfg.SetValue(s_CRASHRPT_ENABLED, boolToString[m_checkUseCrashRpt.GetCheckStatus()]);
    cfg.SetValue(s_MULTIPLE_INSTANCES, boolToString[m_checkMultiInstance.GetCheckStatus()]);
    cfg.SetValue(s_SDL_FULLSCREEN, boolToString[m_checkFullScreen.GetCheckStatus()]);
    cfg.SetValue(s_SDL_MOVIES, boolToString[m_checkShowMovies.GetCheckStatus()]);
    cfg.SetValue(s_SDL_KEEPASPECTRATIO, boolToString[m_checkKeepAspectRatio.GetCheckStatus()]);
    cfg.SetValue(s_SERVER_ENABLED, boolToString[m_checkServerModeOn.GetCheckStatus()]);
    cfg.SetValue(s_SERVER_NOVIDEO, boolToString[m_checkServerModeVideoOn.GetCheckStatus()]);
}

void SdlOptionPage::storeMouseSensitivity(CfgCollector &cfg)
{
    int value = m_slider.GetPos();

    cfg.SetValue(s_MOUSE_SENSITIVITY, std::to_string(value));
    value = m_gammaSlider.GetPos();
    cfg.SetValue(s_SDL_GAMMA, std::to_string(value));
}

void SdlOptionPage::OnExitScreen()
{
    if (m_cfgSettings)
    {
        storeChecks(*m_cfgSettings);
        storeLists(*m_cfgSettings);
        storeMouseSensitivity(*m_cfgSettings);
        m_cfgSettings->WriteCfg();
        m_cfgSettings.reset();
    }
}
