
#include "stdafx.h"
#include "infoPage.h"

#include "resource.h"

IMPLEMENT_DYNAMIC(InfoPage, CTabPage)

void InfoPage::DerivedCom::SlotConnectStatChanged(bool state)
{
    auto wndCore = m_wndCore.expired() ? nullptr : m_wndCore.lock();

    if (!wndCore)
        return;

    InfoPage *infoPage = dynamic_cast<InfoPage *>(wndCore->m_parentWnd);

    if (infoPage == nullptr)
        return;

    infoPage->ChangeStatusText(state);
}

InfoPage::InfoPage(UINT nIdTemplate, CWnd *parent)
    : CTabPage(nIdTemplate, parent)
{ }

InfoPage::~InfoPage()
{ }

void InfoPage::OnInitialUpdate()
{
    CTabPage::OnInitialUpdate();

    m_infoTitleText.SetWindowTextA("NOX STATUS");
    m_infoTitleText.SetTextColor(RGB(255, 127, 39));
    ChangeStatusText(false);
}

std::unique_ptr<CTabPage::PageCom> InfoPage::MakeCommunication()
{
    return std::make_unique<DerivedCom>();
}

void InfoPage::InitialDDX(std::function<void(int, CWnd&)> &&dataExchanger)
{
    dataExchanger(INFO_STATUS_DESC, m_infoTitleText);
    dataExchanger(INFO_STATUS, m_infoStatusText);
    dataExchanger(INFO_ICON_BTN, m_infoIconBtn);    
}

void InfoPage::ChangeStatusText(bool state)
{
    m_infoStatusText.SetTextColor(state ? RGB(34, 177, 76) : RGB(232, 23, 23));
    m_infoStatusText.SetWindowText(state ? "ON" : "OFF");
    m_infoIconBtn.UpdateImage(state ? IDB_PNG2 : IDB_PNG1);
    m_infoIconBtn.Invalidate();
}

InfoPage::DerivedCom *InfoPage::GetCommunication() const
{
    return (m_com == nullptr) ? nullptr : dynamic_cast<DerivedCom *>(m_com.get());
}

//void InfoPage::ValidAreaSetting(CDC &cdc)
//{
//    SetValidArea(cdc, m_infoIconBtn);
//}
