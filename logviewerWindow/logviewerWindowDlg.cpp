
// logviewerWindowDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "logviewerWindow.h"
#include "logviewerWindowDlg.h"
#include "afxdialogex.h"

#include "pageManager.h"
#include "controlPage.h"
#include "infoPage.h"
#include "mainSettingPage.h"
#include "mainLogPage.h"
#include "sdlOptionPage.h"
#include "launcherMainPage.h"
#include "menuTitlePage.h"
#include "miniWindow.h"
#include "coreUi.h"

#include "notifyDialog.h"

#include "cbufferdc.h"

#include "common/utils/stringhelper.h"

using namespace _StringHelper;


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#pragma comment(lib, "common.lib")
#pragma comment(lib, "logviewerCore.lib")

static constexpr UINT s_mainWindowBkColor = RGB(8, 133, 163);


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

class ClogviewerWindowDlg::MainWndCC : public CCObject
{
private:
    ClogviewerWindowDlg *m_mainWnd;

public:
    MainWndCC(ClogviewerWindowDlg *mainWnd)
        : CCObject(), m_mainWnd(mainWnd)
    { }

    ~MainWndCC() override
    { }

    void SlotNotifyScreenChanged(const std::string &screenId)
    {
        m_mainWnd->MainScreenSwitching(screenId);
    }

    void SlotChangedProcessStatus(bool state)
    {
        m_mainWnd->PostMessageA(c_mainWndUserMessage, state ? c_userMessageStartProcess : c_userMessageEndProcess);
    }

    void SlotPopup(const std::string &msg)
    {
        m_mainWnd->showPopupMessage(msg);
    }
};


// ClogviewerWindowDlg 대화 상자



ClogviewerWindowDlg::ClogviewerWindowDlg(CWnd* pParent /*=NULL*/)
    : CDialogEx(IDD_LOGVIEWERWINDOW_DIALOG, pParent),
    m_wndColor(new CBrush(s_mainWindowBkColor))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    m_wndcc = std::make_unique<MainWndCC>(this);
    m_coreui = std::make_unique<CoreUi>();
    m_titleEdgeWidth = GetSystemMetrics(SM_CYEDGE);
    m_titleHeight = GetSystemMetrics(SM_CYCAPTION) + m_titleEdgeWidth;
}

ClogviewerWindowDlg::~ClogviewerWindowDlg()
{ }

void ClogviewerWindowDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

void ClogviewerWindowDlg::InitPageManager()
{
    CWnd *controlPanelFrame = GetDlgItem(MAINWND_CONTROL_PANEL);

    controlPanelFrame->ShowWindow(SW_HIDE);
    m_controlPageMan = std::make_unique<PageManager>(*controlPanelFrame, this);

    std::unique_ptr<ControlPage> controlPage(new ControlPage(IDD_CONTROL_PANEL, this));

    m_controlPageMan->MakePage("controlPage", std::move(controlPage));
    m_controlPageMan->ShowPage("controlPage");

    CWnd *infoPanelFrame = GetDlgItem(MAINWND_INFO_PANEL);

    infoPanelFrame->ShowWindow(SW_HIDE);
    m_infoPageMan = std::make_unique<PageManager>(*infoPanelFrame, this);

    std::unique_ptr<InfoPage> infoPage(new InfoPage(IDD_INFO_PANEL, this));

    m_infoPageMan->MakePage("infoPage", std::move(infoPage));
    m_infoPageMan->ShowPage("infoPage");

    CWnd *titleFrame = GetDlgItem(MAINWND_TITLE_PANEL);

    titleFrame->ShowWindow(SW_HIDE);
    m_titlePageMan = std::make_unique<PageManager>(*titleFrame, this);

    std::unique_ptr<MenuTitlePage> titlePage(new MenuTitlePage(IDD_MENU_TITLE_PANEL, this));

    titlePage->AppendPageTitle("settingPage", "_APP_SETTINGS___");
    titlePage->AppendPageTitle("logListPage", "_LIVE-LOG_MESSAGE___");
    titlePage->AppendPageTitle("sdlOptionPage", "_LAUNCHER_OPTIONS___");
    titlePage->AppendPageTitle("launcherPage", stringFormat("_NOX_LAUNCHER v%s", m_appVer));

    m_titlePageMan->MakePage("title", std::move(titlePage));
    m_titlePageMan->ShowPage("title");

    //Create MainPages
    CWnd *mainPanelFrame = GetDlgItem(MAINWND_MAINPANEL);

    mainPanelFrame->ShowWindow(SW_HIDE);
    m_mainPageMan = std::make_unique<PageManager>(*mainPanelFrame, this);

    m_mainPageMan->MakePage("settingPage", std::make_unique<MainSettingPage>(this, m_coreui->GetSharedPath()));
    m_mainPageMan->MakePage("logListPage", std::make_unique<MainLogPage>(this));
    m_mainPageMan->MakePage("sdlOptionPage", std::make_unique<SdlOptionPage>(this, m_coreui->GetSharedPath() ));
    m_mainPageMan->MakePage("launcherPage", std::make_unique<LauncherMainPage>(this));
    m_mainPageMan->ShowPage("launcherPage");
}

void ClogviewerWindowDlg::InitScreenInstance()
{
    ControlPage *controlPage = nullptr;
    MenuTitlePage *titlePage = nullptr;

    m_controlPageMan->GetPageInstance("controlPage", controlPage);
    m_titlePageMan->GetPageInstance("title", titlePage);

    controlPage->GetCommunication()->OnChangeScreen().Connection(&ClogviewerWindowDlg::MainWndCC::SlotNotifyScreenChanged, m_wndcc.get());
    controlPage->GetCommunication()->OnChangeScreen().Connection(&MenuTitlePage::DerivedCom::SlotScreenChanged, titlePage->GetCommunication());

    std::string currentMainPage = m_mainPageMan->CurrentPageName();

    controlPage->GetCommunication()->OnChangeScreen().Emit(currentMainPage);

    InfoPage *infoPage = nullptr;

    m_infoPageMan->GetPageInstance("infoPage", infoPage);
    m_coreui->OnConnectionChanged().Connection(&InfoPage::DerivedCom::SlotConnectStatChanged, infoPage->GetCommunication());

    MainSettingPage *settingPage = nullptr;

    m_mainPageMan->GetPageInstance("settingPage", settingPage);
    settingPage->GetCommunication()->OnSettingPageUpdate().Connection(&CoreUi::SlotUpdateSettingPage, m_coreui.get());
    m_coreui->OnSettingPageSyncData().Connection(&MainSettingPage::DerivedCom::SlotGetSyncData, settingPage->GetCommunication());
    settingPage->GetCommunication()->OnRequestSetSyncData().Connection(&CoreUi::SlotAutologSyncDataChanged, m_coreui.get());
    settingPage->GetCommunication()->OnRequestOpenLogFolder().Connection(&CoreUi::SlotOpenAutoLogFolderBrowser, m_coreui.get());

    MainLogPage *logPage = nullptr;

    m_mainPageMan->GetPageInstance("logListPage", logPage);
    m_coreui->OnReleaseListItem().Connection(&MainLogPage::DerivedCom::SlotReceiveListItem, logPage->GetCommunication());
    logPage->GetCommunication()->OnClickButton().Connection(&CoreUi::SlotSendOutputTask, m_coreui.get());

    LauncherMainPage *launcherPage = nullptr;

    if (m_mainPageMan->GetPageInstance("launcherPage", launcherPage))
    {
        launcherPage->OnStartGame().Connection(&CoreUi::SlotStartgame, m_coreui.get());
    }
}

void ClogviewerWindowDlg::LastSaveAutolog()
{
    if (m_coreui)
        m_coreui->SaveAutolog();
}

void ClogviewerWindowDlg::DeInitPageManager()
{
    m_mainPageMan->DestroyAll();
    m_mainPageMan.reset();

    m_controlPageMan->DestroyAll();
    m_controlPageMan.reset();

    m_infoPageMan->DestroyAll();
    m_infoPageMan.reset();

    m_titlePageMan->DestroyAll();
    m_titlePageMan.reset();
}

void ClogviewerWindowDlg::InitialMessageBoxCreate()
{
    m_msgbox = std::shared_ptr<NotifyDialog>(new NotifyDialog(this), [](NotifyDialog *msgbox) { msgbox->DestroyWindow(); delete msgbox; });
    m_msgbox->InitialCreate();
    m_msgbox->ToggleCopyMsgFeature(true);

    SetWindowText("NOX MINI LAUNCHER   --happysoft ltd");
}

void ClogviewerWindowDlg::InitialLoadBackground()
{
    m_bkBmp.LoadBitmapA(IDB_BITMAP1);
    BITMAP bm;
    m_bkBmp.GetBitmap(&bm);
    m_bkbmpSize = CSize(bm.bmWidth, bm.bmHeight);
}

bool ClogviewerWindowDlg::InitMainWindow()
{
    try
    {
        InitialLoadBackground();
        InitialMessageBoxCreate();
        InitPageManager();
        InitScreenInstance();
        m_miniWnd = std::make_unique<MiniWindow>();
        m_miniWnd->Create((UINT)0, this);
        m_miniWnd->ShowWindow(SW_HIDE);
        m_coreui->InitCoreUi();
    }
    catch (const std::exception &e)
    {
        ::AfxMessageBox(e.what());
        return false;
    }
    m_coreui->OnConnectionChanged().Connection(&MainWndCC::SlotChangedProcessStatus, m_wndcc.get());
    m_coreui->OnReleaseMessage().Connection(&MainWndCC::SlotPopup, m_wndcc.get());
    return true;
}

void ClogviewerWindowDlg::MainScreenSwitching(const std::string &screenId)
{
    if (!m_mainPageMan)
        return;

    m_mainPageMan->ShowPage(screenId);
}

void ClogviewerWindowDlg::DrawMainWindow(CDC *pDC)
{
    CRect pageArea;
    if (m_mainPageMan)
    {
        if (m_mainPageMan->GetPageRect(pageArea, this))
            pDC->ExcludeClipRect(pageArea);
    }
    if (m_controlPageMan)
    {
        if (m_controlPageMan->GetPageRect(pageArea, this))
            pDC->ExcludeClipRect(pageArea);
    }
    if (m_infoPageMan)
    {
        if (m_infoPageMan->GetPageRect(pageArea, this))
            pDC->ExcludeClipRect(pageArea);
    }
    if (m_titlePageMan)
    {
        if (m_titlePageMan->GetPageRect(pageArea, this))
            pDC->ExcludeClipRect(pageArea);
    }
    CRect clipArea;
    CDC memDC;

    memDC.CreateCompatibleDC(pDC);
    CBitmap *pOldbitmap = memDC.SelectObject(&m_bkBmp);
    pDC->BitBlt(0, 0, m_bkbmpSize.cx, m_bkbmpSize.cy, &memDC, 0, 0, SRCCOPY);
    memDC.SelectObject(pOldbitmap);
    memDC.DeleteDC();
}

void ClogviewerWindowDlg::showPopupMessage(const std::string &msg)
{
    m_msgbox->Emerge(msg);
}

BEGIN_MESSAGE_MAP(ClogviewerWindowDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
    ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
    ON_WM_DESTROY()
    ON_WM_CLOSE()
    ON_WM_CTLCOLOR()
    ON_WM_SIZE()
    ON_MESSAGE(c_mainWndUserMessage, OnUserMessageReceived)
END_MESSAGE_MAP()


// ClogviewerWindowDlg 메시지 처리기
    
BOOL ClogviewerWindowDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

    if (!InitMainWindow())
        PostMessage(WM_CLOSE);
	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void ClogviewerWindowDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

BOOL ClogviewerWindowDlg::OnEraseBkgnd(CDC *pDC)
{
    pDC->SaveDC();
    DrawMainWindow(pDC);
    pDC->RestoreDC(-1);
    return FALSE;
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void ClogviewerWindowDlg::OnPaint()
{
    Default();
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR ClogviewerWindowDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void ClogviewerWindowDlg::OnDestroy()
{
    CDialogEx::OnDestroy();

    m_bkBmp.DeleteObject();
}

void ClogviewerWindowDlg::OnClose()
{
    if (m_coreui->GameIsRunning())
    {
        ::AfxMessageBox("You can't close this app while the game still running. Okay?");
        return;
    }
    DeInitPageManager();
    LastSaveAutolog();
    CDialogEx::OnClose();
}

HBRUSH ClogviewerWindowDlg::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor)
{
    auto hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

    if (pWnd == this)
        return *m_wndColor;

    return hbr;
}

BOOL ClogviewerWindowDlg::PreTranslateMessage(MSG *pMsg)
{
    // TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
    if (pMsg->message == WM_KEYDOWN)
    {
        switch (pMsg->wParam)
        {
        case VK_RETURN:
        case VK_ESCAPE:
        case VK_F1:
            return TRUE;
        }
    }
    return CDialogEx::PreTranslateMessage(pMsg);
}

void ClogviewerWindowDlg::OnSize(UINT nType, int cx, int cy)
{
    if (nType == SIZE_MINIMIZED)
    {
        ShowWindow(SW_HIDE);
        m_miniWnd->ShowWindow(SW_SHOW);
    }
}

HRESULT ClogviewerWindowDlg::OnUserMessageReceived(WPARAM wParam, LPARAM lParam)
{
    switch (wParam)
    {
    case c_userMessageEndProcess:
        m_coreui->GetAppResult();
        m_miniWnd->ShowWindow(SW_HIDE);
        break;

    case c_userMessageStartProcess:
        m_coreui->TryAppPatch();
        ShowWindow(SW_MINIMIZE);
        break;
    }
    return 0;
}
