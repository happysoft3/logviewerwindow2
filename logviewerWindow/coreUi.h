
#ifndef CORE_UI_H__
#define CORE_UI_H__

#include "common/ccobject/ccobject.h"

class GameWatcher;
class ConsoleLogItem;
class ListElement;
class SystemFileModifier;
class PathManagerEx;

class CoreUi : public CCObject
{
private:
    std::unique_ptr<GameWatcher> m_game;
    std::unique_ptr<SystemFileModifier> m_sysFile;
    std::shared_ptr<PathManagerEx> m_targPath;

public:
    CoreUi(CCObject *parent = nullptr);
    ~CoreUi() override;

public:
    void InitCoreUi();
    //std::string TryReconnect();

private:
    void SlotDestinateLogList(const std::list<std::weak_ptr<ConsoleLogItem>> &logList);
    void SlotOnDisconnect();
    void slotOnConnected();
    void initialSettingSystemFile();

public:
    void SaveAutolog();
    void SlotSendOutputTask(const std::string &task, const std::string &message);
    void SlotUpdateSettingPage();
    void SlotAutologSyncDataChanged(const std::string &type, const std::string &value);
    void SlotOpenAutoLogFolderBrowser();
    std::shared_ptr<PathManagerEx> GetSharedPath() const
    {
        return m_targPath;
    }
    void SlotStartgame();
    void GetAppResult();
    void TryAppPatch();
    bool GameIsRunning();

public:
    DECLARE_SIGNAL(OnConnectionChanged, bool)
public:
    DECLARE_SIGNAL(OnReleaseListItem, std::list<std::shared_ptr<ListElement>>)
public: DECLARE_SIGNAL(OnSettingPageSyncData, const std::string, const std::string)
public: DECLARE_SIGNAL(OnReleaseMessage, const std::string)
};

#endif

