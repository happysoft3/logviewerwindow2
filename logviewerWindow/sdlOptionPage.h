
#ifndef SDL_OPTION_PAGE_H__
#define SDL_OPTION_PAGE_H__

#include "cTabPage.h"
#include "assets/checkButton.h"
#include "assets/cdropdownlist.h"
#include "assets/prettySlider.h"
#include "textLabel.h"
#include "groupBox.h"

class PathManagerEx;
class CfgCollector;

class SdlOptionPage : public CTabPage
{
    DECLARE_DYNAMIC(SdlOptionPage)
private:
    std::shared_ptr<PathManagerEx> m_targPath;
    std::unique_ptr<CfgCollector> m_cfgSettings;
    PrettySlider m_slider;
    PrettySlider m_gammaSlider;
    CheckButton m_checkFullScreen;
    CheckButton m_checkShowMovies;
    CheckButton m_checkKeepAspectRatio;
    CheckButton m_checkUseCrashRpt;
    CheckButton m_checkReduceCpuUsage;
    CheckButton m_checkLockTexture;
    CheckButton m_checkMultiInstance;

    CheckButton m_checkServerModeOn;
    CheckButton m_checkServerModeVideoOn;
    CheckButton m_checkSpare3;
    TextLabel m_txtScaleQual;
    TextLabel m_txtRenderer;
    TextLabel m_txtSensitivity;
    TextLabel m_txtGamma;
    GroupBox m_border;
    CDropdownList m_qualList;
    CDropdownList m_rendererList;
    CFont m_checkFont;

public:
    explicit SdlOptionPage(CWnd *parent, std::shared_ptr<PathManagerEx> targPath);
    ~SdlOptionPage() override;

private:
    void initCheckButton(CheckButton &check,const std::string &enabled, const std::string &disabled);
    void initializeCheckFont();
    void OnInitialUpdate() override;
    void InitialDDX(std::function<void(int, CWnd&)> &&exchanger) override;
    void updateMouseSensitivity(CfgCollector &cfg);
    void updateLists(CfgCollector &cfg);
    void updateChecks(CfgCollector &cfg);
    void loadCfgDefaultData(CfgCollector &cfg);
    void updateParams(CfgCollector &cfg);
    void OnEnterScreen() override;
    void storeLists(CfgCollector &cfg);
    void storeChecks(CfgCollector &cfg);
    void storeMouseSensitivity(CfgCollector &cfg);
    void OnExitScreen() override;
};

#endif

