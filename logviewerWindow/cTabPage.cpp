#include "stdafx.h"
#include "CTabPage.h"

IMPLEMENT_DYNAMIC(CTabPage, CFormView)

CTabPage::CTabPage(UINT _nIDTemplate, CWnd *parent)
    : CFormView(_nIDTemplate), m_nIDTemplate(_nIDTemplate), m_bkBrush(RGB(106, 182, 196)), m_bkColor(RGB(106, 182, 196))
{
    m_wndCore = std::make_shared<PageWndCore>(this);
    m_parentWnd = parent;
}

CTabPage::~CTabPage()
{
    m_wndCore.reset();
}

std::unique_ptr<CTabPage::PageCom> CTabPage::MakeCommunication()
{
    return std::make_unique<PageCom>();
}

CTabPage::PageCom *CTabPage::GetCommunication() const
{
    return (m_com == nullptr) ? nullptr : m_com.get();
}

void CTabPage::DrawStuff(CDC &cdc)
{
    CPen pen(PS_SOLID, 2, RGB(43, 157, 115));
    CBrush brush(RGB(28, 200, 200));
    CPen *oldPen = cdc.SelectObject(&pen);
    CBrush *oldBrush = cdc.SelectObject(&brush);
    CRect border;

    cdc.GetClipBox(&border);
    cdc.Rectangle(&border);

    cdc.SelectObject(oldPen);
    cdc.SelectObject(oldBrush);
}

void CTabPage::SetValidArea(CDC &cdc, CWnd &wnd)
{
    CRect validRect;

    if (wnd.m_hWnd == nullptr)
        return;

    wnd.GetWindowRect(&validRect);
    ScreenToClient(&validRect);
    cdc.ExcludeClipRect(&validRect);
}

void CTabPage::ChangeBkColor(UINT _color)
{
    m_bkColor = _color;
    m_bkBrush.DeleteObject();
    m_bkBrush.CreateSolidBrush(_color);
}

BOOL CTabPage::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, CCreateContext *pContext)
{
    //Todo something...
    return CFormView::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, m_nIDTemplate, pContext);
}

void CTabPage::OnInitialUpdate()
{
    CFormView::OnInitialUpdate();

    if (!m_com)
    {
        m_com = MakeCommunication();
        m_com->SetParentWnd(m_wndCore);
    }
    //Todo: init this dialog here
}

//@brief. 해당 페이지 입장
void CTabPage::OnEnterScreen()
{ }

//@brief. 해당 페이지로 부터 나감
void CTabPage::OnExitScreen()
{ }

void CTabPage::Show(bool show)
{
    if (show)
        OnEnterScreen();
    else
        OnExitScreen();
    ShowWindow(show ? SW_SHOW : SW_HIDE);
}

bool CTabPage::CreatePage(const CRect &location, CCreateContext *pContext)
{
    bool result = Create(nullptr, nullptr, WS_CHILD | WS_VSCROLL | WS_HSCROLL, location, m_parentWnd, pContext) & true;

    if (result)
        UpdateData(false);

    return result;
}

bool CTabPage::onEraseBackground(CDC *pDC) {
    pDC->SaveDC();

    CDC memDC;

    memDC.CreateCompatibleDC(pDC);
    CRect area;

    GetClientRect(&area);
    CBitmap bitmap;

    bitmap.CreateCompatibleBitmap(pDC, area.right, area.bottom);
    CBitmap *pOldBitmap = memDC.SelectObject(&bitmap);

    memDC.SaveDC();
    ValidAreaSetting(*pDC);
    DrawStuff(memDC);
    pDC->BitBlt(0, 0, area.right, area.bottom, &memDC, 0, 0, SRCCOPY);
    memDC.RestoreDC(-1);
    memDC.SelectObject(pOldBitmap);
    bitmap.DeleteObject();
    memDC.DeleteDC();

    pDC->RestoreDC(-1);
    return true;
}

void CTabPage::DoDataExchange(CDataExchange *pDX)
{
    CFormView::DoDataExchange(pDX);

    InitialDDX([pDX](int nIdc, CWnd &wnd) { DDX_Control(pDX, nIdc, wnd); });
}

HBRUSH CTabPage::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor)
{
    CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

    pDC->SetBkColor(m_bkColor);
    return m_bkBrush;
}

BOOL CTabPage::OnEraseBkgnd(CDC *pDC)
{
    return onEraseBackground(pDC);
}

void CTabPage::OnPaint()
{
    Default();
}

BEGIN_MESSAGE_MAP(CTabPage, CFormView)
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
END_MESSAGE_MAP()
