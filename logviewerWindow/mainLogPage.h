
#ifndef MAIN_LOG_PAGE_H__
#define MAIN_LOG_PAGE_H__

#include "cTabPage.h"
#include "prettyButton.h"
#include "loglistviewer.h"
#include "textinput.h"

class ListElement;
class NotifyDialog;

class MainLogPage : public CTabPage
{
    DECLARE_DYNAMIC(MainLogPage)

    class DerivedCom : public CTabPage::PageCom
    {
        friend MainLogPage;

    public:
        void SlotReceiveListItem(const std::list<std::shared_ptr<ListElement>> &itemlist);
    public:
        DECLARE_SIGNAL(OnClickButton, std::string, std::string)
    };

private:
    PrettyButton m_sendChatBtn;
    PrettyButton m_sendCmdBtn;
    LogListViewer m_logViewer;
    TextInput m_textInput;
    std::unique_ptr<NotifyDialog> m_logMsgbox;

public:
    explicit MainLogPage(CWnd *parent);
    ~MainLogPage() override;

private:
    void OnClickedOutputButton(const std::string &outputType);
    std::unique_ptr<PageCom> MakeCommunication() override;
    void InitialCreateLogMessagebox();
    void OnInitialUpdate() override;
    void InitialDDX(std::function<void(int, CWnd&)> &&exchanger) override;
    void ViewInsert(const std::list<std::shared_ptr<ListElement>> &items);
    void OpenLogMessageBox(int columnId, int rowId);
    void ValidAreaSetting(CDC &cdc) override;
    void DrawStuff(CDC &cdc) override;
    bool onEraseBackground(CDC *pDC) override;

public:
    DerivedCom *GetCommunication() const override;
};

#endif

