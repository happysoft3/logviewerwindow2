#ifndef IMAGE_BUTTON_H__
#define IMAGE_BUTTON_H__

#include <functional>
#include <string>

class ImageButton : public CMFCButton
{
private:
    CString m_imgType;
    UINT m_resourceId;
    CWnd *m_receiver;

    using clickCallbackType = std::function<void(CWnd*)>;
    clickCallbackType m_onClicked;

public:
    explicit ImageButton();
    virtual ~ImageButton() override;

    void UpdateImage(UINT resourceId);
    void SetClickCallback(CWnd *receiver, clickCallbackType method);

private:
    IStream *CreateResourceStream();

protected:
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

    DECLARE_MESSAGE_MAP()
};

#endif
