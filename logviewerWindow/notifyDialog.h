
#ifndef NOTIFY_DIALOG_H__
#define NOTIFY_DIALOG_H__

#include "groupBox.h"
#include "textlabel.h"
#include "prettyButton.h"

class NotifyDialog : public CDialogEx
{
private:
    GroupBox m_topBorder;
    GroupBox m_bottomBorder;
    TextLabel m_titleText;
    TextLabel m_bodyText;
    PrettyButton m_closeBtn;
    PrettyButton m_txtcopyBtn;
    TextLabel m_textcopyText;

    CWnd *m_parentWindow;
    bool m_textCopied;

public:
    NotifyDialog(CWnd *parentWnd = nullptr);
    ~NotifyDialog() override;

protected:
    void DoDataExchange(CDataExchange *pDX) override;
    BOOL OnInitDialog() override;

private:
    void WindowCentering();
    void HideThisWindow();

public:
    void InitCControls();
    void InitialCreate();
    void Emerge(const std::string &message);
    void ToggleCopyMsgFeature(bool use);

private:
    void MessageCopyToClipboard();

protected:
    DECLARE_MESSAGE_MAP()
    afx_msg void OnPaint();
    afx_msg BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg void OnActivate(UINT nState, CWnd *pWndOther, BOOL bMinimized);
};

#endif

