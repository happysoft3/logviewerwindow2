
#include "stdafx.h"
#include "cbufferdc.h"
#include "prettySlider.h"
#include "common/utils/stringhelper.h"
#include <string>

IMPLEMENT_DYNAMIC(PrettySlider, CSliderCtrl)

using namespace _StringHelper;

PrettySlider::PrettySlider()
    : CSliderCtrl()
{ }

PrettySlider::~PrettySlider()
{ }

void PrettySlider::fillGradientBg(CDC *pDC, CRect &area, UINT colr1, UINT colr2)
{
    TRIVERTEX triver[2];

    triver[0].x = area.left;
    triver[0].y = area.top;
    triver[0].Red = GetRValue(colr1) << 8;
    triver[0].Green = GetGValue(colr1) << 8;
    triver[0].Blue = GetBValue(colr1) << 8;
    triver[0].Alpha = 0;

    //End Gradient Info
    triver[1].x = area.right;
    triver[1].y = area.bottom;
    triver[1].Red = GetRValue(colr2) << 8;
    triver[1].Green = GetGValue(colr2) << 8;
    triver[1].Blue = GetBValue(colr2) << 8;
    triver[1].Alpha = 0;

    GRADIENT_RECT gRt = { 0,1 };

    GradientFill(pDC->m_hDC, triver, 2, &gRt, 1, GRADIENT_FILL_RECT_V);
}

void PrettySlider::drawBackground(CDC *pDC)
{
    CRect area;

    GetClientRect(area);
    fillGradientBg(pDC, area, RGB(239, 237, 248), RGB(248, 64, 132));

    CRect thumbArea;
    CSliderCtrl::GetThumbRect(thumbArea);

    area.right = thumbArea.left;
    fillGradientBg(pDC, area, RGB(239, 237, 248), RGB(140, 16, 208));
}

void PrettySlider::drawCurrentLevel(CDC *pDC)
{
    CRect rect;

    CSliderCtrl::GetThumbRect(rect);
    CPen *pOldPen = pDC->SelectObject(&m_pen);
    CBrush *pOldBrush = pDC->SelectObject(&m_brush);

    pDC->Ellipse(rect);
    pDC->SelectObject(pOldPen);
    pDC->SelectObject(pOldBrush);
    //GetRangeMax
    /*int value = GetPos();
    std::string s = stringFormat("%d", value);

    CRect rect;
    pDC->GetClipBox(rect);
    pDC->DrawText(s.c_str(), s.length(), rect, DT_VCENTER | DT_SINGLELINE);*/
}

void PrettySlider::updateCurrentLevel()
{
    CRect rect;
    CSliderCtrl::GetThumbRect(rect);

    rect.right += 50;
    m_label.MoveWindow(rect);
    m_label.SetLabelText(stringFormat("%d", GetPos()));
    Invalidate();
}

void PrettySlider::PreSubclassWindow()
{
    m_pen.CreatePen(PS_SOLID, 1, RGB(0, 255, 0));
    m_brush.CreateSolidBrush(RGB(0, 128, 225));
    CRect rect;

    GetClientRect(rect);
    m_label.Create("label", WS_CHILD | WS_VISIBLE, rect, this);
    m_label.SetTextColor(RGB(241, 0, 225));
    updateCurrentLevel();
}

BOOL PrettySlider::PreTranslateMessage(MSG *pMsg)
{
    if (pMsg->message == WM_KEYDOWN)
    {
        switch (pMsg->wParam)
        {
        case VK_LEFT:
        case VK_DOWN:
            ChangePos(GetPos() - 1);
            return true;
        case VK_RIGHT:
        case VK_UP:
            ChangePos(GetPos() + 1);
            return true;
        }
    }
    return CSliderCtrl::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(PrettySlider, CSliderCtrl)
    //ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnCustomDraw)
    ON_WM_HSCROLL()
    ON_WM_MOUSEMOVE()
    ON_WM_LBUTTONDOWN()
    ON_WM_PAINT()
    //ON_WM_CTLCOLOR_REFLECT()
    ON_WM_MOUSEWHEEL()
    ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

BOOL PrettySlider::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
    return TRUE;
}

//void PrettySlider::OnCustomDraw(NMHDR *pNotifyStruct, LRESULT *result)
//{
//    
//    NMCUSTOMDRAW nmcd = *(LPNMCUSTOMDRAW)pNotifyStruct;
//
//    if (nmcd.dwDrawStage == CDDS_PREPAINT)
//    {
//        // return CDRF_NOTIFYITEMDRAW so that we will get subsequent 
//        // CDDS_ITEMPREPAINT notifications
//        *result = CDRF_NOTIFYITEMDRAW ;
//        return;
//    }
//    else if (nmcd.dwDrawStage == CDDS_ITEMPREPAINT)
//    {
//        if (nmcd.dwItemSpec == TBCD_THUMB)
//        {
//            CDC* pDC = CDC::FromHandle(nmcd.hdc);
//            pDC->SelectObject(m_brush);
//            pDC->SelectObject(m_pen);
//            pDC->Ellipse(&(nmcd.rc));
//            pDC->Detach();
//            *result = CDRF_SKIPDEFAULT;
//        }
//    }
//}

void PrettySlider::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{    
    m_label.SetLabelText(stringFormat("%d", nPos));
}

void PrettySlider::OnMouseMove(UINT nFlags, CPoint point)
{
    CSliderCtrl::OnMouseMove(nFlags, point);

    if (nFlags & MK_LBUTTON)
        updateCurrentLevel();
}

void PrettySlider::OnLButtonDown(UINT nFlags, CPoint point)
{
    CSliderCtrl::OnLButtonDown(nFlags, point);
    updateCurrentLevel();
}

void PrettySlider::OnPaint() {
    CBufferDC cdc(this);

    drawBackground(&cdc);
    drawCurrentLevel(&cdc);
}

BOOL PrettySlider::OnEraseBkgnd(CDC *pDC) {
    //drawBackground(pDC);
    return TRUE;
}

void PrettySlider::ChangePos(int n)
{
    int min, max;

    GetRange(min, max);
    if (n >= min && n <= max)
    {
        if (n != GetPos())
        {
            SetPos(n);
            updateCurrentLevel();
        }
    }
}

