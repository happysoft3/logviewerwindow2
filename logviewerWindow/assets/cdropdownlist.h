
#ifndef C_DROPDOWN_LIST_H__
#define C_DROPDOWN_LIST_H__

#include <list>
#include <string>
#include <memory>
#include <functional>

class CDropdownList : public CComboBox
{
public:
    using brushDeleterType = std::function<void(CBrush *)>;
    using componentCbType = std::function<void(CWnd*, uint32_t id, std::string)>;

private:
    std::unique_ptr<CBrush, brushDeleterType> m_bkBrush;
    UINT m_bkColor;
    UINT m_textColor;
    componentCbType m_callback;
    CWnd *m_callbackOwner;
    uint32_t m_id;

public:
    explicit CDropdownList(uint32_t id = -1);
    virtual ~CDropdownList() override;

private:
    void extentOpenHeight(int maxItemCount = -1);

public:
    int GetHeight() const;
    void ChangeHeight(int height);
    void SetCallback(CWnd *owner, componentCbType cb);
    void ChangeBackgroundColor(UINT color);
    void ChangeTextColor(UINT color);
    void UpdateItemList(const std::list<std::string> &itemList);
    std::string GetSelectItemName();

private:
    std::unique_ptr<CBrush, brushDeleterType> MakeBrush(UINT bkColor);

protected:
    afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);
    afx_msg BOOL OnCommand(WPARAM wParam, LPARAM lParam);
    afx_msg void OnSelectedItemChanged();
    DECLARE_MESSAGE_MAP()
};

#endif
