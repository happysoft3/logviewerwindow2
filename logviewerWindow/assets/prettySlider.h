
#ifndef PRETTY_SLIDER_H__
#define PRETTY_SLIDER_H__

#include "../textLabel.h"

class PrettySlider : public CSliderCtrl
{
    DECLARE_DYNAMIC(PrettySlider)
private:
    CPen m_pen;
    CBrush m_brush;
    TextLabel m_label;

public:
    PrettySlider();
    ~PrettySlider() override;

private:
    void fillGradientBg(CDC *pDC, CRect &area, UINT colr1, UINT colr2);
    void drawBackground(CDC *pDC);
    void drawCurrentLevel(CDC *pDC);
    void updateCurrentLevel();
    void PreSubclassWindow() override;
    BOOL PreTranslateMessage(MSG *pMsg) override;
    DECLARE_MESSAGE_MAP()
    afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
    //afx_msg void OnCustomDraw(NMHDR *pNotifyStruct, LRESULT *result);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC *pDC);

public:
    void ChangePos(int n);
};

#endif

