
#ifndef CHECK_BUTTON_H__
#define CHECK_BUTTON_H__

#include <functional>
#include <string>

class CheckButton : public CMFCButton
{
    DECLARE_DYNAMIC(CheckButton)
private:
    bool m_status;
    UINT m_checkedColor;
    UINT m_uncheckedColor;
    UINT m_checkedBkColor;
    UINT m_uncheckedBkColor;
    std::string m_checkText;
    std::string m_uncheckText;

    uint32_t m_clickdata;
    CWnd *m_receiver;
    using clickCallbackType = std::function<void(CWnd*, uint32_t, bool)>;
    clickCallbackType m_onClicked;

public:
    explicit CheckButton();
    virtual ~CheckButton() override;

    bool GetCheckStatus() const {
        return m_status;
    };
    void SetCheckStatus(bool state);
    void SetConditionalBkColor(UINT checkedColor, UINT uncheckColor);
    void SetConditionalTextColor(UINT checkedColor, UINT uncheckColor);
    void SetConditionalText(const std::string &checkedText, const std::string &uncheckedText = { });
    void SetClickCallback(CWnd *receiver, clickCallbackType method, uint32_t clickdata = 0);

private:
    void tryUpdate();

protected:
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

    DECLARE_MESSAGE_MAP()
};

#endif
