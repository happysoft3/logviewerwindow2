
#include "stdafx.h"
#include "CheckButton.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

IMPLEMENT_DYNAMIC(CheckButton, CMFCButton)

CheckButton::CheckButton()
    : CMFCButton()
{
    m_checkedColor = RGB(255, 255, 255);
    m_uncheckedColor = RGB(0, 0, 0);
    m_checkedBkColor = RGB(0, 0, 0);
    m_uncheckedBkColor = RGB(255, 255, 255);
    m_status = false;
    m_checkText = "checked";
    m_uncheckText = "unchecked";

    m_clickdata = 0;
    m_receiver = nullptr;
    m_onClicked = nullptr;
    EnableWindowsTheming(false);
    m_nFlatStyle = BUTTONSTYLE_NOBORDERS;
}

CheckButton::~CheckButton()
{ }

void CheckButton::SetCheckStatus(bool state)
{
    SetTextColor(state ? m_checkedColor : m_uncheckedColor);
    SetFaceColor(state ? m_checkedBkColor : m_uncheckedBkColor);
    m_status = state;
    if (((GetCheck()) & 1) != m_status)
        SetCheck(m_status & 1);
    tryUpdate();
}

void CheckButton::SetConditionalBkColor(UINT checkedColor, UINT uncheckedColor)
{
    m_checkedBkColor = checkedColor;
    m_uncheckedBkColor = uncheckedColor;
}

void CheckButton::SetConditionalTextColor(UINT checkedColor, UINT uncheckedColor)
{
    m_checkedColor = checkedColor;
    m_uncheckedColor = uncheckedColor;
}

void CheckButton::SetConditionalText(const std::string &checkedText, const std::string &uncheckedText)
{
    m_uncheckText = (uncheckedText.length() > 0) ? uncheckedText : checkedText;
    m_checkText = checkedText;
    tryUpdate();
}

void CheckButton::SetClickCallback(CWnd *receiver, clickCallbackType method, uint32_t clickdata)
{
    m_receiver = receiver;
    m_onClicked = method;
    m_clickdata = clickdata;
}

void CheckButton::tryUpdate()
{
    SetWindowText(toArray( m_status ? m_checkText : m_uncheckText) );
}

void CheckButton::OnLButtonDown(UINT nFlags, CPoint point)
{
    SetCheckStatus(m_status ^ true);
    if (m_onClicked != nullptr)
        m_onClicked(m_receiver, m_clickdata, m_status);
}

BEGIN_MESSAGE_MAP(CheckButton, CMFCButton)
    ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()
