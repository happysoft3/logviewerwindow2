
#include "stdafx.h"
#include "CDropdownList.h"
#include <algorithm>

CDropdownList::CDropdownList(uint32_t id)
    : CComboBox(), m_bkColor(RGB(255, 255, 255)), m_textColor(RGB(0, 0, 0))
{
    m_id = id;
    m_bkBrush = MakeBrush(m_bkColor);
    m_callback = nullptr;
    m_callbackOwner = nullptr;
}

CDropdownList::~CDropdownList()
{
    m_bkBrush.reset();
}

void CDropdownList::extentOpenHeight(int maxItemCount)
{
    CString str;
    CSize   sz;
    CDC*    pDC = GetDC();
    if (maxItemCount == -1)
        maxItemCount = GetCount();
    for (int i = 0; i < maxItemCount; i++)
    {
        GetLBText(i, str);
        sz = pDC->GetTextExtent(str);

        SetItemHeight(i, sz.cy);
    }
    ReleaseDC(pDC);
}

int CDropdownList::GetHeight() const
{
    CRect rect;

    GetWindowRect(rect);
    return rect.Height();
}

void CDropdownList::ChangeHeight(int height)
{
    CRect rect;

    GetWindowRect(rect);
    int cwidth = rect.Width();
    SetWindowPos(nullptr, 0, 0, cwidth, height, SWP_NOMOVE | SWP_NOZORDER);
    ShowWindow(SW_SHOW);
}

void CDropdownList::SetCallback(CWnd *owner, componentCbType cb)
{
    m_callbackOwner = owner;
    m_callback = cb;
}

void CDropdownList::ChangeBackgroundColor(UINT color)
{
    m_bkColor = color;
    m_bkBrush = MakeBrush(color);
}

void CDropdownList::ChangeTextColor(UINT color)
{

    m_textColor = color;
}

void CDropdownList::UpdateItemList(const std::list<std::string> &itemList)
{
    if (itemList.size())
    {
        SetRedraw(FALSE);
        for (const auto &s : itemList)
            InsertString(-1, s.c_str());
        SetCurSel(0);
        SetRedraw(TRUE);
    }
}

std::string CDropdownList::GetSelectItemName()
{
    int cursel = GetCurSel();

    if (cursel < 0)
        return{ };
    CString ctlFieldName;

    GetLBText(cursel, ctlFieldName);
    CT2CA pszConvertedAnsi(ctlFieldName);

    return std::string(pszConvertedAnsi);
}

std::unique_ptr<CBrush, CDropdownList::brushDeleterType> CDropdownList::MakeBrush(UINT bkColor)
{
    return std::unique_ptr<CBrush, brushDeleterType>(new CBrush(bkColor), [](CBrush *brush) { brush->DeleteObject(); delete brush; });
}

HBRUSH CDropdownList::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor)
{
    if (!m_bkBrush)
        return CComboBox::OnCtlColor(pDC, pWnd, nCtlColor);

    CComboBox::OnCtlColor(pDC, pWnd, nCtlColor);

    switch (nCtlColor)
    {
    case CTLCOLOR_EDIT:
    case CTLCOLOR_LISTBOX:
        pDC->SetBkColor(m_bkColor);
        pDC->SetTextColor(m_textColor);
    default:
        break;
    }
    return *m_bkBrush;
}

void CDropdownList::OnSelectedItemChanged()
{
    //리스트 선택 시 이벤트를 여기에서 처리합니다
    CString currentItemName;
    int cursel = GetCurSel();

    GetLBText(cursel, currentItemName);
    std::string itemName = currentItemName.GetString();
    if (m_callback != nullptr && currentItemName.GetLength())
        m_callback(m_callbackOwner, m_id, itemName);
}

BOOL CDropdownList::OnCommand(WPARAM wParam, LPARAM lParam)
{
    OnSelectedItemChanged();
    return CComboBox::OnCommand(wParam, lParam);
}

BEGIN_MESSAGE_MAP(CDropdownList, CComboBox)
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

