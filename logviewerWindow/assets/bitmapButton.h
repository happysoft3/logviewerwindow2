
#ifndef BITMAP_BUTTON_H__
#define BITMAP_BUTTON_H__

#include "../prettyButton.h"

class BitmapButton : public PrettyButton
{
private:
    CImage m_img;

public:
    explicit BitmapButton();
    virtual ~BitmapButton() override;

private:
    void drawComponent(CDC &cdc) override;

public:
    void FetchImage(const std::string &url, UINT altImg);
};

#endif
