
#include "stdafx.h"
#include "bitmapButton.h"

BitmapButton::BitmapButton()
    : PrettyButton()
{ }

BitmapButton::~BitmapButton()
{ }

void BitmapButton::drawComponent(CDC &cdc)
{
    CDC memDC;

    memDC.CreateCompatibleDC(&cdc);
    auto pOld = memDC.SelectObject(m_img);
    cdc.BitBlt(0, 0, m_img.GetWidth(), m_img.GetHeight(), &memDC, 0, 0, SRCCOPY);
    memDC.SelectObject(pOld);
    memDC.DeleteDC();
}

void BitmapButton::FetchImage(const std::string &url, UINT altImg)
{
    if (m_img.Load(url.c_str()) != S_OK)
    {
        m_img.LoadFromResource(AfxGetInstanceHandle(), altImg);
    }
}
