
#ifndef C_TAB_PAGE_H__
#define C_TAB_PAGE_H__

#include "common/ccobject/ccobject.h"
#include <string>
#include <functional>

class CTabPage : public CFormView
{
    DECLARE_DYNAMIC(CTabPage)

    struct PageWndCore
    {
        CWnd *m_parentWnd;

        PageWndCore(CWnd *wnd)
            : m_parentWnd(wnd)
        { }
    };
private:
    UINT m_nIDTemplate;
    CBrush m_bkBrush;
    UINT m_bkColor;
    std::string m_pageName;

protected:
    CWnd *m_parentWnd;

    class PageCom;
    std::shared_ptr<PageWndCore> m_wndCore;
    std::unique_ptr<PageCom> m_com;

public:
    CTabPage(UINT _nIDTemplate, CWnd *parent = nullptr);
    virtual ~CTabPage();

private:
    virtual std::unique_ptr<PageCom> MakeCommunication();
    virtual PageCom *GetCommunication() const;
    virtual void ValidAreaSetting(CDC &) {}

protected:
    virtual void DrawStuff(CDC &cdc);
    void SetValidArea(CDC &cdc, CWnd &wnd);

public:
    void ChangeBkColor(UINT _color);
    virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, CCreateContext *pContext = nullptr);
    virtual void OnInitialUpdate();
    virtual void OnEnterScreen();
    virtual void OnExitScreen();
    void Show(bool show = true);

    virtual bool CreatePage(const CRect &location, CCreateContext *pContext = nullptr);

    void SetPageName(const std::string &pageName)
    {
        m_pageName = pageName;
    }
    virtual void BeforeDestroy() {}

private:
    virtual void InitialDDX(std::function<void(int, CWnd&)> &&) {}
    virtual bool onEraseBackground(CDC *pDC);

protected:
    virtual void DoDataExchange(CDataExchange *pDX) override;
    afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);
    afx_msg BOOL OnEraseBkgnd(CDC *pDC);
    afx_msg void OnPaint();

    //afx_msg 
    DECLARE_MESSAGE_MAP()
};

class CTabPage::PageCom : public CCObject
{
protected:
    std::weak_ptr<CTabPage::PageWndCore> m_wndCore;

public:
    explicit PageCom(CCObject *parent = nullptr)
        : CCObject(parent)
    { }

    ~PageCom() override
    { }

    void SetParentWnd(std::weak_ptr<CTabPage::PageWndCore> wndCore)
    {
        m_wndCore = wndCore;
    }
};

#endif

