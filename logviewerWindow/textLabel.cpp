#include "stdafx.h"
#include "TextLabel.h"

TextLabel::TextLabel()
    : CStatic(), m_textColor(RGB(0, 0, 0))
{
    SetBackgroundColor(RGB(255, 255, 255));
}

void TextLabel::SetBackgroundColor(UINT rgb)
{
    m_bkColor = rgb;

    m_bkBrush = std::make_unique<CBrush>( /*m_bkColor*/);
}

void TextLabel::SetTextColor(UINT rgb)
{
    m_textColor = rgb;
}

std::string TextLabel::GetLabelText()
{
    CString text;

    GetWindowText(text);
    return text.GetBuffer();
}

void TextLabel::SetLabelText(const std::string &text)
{
    SetWindowText(text.c_str());
}

BEGIN_MESSAGE_MAP(TextLabel, CStatic)
    ON_WM_CTLCOLOR_REFLECT()
    ON_MESSAGE(WM_SETTEXT, OnSetText)
END_MESSAGE_MAP()

LRESULT TextLabel::OnSetText(WPARAM wParam, LPARAM lParam)
{
    LRESULT Result = Default();
    CRect Rect;

    GetWindowRect(&Rect);
    GetParent()->ScreenToClient(&Rect);
    GetParent()->InvalidateRect(&Rect);
    GetParent()->UpdateWindow();
    return Result;
}

HBRUSH TextLabel::CtlColor(CDC* pDC, UINT nCtlColor)
{
    pDC->SetTextColor(m_textColor);
    pDC->SetBkMode(TRANSPARENT);

    return (HBRUSH)GetStockObject(NULL_BRUSH);
}
