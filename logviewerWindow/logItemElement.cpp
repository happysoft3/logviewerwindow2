#include "stdafx.h"
#include "logItemElement.h"

LogItemElement::LogItemElement()
    : ListElement()
{ }

LogItemElement::~LogItemElement()
{ }

std::string LogItemElement::GetElement(int index)
{
    switch (index)
    {
    case PropertyInfo::LogMessage: return m_logMessage;
    case PropertyInfo::LogDatetime: return m_dateTime;
    default: return{ };
    }
}

void LogItemElement::SetElement(int index, const std::string &value)
{
    switch (index)
    {
    case PropertyInfo::LogDatetime:
        m_dateTime = value;
        break;
    case PropertyInfo::LogMessage:
        m_logMessage = value;
        break;
    default:
        break;
    }
}
