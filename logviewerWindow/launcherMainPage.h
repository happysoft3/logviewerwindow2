
#ifndef LAUNCHER_MAIN_PAGE_H__
#define LAUNCHER_MAIN_PAGE_H__

#include "cTabPage.h"
#include "prettyButton.h"
#include "assets/bitmapButton.h"

class LauncherMainPage : public CTabPage
{
    DECLARE_DYNAMIC(LauncherMainPage)

    class DerivedCom : public CTabPage::PageCom
    {
    //public:
        //DECLARE_SIGNAL(OnStartGame)
    };
private:
    BitmapButton /*PrettyButton*/ m_startBtn;
    BitmapButton m_webBtn;
    DerivedCom *m_staticPtr;

public:
    explicit LauncherMainPage(CWnd *parent);
    ~LauncherMainPage() override;

private:
    std::unique_ptr<PageCom> MakeCommunication() override;
    void OnInitialUpdate() override;
    void InitialDDX(std::function<void(int, CWnd&)> &&exchanger) override;
    void OnEnterScreen() override;
    void onStartButtonTriggered();
    void ValidAreaSetting(CDC &cdc) override;
    void DrawStuff(CDC &cdc) override;

public: DECLARE_SIGNAL(OnStartGame)
};

#endif

