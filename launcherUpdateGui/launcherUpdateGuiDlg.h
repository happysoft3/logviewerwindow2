
// launcherUpdateGuiDlg.h : 헤더 파일
//

#pragma once

#include <memory>

class CoreUi;

// ClauncherUpdateGuiDlg 대화 상자
class ClauncherUpdateGuiDlg : public CDialogEx
{
    class MainWindowImpl;

    static constexpr UINT c_mainWndUserMessage = WM_USER + 1;
    static constexpr UINT c_userMessageEndUpdateTask = 0;
    //static constexpr UINT c_userMessageStartProcess = 1;
private:
    std::unique_ptr<CoreUi> m_coreui;
    std::unique_ptr<MainWindowImpl> m_wndImpl;
    CStatic m_label;

// 생성입니다.
public:
	ClauncherUpdateGuiDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
    ~ClauncherUpdateGuiDlg() override;

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LAUNCHERUPDATEGUI_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

private:
    bool initializeMainWindow();
    void requestUpdateResult();

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
    afx_msg HRESULT OnUserMessageReceived(WPARAM wParam, LPARAM lParam);
    BOOL PreTranslateMessage(MSG *pMsg) override;
};
