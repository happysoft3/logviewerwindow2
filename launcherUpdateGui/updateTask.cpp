
#include "stdafx.h"
#include "updateTask.h"
#include "updateResult.h"
#include "newLauncherUpdateApp/newLauncherUpdateApp/updateModuleInterface.h"

UpdateTask::UpdateTask(UpdateModuleInterface *mod)
    : TaskUnit(), m_interface(mod)
{
    m_result = std::make_unique<UpdateResult>();
}

UpdateTask::~UpdateTask()
{ }

void UpdateTask::Run()
{
    if (!m_interface)
    {
        m_result->PutMessage("interface is null");
        return;
    }
    m_result->PutMessage(m_interface->DoUpdate());
}

std::unique_ptr<TaskResult> UpdateTask::GetResult()
{
    return std::move( m_result );
}

