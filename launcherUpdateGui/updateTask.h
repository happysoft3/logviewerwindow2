
#ifndef UPDATE_TASK_H__
#define UPDATE_TASK_H__

#include "common/taskthread/taskUnit.h"

class UpdateModuleInterface;
class UpdateResult;

class UpdateTask : public TaskUnit
{
private:
    std::unique_ptr<UpdateResult> m_result;
    std::unique_ptr<UpdateModuleInterface> m_interface;

public:
    explicit UpdateTask(UpdateModuleInterface *mod);
    ~UpdateTask() override;

private:
    void Run() override;
    std::unique_ptr<TaskResult> GetResult() override;
};

#endif

