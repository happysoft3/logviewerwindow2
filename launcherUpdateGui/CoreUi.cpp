
#include "stdafx.h"
#include "coreUi.h"
#include "updatetaskthread.h"
#include "common/taskthread/taskQueue.h"
#include "common/taskthread/taskResultHash.h"
#include "updateResult.h"
#include "updateTask.h"
#include "newLauncherUpdateApp/newLauncherUpdateApp/updateModuleInterface.h"
#include <windows.h>
#include <string>

class ExternLib
{
private:
    HMODULE m_hInstDll;

public:
    explicit ExternLib(HMODULE hInst)
        : m_hInstDll(hInst)
    { }
    ~ExternLib()
    {
        if (m_hInstDll == nullptr || m_hInstDll == INVALID_HANDLE_VALUE)
            ::FreeLibrary(m_hInstDll);
    }
};

class ExternLoader
{
private:
    std::unique_ptr<ExternLib> m_lib;
    using extern_proc_ty = void*;
    extern_proc_ty m_proc;

public:
    ExternLoader()
    {}
    ~ExternLoader()
    {}

private:
    std::unique_ptr<ExternLib> loadLib(const std::string &path, const std::string &symbolic)
    {
        HMODULE hInstDll = ::LoadLibraryA(path.c_str());

        if (hInstDll == nullptr || hInstDll == INVALID_HANDLE_VALUE)
        {
            return{ };
        }
        auto lib = std::make_unique<ExternLib>(hInstDll);
        auto proc = ::GetProcAddress(hInstDll, symbolic.c_str());

        if (!proc)
            return{ };
        m_proc = proc;
        return lib;
    }

public:
    bool Load(const std::string &path)
    {
        auto lib = loadLib(path, "?MakeInstance@UpdateModule@@SAPAVUpdateModuleInterface@@XZ");

        if (!lib)
            return false;

        m_lib = std::move(lib);
        return true;
    }

    template <class Ty, class... Args>
    Ty Invoke(Args&&... args)
    {
        auto proc = reinterpret_cast<Ty(*)(Args...)>(m_proc);

        return proc(std::forward<Args>(args)...);
    }
};

CoreUi::CoreUi(CCObject *parent)
    : CCObject(parent)
{ }

CoreUi::~CoreUi()
{ }

void CoreUi::createTaskThread()
{
    auto taskThread = std::make_unique<UpdateTaskThread>();

    taskThread->OnEndTask().Connection(&CoreUi::onCompleteTask, this);
    m_taskThread = std::move(taskThread);
}

bool CoreUi::loadUpdateApp(const std::string &appName)
{
    auto libLoader = std::make_unique<ExternLoader>();

    if (libLoader->Load(appName))
    {
        m_loader = std::move(libLoader);
        return true;
    }
    return false;
}

void CoreUi::onCompleteTask()
{
    m_OnEndTask.Emit();
}

bool CoreUi::Initialize()
{
    if (!loadUpdateApp("newLauncherUpdateApp.dll"))
        return false;

    createTaskThread();
    return true;
}

void CoreUi::RunUpdate()
{
    auto queue = std::make_unique<TaskQueue>();

    queue->Push(std::make_unique<UpdateTask>(m_loader->Invoke<UpdateModuleInterface *>()));
    m_taskThread->Perform(std::move(queue));
}

void CoreUi::FetchResult()
{
    auto resPool = m_taskThread->GetResult();
    auto res = resPool->PopAsValidType<UpdateResult>();

    m_OnReportState.Emit(res->Message());
    m_result = std::move(res);
}

