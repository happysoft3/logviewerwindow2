
#ifndef CORE_UI_H__
#define CORE_UI_H__

#include "common/ccobject/ccobject.h"

class ExternLoader;
class TaskThread;
class TaskResult;

class CoreUi : public CCObject
{
private:
    std::unique_ptr<ExternLoader> m_loader;
    std::unique_ptr<TaskThread> m_taskThread;
    std::unique_ptr<TaskResult> m_result;

public:
    explicit CoreUi(CCObject *parent = nullptr);
    ~CoreUi() override;

private:
    void createTaskThread();
    bool loadUpdateApp(const std::string &appName);
    void onCompleteTask();

public:
    bool Initialize();
    void RunUpdate();
    void FetchResult();

    DECLARE_SIGNAL(OnEndTask)
    DECLARE_SIGNAL(OnReportState, const char*)
};

#endif
