
#include "stdafx.h"
#include "updateTaskThread.h"

UpdateTaskThread::UpdateTaskThread()
    : TaskThread()
{ }

UpdateTaskThread::~UpdateTaskThread()
{ }

void UpdateTaskThread::afterExec(TaskResultHash &resultMap) noexcept
{
    m_OnEndTask.Emit();
}

