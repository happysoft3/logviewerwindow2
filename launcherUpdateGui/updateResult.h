
#ifndef UPDATE_RESULT_H__
#define UPDATE_RESULT_H__

#include "common/taskthread/taskResult.h"

class UpdateResult : public TaskResult
{
private:
    std::string m_message;

public:
    UpdateResult();
    ~UpdateResult() override;

public:
    void PutMessage(const std::string &message)
    {
        m_message = message;
    }
    const char *const Message() const
    {
        return m_message.c_str();
    }
};

#endif

