
#ifndef UPDATE_TASK_THREAD_H__
#define UPDATE_TASK_THREAD_H__

#include "common/taskthread/taskThread.h"

class UpdateTaskThread : public TaskThread
{
public:
    UpdateTaskThread();
    ~UpdateTaskThread() override;

private:
    void afterExec(TaskResultHash &resultMap) noexcept override;

    DECLARE_SIGNAL(OnEndTask)
};

#endif

